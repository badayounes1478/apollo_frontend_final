import React, { useEffect, useContext } from 'react'
import { drawBorder, showTheAlignmentOfSelectedElement, snapTheSelectedElement } from '../../functions/Showcontrol'
import { draw } from '../../functions/DrawElementsOnCanvas'
import CanvasData from '../../Canvas'
import '../screenscss/Editor.css'
import Navigationbar from '../reusablecomponents/Navigationbar'
import LeftSideBarContainer from '../reusablecomponents/LeftSideBarContainer'
import AppContext from '../../store/DataProvider'
import PreviewElements from '../reusablecomponents/PreviewElements'
import WebFont from 'webfontloader'
import ArtBoard from '../reusablecomponents/ArtBoard'

const Editor = () => {

    // global variables
    let draggingElement = false
    let selectedShape = null
    let offsetX = 0
    let offsetY = 0
    let startX = 0
    let startY = 0

    const context = useContext(AppContext)

    const initialiseCanvas = () => {
        CanvasData.canvas = document.getElementById("canvas1")
        CanvasData.ctx = CanvasData.canvas.getContext("2d");

        //declare the heat canvas
        CanvasData.canvas2 = document.getElementById('canvas2');
        CanvasData.ctx2 = CanvasData.canvas2.getContext('2d');

        //preview Canvas
        CanvasData.canvas4 = document.getElementById('canvas4');
        CanvasData.ctx4 = CanvasData.canvas4.getContext('2d');

        CanvasData.canvas.width = CanvasData.canvasWidth * CanvasData.multiplier
        CanvasData.canvas.height = CanvasData.canvasHeight * CanvasData.multiplier

        CanvasData.canvas2.width = CanvasData.canvasWidth * CanvasData.multiplier
        CanvasData.canvas2.height = CanvasData.canvasHeight * CanvasData.multiplier

        CanvasData.canvas4.width = CanvasData.canvasWidth * CanvasData.multiplier
        CanvasData.canvas4.height = CanvasData.canvasHeight * CanvasData.multiplier
    }

    const loadFonts = () => {
        let fonts = []
        CanvasData.multipleCanvas.forEach(data => {
            data.arrayOfElements.forEach(data1 => {
                if (data1.type === "text") {
                    fonts.push(data1.fontFamily)
                }
                if (data1.type === "group") {
                    data1.data.forEach(data2 => {
                        if (data2.type === "text") {
                            fonts.push(data2.fontFamily)
                        }
                    })
                }
            })
        })

        if (fonts.length <= 0) return
        WebFont.load({
            google: {
                families: fonts
            }
        });
        setTimeout(() => {
            draw()
        }, 3000);
    }

    useEffect(() => {
        // declare the show canvas
        initialiseCanvas()
        // event listeners on the canvas
        CanvasData.canvas.onmousedown = handleMouseDown;
        CanvasData.canvas.ondblclick = handleDoubleClick;
        window.onmousemove = handleMouseMove;
        window.onmouseup = handleMouseUp;

        loadFonts()
        draw()

        if (CanvasData.multipleCanvas.length <= 0) {
            CanvasData.design_id = Date.now().toString()
            const imageData = CanvasData.canvas.toDataURL('image/jpeg', 0.1);
            let addCanvasPage = {
                arrayOfElements: CanvasData.arrayOfElements,
                image: imageData,
                id: CanvasData.design_id,
                backgroundColor: CanvasData.backgroundColor
            }
            CanvasData.multipleCanvas = [...CanvasData.multipleCanvas, addCanvasPage]
        }
        context.refreshTheScreen()
    }, [])// eslint-disable-line react-hooks/exhaustive-deps

    const handleDoubleClick = () => {
        if (CanvasData.activeObject === null) return
        if (CanvasData.activeObject.type === "group") {
            CanvasData.arrayOfElements = [...CanvasData.arrayOfElements, ...CanvasData.activeObject.data]
            CanvasData.arrayOfElements = CanvasData.arrayOfElements.filter(data => data.id !== CanvasData.activeObject.id)
            CanvasData.arrayOfElements.forEach(data => {
                if (data.type === "svg") {
                    data.colorSet = false
                }
            })
            CanvasData.activeObject = null
            draw()
            context.refreshTheScreen()
            return
        }
        if (CanvasData.activeObject.type !== "text") return
        const activeObject = CanvasData.activeObject
        const textarea = document.createElement('textarea');

        textarea.value = activeObject.content;
        textarea.id = "editBox1"
        textarea.style.position = 'absolute';
        textarea.style.top = activeObject.y - 3.2 + 'px';
        textarea.style.left = activeObject.x + 'px';
        textarea.style.width = activeObject.width + 'px';
        let data1 = CanvasData.activeObject.content.split("\n")
        //3 padding
        textarea.style.height = (data1.length + CanvasData.activeObject.lineHeight) * CanvasData.activeObject.size + 10 + "px"
        textarea.style.fontSize = activeObject.size + 'px';
        textarea.style.border = 'none';
        textarea.style.padding = '0px';
        textarea.style.margin = '0px';
        textarea.style.overflow = 'hidden';
        textarea.style.background = 'none';
        textarea.style.outline = 'none';
        textarea.style.resize = 'none';
        //textarea.style.lineHeight = textNode.lineHeight();
        textarea.style.fontFamily = activeObject.fontFamily
        textarea.style.transformOrigin = 'left top';
        textarea.style.textAlign = activeObject.align;
        textarea.style.color = activeObject.color
        textarea.style.zIndex = 10
        textarea.onkeyup = handelSize
        textarea.focus();
        CanvasData.activeObject.content = ""
        document.getElementById("canvas-wraper").appendChild(textarea)
        function handelSize() {
            textarea.style.height = 'auto';
            textarea.style.height = textarea.scrollHeight + activeObject.size + 'px';
        }
        draw()
    }

    function handleEditBox() {
        let element = document.getElementById("editBox1")
        if (element === null) return
        if (CanvasData.activeObject !== null) {
            CanvasData.activeObject.content = element.value
        }
        element.remove();
        draw()
    }

    const handleMouseDown = (e) => {
        let shape
        // get the mouse co-ordinates
        handleEditBox()
        const canvas = CanvasData.canvas.getBoundingClientRect();
        const mousePos = {
            x: e.clientX - canvas.left,
            y: e.clientY - canvas.top
        }

        const pixel = CanvasData.ctx2.getImageData(mousePos.x, mousePos.y, 1, 1).data;
        const color = `rgb(${pixel[0]},${pixel[1]},${pixel[2]})`;

        if (color === "rgb(255,255,255)") {
            selectedShape = "TopLeft"
            draggingElement = true
        } else if (color === "rgb(255,255,254)") {
            selectedShape = "TopRight"
            draggingElement = true
        } else if (color === "rgb(255,255,253)") {
            selectedShape = "TopCenter"
            draggingElement = true
        } else if (color === "rgb(255,255,252)") {
            selectedShape = "BottomLeft"
            draggingElement = true
        } else if (color === "rgb(255,255,251)") {
            selectedShape = "LeftCenter"
            draggingElement = true
        } else if (color === "rgb(255,255,250)") {
            selectedShape = "BottomRight"
            draggingElement = true
        } else if (color === "rgb(255,255,249)") {
            selectedShape = "BottomCenter"
            draggingElement = true
        } else if (color === "rgb(255,255,248)") {
            selectedShape = "RightCenter"
            draggingElement = true
        } else {
            shape = CanvasData.colorsHash.includes(color);
            if (shape === false) {
                CanvasData.activeObject = null
                startX = parseInt(e.clientX - canvas.left);
                startY = parseInt(e.clientY - canvas.top);
                draggingElement = true
                draw()
                context.refreshTheScreen()
                return
            }
            let data = CanvasData.arrayOfElements.filter(data => color === data.colorKey)
            if (data.length === 0) {
                CanvasData.activeObject = null
                draw()
                context.refreshTheScreen()
                return
            }
            shape = data[0]
            CanvasData.activeObject = shape
            if (shape.lock === true) {
                draw()
                context.refreshTheScreen()
                context.changeActiveButtonLeft(2)
                return
            }
            draggingElement = true
            selectedShape = shape
            offsetX = e.clientX - selectedShape.x
            offsetY = e.clientY - selectedShape.y;
            draw()
        }
    }


    const handleMouseMove = (e) => {
        let mousePos
        //  this section is used to show the cursor according to the color hover
        const canvas = CanvasData.canvas.getBoundingClientRect();
        const mousePos1 = {
            x: e.clientX - canvas.left,
            y: e.clientY - canvas.top
        };
        const pixel = CanvasData.ctx2.getImageData(mousePos1.x, mousePos1.y, 1, 1).data;
        const color = `rgb(${pixel[0]},${pixel[1]},${pixel[2]})`;
        if (color !== "rgb(0,0,0)") {
            if (color === "rgb(255,255,255)" || color === "rgb(255,255,250)") {
                CanvasData.canvas.style.cursor = "nwse-resize"
            } else if (color === "rgb(255,255,254)" || color === "rgb(255,255,252)") {
                CanvasData.canvas.style.cursor = "nesw-resize"
            } else if (color === "rgb(255,255,253)" || color === "rgb(255,255,249)") {
                CanvasData.canvas.style.cursor = "ns-resize"
            } else if (color === "rgb(255,255,251)" || color === "rgb(255,255,248)") {
                CanvasData.canvas.style.cursor = "ew-resize"
            } else {
                CanvasData.canvas.style.cursor = "move"
            }
        } else {
            CanvasData.canvas.style.cursor = "default"
        }

        if (selectedShape === null && draggingElement === true) {
            draw()
            e.preventDefault();
            e.stopPropagation();
            const canvas = CanvasData.canvas.getBoundingClientRect();
            let mouseX = parseInt(e.clientX - canvas.left);
            let mouseY = parseInt(e.clientY - canvas.top);

            let width = mouseX - startX;
            let height = mouseY - startY;

            let widthe = Math.abs(startX + Math.abs(width))
            let highte = Math.abs(startY + Math.abs(height))

            CanvasData.ctx.lineWidth = 2;
            CanvasData.ctx.strokeStyle = "#FF4359";

            let data = CanvasData.arrayOfElements.filter(data => {
                return ((data.x > startX && data.x < widthe && data.y > startY && data.y < highte) || (data.x < startX && data.x > mouseX && data.y > startY && data.y < mouseY))
            })

            let filterGroupIfExistes = []
            data.forEach(data => {
                if (data.type === "group") {
                    filterGroupIfExistes.push(data)
                    data.data.forEach(data1 => {
                        filterGroupIfExistes.push(data1)
                    })
                } else {
                    filterGroupIfExistes.push(data)
                }
            })

            filterGroupIfExistes.forEach(data => {
                CanvasData.ctx.strokeRect(data.x, data.y, data.width, data.height);
            })

            CanvasData.groupes = [...filterGroupIfExistes]
            CanvasData.ctx.save()
            CanvasData.ctx.strokeRect(startX, startY, width, height);
            CanvasData.ctx.restore()
            return
        }

        if (selectedShape === null) return
        if (draggingElement === false) return

        //drag the shap and change it's width and height
        if (selectedShape === "TopLeft") {
            const canvas = CanvasData.canvas.getBoundingClientRect();
            mousePos = point(e.pageX - canvas.left, e.pageY - canvas.top);
            if (CanvasData.activeObject.type === "text") {
                CanvasData.activeObject.size += CanvasData.activeObject.y - mousePos.y;
            } else {
                let oldWidth = CanvasData.activeObject.width
                let oldHeight = CanvasData.activeObject.height
                if (Math.abs(CanvasData.activeObject.x - mousePos.x) > Math.abs(CanvasData.activeObject.y - mousePos.y)) {
                    if (CanvasData.activeObject.y === mousePos.y) {
                        CanvasData.activeObject.width = Math.abs(((CanvasData.activeObject.height - (CanvasData.activeObject.y - mousePos.y)) * CanvasData.activeObject.width) / CanvasData.activeObject.height)
                        CanvasData.activeObject.height = ((CanvasData.activeObject.width * oldHeight) / oldWidth)
                    }
                    if (CanvasData.activeObject.x > mousePos.x) {
                        CanvasData.activeObject.width = ((CanvasData.activeObject.x - mousePos.x) * (CanvasData.activeObject.width / 100)) + CanvasData.activeObject.width
                        CanvasData.activeObject.height = ((CanvasData.activeObject.width * oldHeight) / oldWidth)
                    }
                    if (CanvasData.activeObject.x < mousePos.x) {
                        CanvasData.activeObject.width = Math.abs(CanvasData.activeObject.width - ((mousePos.x - CanvasData.activeObject.x) * (CanvasData.activeObject.width / 100)))
                        CanvasData.activeObject.height = ((CanvasData.activeObject.width * oldHeight) / oldWidth)
                    }
                }

                if (Math.abs(CanvasData.activeObject.x - mousePos.x) < Math.abs(CanvasData.activeObject.y - mousePos.y)) {
                    if (CanvasData.x === mousePos.x) {
                        CanvasData.activeObject.height = Math.abs(((CanvasData.activeObject.width - (CanvasData.activeObject.x - mousePos.x)) * CanvasData.activeObject.height) / CanvasData.activeObject.width)
                        CanvasData.activeObject.height = ((CanvasData.activeObject.width * oldHeight) / oldWidth)
                    }
                    if (CanvasData.activeObject.y > mousePos.y) {
                        CanvasData.activeObject.height = ((CanvasData.activeObject.y - mousePos.y) * (CanvasData.activeObject.height / 100)) + CanvasData.activeObject.height
                        CanvasData.activeObject.width = ((CanvasData.activeObject.height * oldWidth) / oldHeight)
                    }
                    if (CanvasData.activeObject.y < mousePos.y) {
                        CanvasData.activeObject.height = Math.abs(CanvasData.activeObject.height - ((mousePos.y - CanvasData.activeObject.y) * (CanvasData.activeObject.height / 100)))
                        CanvasData.activeObject.width = ((CanvasData.activeObject.height * oldWidth) / oldHeight)
                    }
                }
            }
            CanvasData.activeObject.x = mousePos.x;
            CanvasData.activeObject.y = mousePos.y;
            changeTheCords()
            showTheAlignmentOfSelectedElement(CanvasData.arrayOfElements, CanvasData, CanvasData.activeObject)
        } else if (selectedShape === "TopCenter") {
            mousePos = point(e.pageX - canvas.left, e.pageY - canvas.top);
            if (CanvasData.activeObject.type === "text") {
                CanvasData.activeObject.size += CanvasData.activeObject.y - mousePos.y;
            } else {
                CanvasData.activeObject.height += CanvasData.activeObject.y - mousePos.y;
                if (CanvasData.activeObject.type === "group") {
                    CanvasData.activeObject.data.forEach(element => {
                        element.height = element.height + CanvasData.activeObject.y - mousePos.y
                        element.y = mousePos.y - element.ySide
                    })
                }
            }
            CanvasData.activeObject.y = mousePos.y;
            changeTheCords()
            showTheAlignmentOfSelectedElement(CanvasData.arrayOfElements, CanvasData, CanvasData.activeObject)
        } else if (selectedShape === "TopRight") {
            mousePos = point(e.pageX - canvas.left, e.pageY - canvas.top);
            if (CanvasData.activeObject.type === "text") {
                CanvasData.activeObject.size += CanvasData.activeObject.y - mousePos.y;
            } else {
                if (CanvasData.activeObject.y > mousePos.y) {
                    const ratio = CanvasData.activeObject.width / CanvasData.activeObject.height
                    CanvasData.activeObject.width += (CanvasData.activeObject.height * ratio) / 500
                    CanvasData.activeObject.height += (CanvasData.activeObject.width / ratio) / 500
                } else {
                    const ratio = CanvasData.activeObject.width / CanvasData.activeObject.height
                    CanvasData.activeObject.width -= (CanvasData.activeObject.height * ratio) / 500
                    CanvasData.activeObject.height -= (CanvasData.activeObject.width / ratio) / 500
                }
            }
            CanvasData.activeObject.y = mousePos.y;
            changeTheCords()
            showTheAlignmentOfSelectedElement(CanvasData.arrayOfElements, CanvasData, CanvasData.activeObject)
        } else if (selectedShape === "LeftCenter") {
            mousePos = point(e.pageX - canvas.left, e.pageY - canvas.top);
            let oldWidth = CanvasData.activeObject.width
            CanvasData.activeObject.width += CanvasData.activeObject.x - mousePos.x;
            CanvasData.activeObject.x = mousePos.x;
            if (CanvasData.activeObject.type === "group") {
                CanvasData.activeObject.data.forEach(element => {
                    element.width = element.width + CanvasData.activeObject.width - oldWidth
                    element.x = mousePos.x - element.xSide
                })
            }
            changeTheCords()
            showTheAlignmentOfSelectedElement(CanvasData.arrayOfElements, CanvasData, CanvasData.activeObject)
        } else if (selectedShape === "BottomLeft") {
            mousePos = point(e.pageX - canvas.left, e.pageY - canvas.top);
            CanvasData.activeObject.width += CanvasData.activeObject.x - mousePos.x;
            CanvasData.activeObject.x = mousePos.x;
            CanvasData.activeObject.height = mousePos.y - CanvasData.activeObject.y;
            changeTheCords()
            showTheAlignmentOfSelectedElement(CanvasData.arrayOfElements, CanvasData, CanvasData.activeObject)
        } else if (selectedShape === "BottomCenter") {
            mousePos = point(e.pageX - canvas.left, e.pageY - canvas.top);
            let oldHeight = CanvasData.activeObject.height
            CanvasData.activeObject.height = mousePos.y - CanvasData.activeObject.y;
            if (CanvasData.activeObject.type === "group") {
                CanvasData.activeObject.data.forEach(element => {
                    element.height = element.height + CanvasData.activeObject.height - oldHeight
                })
            }
            changeTheCords()
            showTheAlignmentOfSelectedElement(CanvasData.arrayOfElements, CanvasData, CanvasData.activeObject, draw)
        } else if (selectedShape === "BottomRight") {
            mousePos = point(e.pageX - canvas.left, e.pageY - canvas.top);
            CanvasData.activeObject.width = mousePos.x - CanvasData.activeObject.x;
            CanvasData.activeObject.height = mousePos.y - CanvasData.activeObject.y;
            changeTheCords()
            showTheAlignmentOfSelectedElement(CanvasData.arrayOfElements, CanvasData, CanvasData.activeObject)
        } else if (selectedShape === "RightCenter") {
            mousePos = point(e.pageX - canvas.left, e.pageY - canvas.top);
            let oldWidth = CanvasData.activeObject.width
            CanvasData.activeObject.width = mousePos.x - CanvasData.activeObject.x;
            if (CanvasData.activeObject.type === "group") {
                CanvasData.activeObject.data.forEach(element => {
                    element.width = element.width + CanvasData.activeObject.width - oldWidth
                })
            }
            changeTheCords()
            showTheAlignmentOfSelectedElement(CanvasData.arrayOfElements, CanvasData, CanvasData.activeObject)
        } else {
            selectedShape.x = e.clientX - offsetX;
            selectedShape.y = e.clientY - offsetY;
            let positionChangedArrayOfElements = CanvasData.arrayOfElements.map(data => {
                if (data.colorKey === selectedShape.colorKey) {
                    return data
                } else {
                    return data
                }
            })
            CanvasData.arrayOfElements = positionChangedArrayOfElements
            snapTheSelectedElement(CanvasData.arrayOfElements, CanvasData, selectedShape)
            if (selectedShape.type === "group") {
                selectedShape.data.forEach(data => {
                    data.x = selectedShape.x - data.xSide
                    data.y = selectedShape.y - data.ySide
                })
            }
            draw()
            drawBorder(selectedShape, CanvasData.ctx, CanvasData.ctx2)
            showTheAlignmentOfSelectedElement(CanvasData.arrayOfElements, CanvasData, selectedShape)
        }
    }

    const point = (x, y) => {
        return {
            x: x,
            y: y
        };
    }

    const changeTheCords = () => {
        let positionChangedArrayOfElements = CanvasData.arrayOfElements.map(data => {
            if (data.colorKey === CanvasData.activeObject.colorKey) {
                return CanvasData.activeObject
            } else {
                return data
            }
        })
        CanvasData.arrayOfElements = positionChangedArrayOfElements
        draw()
        drawBorder(CanvasData.activeObject, CanvasData.ctx, CanvasData.ctx2)
    }

    const handleMouseUp = () => {
        if (!draggingElement) { return; }
        if (CanvasData.activeObject === null && draggingElement === true) {
            draggingElement = false
            let x = Number.POSITIVE_INFINITY
            let xEnd = 0
            let y = Number.POSITIVE_INFINITY
            let yEnd = 0
            if (CanvasData.groupes.length > 1) {
                CanvasData.groupes.forEach(data => {
                    if (x > data.x) {
                        x = data.x
                    }
                    if (y > data.y) {
                        y = data.y
                    }
                    if (xEnd < data.x + data.width) {
                        xEnd = data.x + data.width
                    }
                    if (yEnd < data.y + data.height) {
                        yEnd = data.y + data.height
                    }
                })

                let filterArrayRemoveGroup = CanvasData.groupes.filter(data => data.type !== "group")

                let data = {
                    id: Date.now(), x: x, y: y, width: (xEnd - x), height: (yEnd - y), type: "group",
                    name: "Group", data: filterArrayRemoveGroup, lock: false, display: true, startTime: null,
                    endTime: null
                }

                CanvasData.groupes.forEach(data => {
                    data.xSide = x - data.x
                    data.ySide = y - data.y
                })

                let filterArray = CanvasData.arrayOfElements.filter(element => !CanvasData.groupes.find(elementGroup => (elementGroup.id === element.id && elementGroup.place === element.place)))

                CanvasData.arrayOfElements = [...filterArray, data]
                CanvasData.groupes = []
                CanvasData.ctx.restore()
                draw()
                drawBorder(data, CanvasData.ctx, CanvasData.ctx2)
                CanvasData.activeObject = data
                context.refreshTheScreen()
            } else {
                draw()
            }
            return
        }
        draw()
        let activeCanvas = CanvasData.multipleCanvas.find(data => data.id === CanvasData.design_id)
        activeCanvas.arrayOfElements = CanvasData.arrayOfElements
        activeCanvas.backgroundColor = CanvasData.backgroundColor
        activeCanvas.duration = CanvasData.duration
        activeCanvas.image = CanvasData.canvas.toDataURL('image/jpeg', 0.1)

        context.refreshTheScreen()
        context.changeActiveButtonLeft(2)
        drawBorder(selectedShape, CanvasData.ctx, CanvasData.ctx2)
        draggingElement = false
        selectedShape = null
    }

    return (
        <div className="top-container">
            <Navigationbar />
            <PreviewElements />
            <main id="move-area">
                <LeftSideBarContainer />

                <div className="editor-container">
                    <div className="canvas-container">
                        <div id="canvas-wraper" style={{ height: CanvasData.canvasHeight, width: CanvasData.canvasWidth }}>
                            <canvas id="canvas1" className={CanvasData.activeObject === null ? "borderCanvas" : null}></canvas>
                            <canvas id="canvas2"></canvas>
                            <canvas id="canvas3"></canvas>
                            <canvas id="canvas4"></canvas>
                        </div>
                    </div>

                    <ArtBoard />
                </div>
            </main>
        </div>
    )
}

export default Editor

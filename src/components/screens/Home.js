import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import Logo from '../../assets/Logo.svg'
import Search from '../../assets/Search.svg'
import CanvasData from '../../Canvas'
import '../screenscss/Home.css'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import { useHistory } from 'react-router-dom'

const Home = () => {

    const [templates, settemplates] = useState([])
    const history = useHistory();


    useEffect(() => {
        getTheUserTemplates()
    }, [])

    const getTheUserTemplates = async () => {
        try {
            const decoded = jwt_decode(localStorage.getItem('token'))
            const res = await Axios.get(constant.url + "templates/" + decoded.id)
            const filterDataToNormal = res.data.map(data => {
                data.template = JSON.parse(data.template)
                return data
            })
            settemplates(filterDataToNormal)
        } catch (error) {
            console.log(error)
        }
    }

    const callEditor = (width, height, multiply) => {
        CanvasData.arrayOfElements = []
        CanvasData.canvasWidth = width
        CanvasData.canvasHeight = height
        CanvasData.multiply = multiply
        CanvasData.multipleCanvas = []
        CanvasData.backgroundColor = "#ffffff"
        CanvasData.duration = 1000
        CanvasData.audio = null
        CanvasData.activeObject = null
        history.push('/editor')
    }

    const openTheDesign = (data) => {
        CanvasData.arrayOfElements = data.template[0].arrayOfElements
        CanvasData.multipleCanvas = data.template
        CanvasData.design_id = data.template[0].id
        CanvasData.backgroundColor = data.template[0].backgroundColor
        CanvasData.canvasWidth = data.canvasWidth
        CanvasData.canvasHeight = data.canvasHeight
        CanvasData.multiply = data.canvasMultiply
        history.push('/editor')
    }

    return (
        <div className="home-container">
            <nav>
                <div className="row">
                    <img src={Logo} alt="" />
                    <div><span>Creatosaurus</span> | Apollo</div>
                </div>
                <div className="circle-container" />
            </nav>

            <div className="image-container">
                <p>Design is not just what it looks like and feels like. Design is how it works.</p>
                <div className='search-box'>
                    <input placeholder="Search formats and designs" />
                    <img src={Search} alt="" />
                </div>
                <div className="card-container">
                    <div>
                        <span className="title" onClick={() => callEditor(640, 360, 3)}>16:9</span>
                    </div>
                    <div>
                        <span className="title" onClick={() => callEditor(180, 320, 6)}>9:16</span>
                    </div>
                    <div>
                        <span className="title" onClick={() => callEditor(360, 360, 3)}>1:1</span>
                    </div>
                    <div>
                        <span className="title">4:5</span>
                    </div>
                </div>
            </div>

            <div className="create-design">
                <div className="header">
                    <div className="title">Create design for</div>
                    <div style={{ color: '#404040' }}>{'<  >    See all'}</div>
                </div>
                <div className="container">
                    <div className="custom">
                        <div />
                        <span className="title">Custom Size</span>
                    </div>

                    <div className="presentation center"
                        onClick={() => callEditor(640, 360, 3)}>
                        <div />
                        <span className="title">Presentation (16:9)</span>
                        <span className="sub-title">1920 x 1080 px</span>
                    </div>

                    <div className="story center"
                        onClick={() => callEditor(180, 320, 6)}>
                        <div />
                        <span className="title">Story</span>
                        <span className="sub-title">1920 x 1080 px</span>
                    </div>

                    <div className="instagram-post center" onClick={() => callEditor(360, 360, 3)} >
                        <div />
                        <span className="title">Instagram Post</span>
                        <span className="sub-title">1080 x 1080 px</span>
                    </div>

                    <div className="facebook-post center" onClick={() => callEditor(235, 197, 4)}>
                        <div />
                        <span className="title">Facebook Post</span>
                        <span className="sub-title">940 x 788 px</span>
                    </div>

                    <div className="youtube-thumbnail center" onClick={() => callEditor(640, 360, 2)}>
                        <div />
                        <span className="title">YouTube Thumbnail</span>
                        <span className="sub-title">1280 x 720 px</span>
                    </div>

                    <div className="linkedin-post center" onClick={() => callEditor(300, 300, 4)}>
                        <div />
                        <span className="title">LinkedIn Post</span>
                        <span className="sub-title">1200 x 1200 px</span>
                    </div>

                    <div className="twitter-post center" onClick={() => callEditor(400, 225, 3)}>
                        <div />
                        <span className="title">Twitter Post</span>
                        <span className="sub-title">1600 x 900 px</span>
                    </div>

                    <div className="pinterest-pin center" onClick={() => callEditor(200, 300, 5)}>
                        <div />
                        <span className="title">Pinterest Pin</span>
                        <span className="sub-title">1000 x 1500 px</span>
                    </div>

                    <div className="facebook-ad center" onClick={() => callEditor(600, 314, 2)}>
                        <div />
                        <span className="title">Facebook Ad</span>
                        <span className="sub-title">1200 x 628 px</span>
                    </div>
                </div>
            </div>

            <div className="create-design">
                <div className="header">
                    <div className="title">Recent designs</div>
                    <div style={{ color: '#404040' }}>{'<  >    See all'}</div>
                </div>
                <div className="container">
                    {
                        templates.map(data => {
                            return <div className="design" key={data._id} onClick={() => openTheDesign(data)}>
                                <img src={data.template[0].image} crossOrigin="anonymous" alt="" />
                                <span className="project-name">{data.projectName}</span>
                            </div>
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default Home

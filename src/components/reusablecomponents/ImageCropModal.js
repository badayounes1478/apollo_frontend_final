import React, { Component } from 'react'
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import CanvasData from '../../Canvas';
import Crop from '../../assets/Crop.svg'
import '../reusablecomponentscss/ImageCropModal.css'
import { draw } from '../../functions/DrawElementsOnCanvas';

export default class ImageCropModal extends Component {
    state = {
        crop: {
            unit: '%',
            width: 30,
            aspect: 16 / 9,
        },
    };


    onImageLoaded = image => {
        this.imageRef = image;
        // Center a square percent crop.
        const width = image.width > image.height ? (image.height / image.width) * 100 : 100;
        const height = image.height > image.width ? (image.width / image.height) * 100 : 100;
        const x = width === 100 ? 0 : (100 - width) / 2;
        const y = height === 100 ? 0 : (100 - height) / 2;

        this.setState({
            crop: {
                unit: '%',
                aspect: 1,
                width,
                height,
                x,
                y,
            },
        });

        return false; // Return false if you set crop state in here.

    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        this.setState({ crop });
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            this.setState({ croppedImageUrl });
        }
    }

    updateCrop = () => {
        const img = document.getElementById(CanvasData.activeObject.elementId)
        img.src = this.state.croppedImageUrl
        this.props.toggleCropModal()
        draw()
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return canvas.toDataURL()
    }

    render() {
        const { crop } = this.state;
        const src = CanvasData.activeObject.src

        return (
            <div className="modal-image">
                <div className="holder-modal">
                    <div className="header-modal">
                        <span className="center"><img className="crop-modal" src={Crop} alt=""></img>Crop</span>
                        <span onClick={()=> this.props.toggleCropModal()}>❌ Close</span>
                    </div>
                    <div>
                        {src && (
                            <ReactCrop
                                src={src}
                                crop={crop}
                                ruleOfThirds
                                onImageLoaded={this.onImageLoaded}
                                onComplete={this.onCropComplete}
                                onChange={this.onCropChange}
                                crossorigin="anonymous"
                                imageStyle={{ width: 'auto', height: 300 }}
                                className="image-edit"
                            />
                        )}
                    </div>
                    <button onClick={this.updateCrop}>✅ Update</button>
                </div>
            </div>
        );
    }
}

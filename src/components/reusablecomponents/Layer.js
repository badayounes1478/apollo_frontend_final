import React, { useContext } from 'react'
import Lock2 from '../../assets/Lock2.svg'
import Unlock from '../../assets/Unlock.svg'
import Eye from '../../assets/Eye.svg'
import EyeOff from '../../assets/EyeOff.svg'
import CanvasData from '../../Canvas'
import '../reusablecomponentscss/Layer.css'
import { draw } from '../../functions/DrawElementsOnCanvas'
import { drawBorder } from '../../functions/Showcontrol'
import AppContext from '../../store/DataProvider'
import { lockAndUnlock, displayElementToggle } from '../../functions/ElementsCommonFunctions'

const Layer = () => {

    const context = useContext(AppContext)

    const activateElement = (data) => {
        CanvasData.activeObject = data
        draw()
        drawBorder(data, CanvasData.ctx, CanvasData.ctx2)
        context.refreshTheScreen()
    }

    const activeCanvas = () => {
        CanvasData.activeObject = null
        draw()
        context.refreshTheScreen()
    }

    return (
        <div className="layer-container">
            <div className="left-header-title">Project name</div>
            <div className="layer-section">
                {
                    CanvasData.arrayOfElements.map((data, index) => {
                        return <div key={"object" + index} className="card" onClick={() => activateElement(data)}>
                            <span>{data.name}</span>
                            <span className="layer-button-container">
                                <img onClick={lockAndUnlock} src={data.lock ? Unlock : Lock2} alt="" />
                                <img onClick={displayElementToggle} src={data.display ? Eye : EyeOff} alt="" />
                            </span>
                        </div>
                    }).reverse()
                }
                <div className="card" onClick={activeCanvas}>
                    <span>Canvas Background</span>
                </div>
            </div>
        </div>
    )
}

export default Layer

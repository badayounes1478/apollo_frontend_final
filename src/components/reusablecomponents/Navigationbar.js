import React, { useContext, useState } from 'react'
import Logo from '../../assets/Logo.svg'
import CanvasData from '../../Canvas'
import '../reusablecomponentscss/Navigationbar.css'
import Share from '../../assets/Share.svg'
import Update from '../../assets/Update.svg'
import Download from '../../assets/Download.svg'
import { draw } from '../../functions/DrawElementsOnCanvas'
import Axios from 'axios'
import AppContext from '../../store/DataProvider'
import jwt_decode from 'jwt-decode'
import constant from '../../constant'
import AdminPopup from './AdminPopup'

const Navigationbar = () => {

    const context = useContext(AppContext)
    const [loading, setloading] = useState(false)
    const [showCard, setshowCard] = useState(false)

    const changeProjectName = (projectname) => {
        CanvasData.projectName = projectname
        context.refreshTheScreen()
    }

    const downloadImage = async () => {
        try {

            let data = [...CanvasData.multipleCanvas]
            data.forEach(data => {
                data.arrayOfElements.forEach(data1 => {
                    if (data1.type === "svg") {
                        data1.flag = 0
                        data1.blobURL = null
                        data1.blobCompleted = false
                    }
                    if (data1.type === "group") {
                        data1.data.forEach(data2 => {
                            if (data2.type === "svg") {
                                data2.flag = 0
                                data2.blobURL = null
                                data2.blobCompleted = false
                            }
                        })
                    }
                })
            })

            const decoded = jwt_decode(localStorage.getItem('token'))
            await Axios.post(constant.url + "video/create", {
                userId: decoded.id,
                template: data,
                canvasWidth: CanvasData.canvasWidth,
                canvasHeight: CanvasData.canvasHeight,
                canvasMultiply: CanvasData.multiply,
                projectName: CanvasData.projectName,
                audio: CanvasData.audio
            })
            alert("video started processing")
        } catch (error) {
            console.log(error)
        }
    }

    const saveTemplate = async () => {
        try {
            const decoded = jwt_decode(localStorage.getItem('token'))
            if (decoded.isAdmin === true) return setshowCard(true)
            draw()
            let activeCanvas = CanvasData.multipleCanvas.find(data => data.id === CanvasData.design_id)
            activeCanvas.arrayOfElements = CanvasData.arrayOfElements
            activeCanvas.backgroundColor = CanvasData.backgroundColor
            activeCanvas.image = CanvasData.canvas.toDataURL('image/jpeg', 0.8)

            let data = [...CanvasData.multipleCanvas]
            data.forEach(data => {
                data.arrayOfElements.forEach(data1 => {
                    if (data1.type === "svg") {
                        data1.flag = 0
                        data1.blobURL = null
                        data1.blobCompleted = false
                    }
                    if (data1.type === "group") {
                        data1.data.forEach(data2 => {
                            if (data2.type === "svg") {
                                data2.flag = 0
                                data2.blobURL = null
                                data2.blobCompleted = false
                            }
                        })
                    }
                })
            })

            setloading(true)
            let template = JSON.stringify(data)
            const res = await Axios.post(constant.url + 'templates', {
                userId: decoded.id,
                template: template,
                canvasWidth: CanvasData.canvasWidth,
                canvasHeight: CanvasData.canvasHeight,
                canvasMultiply: CanvasData.multiply,
                projectName: CanvasData.projectName
            })
            if (res.data === "done") {
                setloading(false)
            }
        } catch (error) {
            setloading(false)
            console.log(error.response)
        }
    }

    const addAdminTemplate = async (description, colors, category, tags, regions) => {
        try {
            setshowCard(false)
            draw()
            let activeCanvas = CanvasData.multipleCanvas.find(data => data.id === CanvasData.design_id)
            activeCanvas.arrayOfElements = CanvasData.arrayOfElements
            activeCanvas.backgroundColor = CanvasData.backgroundColor
            activeCanvas.image = CanvasData.canvas.toDataURL('image/jpeg', 0.1)

            let data = [...CanvasData.multipleCanvas]
            data.forEach(data => {
                data.arrayOfElements.forEach(data1 => {
                    if (data1.type === "svg") {
                        data1.flag = 0
                        data1.blobURL = null
                        data1.blobCompleted = false
                    }
                    if (data1.type === "group") {
                        data1.data.forEach(data2 => {
                            if (data2.type === "svg") {
                                data2.flag = 0
                                data2.blobURL = null
                                data2.blobCompleted = false
                            }
                        })
                    }
                })
            })

            setloading(true)
            const decoded = jwt_decode(localStorage.getItem('token'))
            let template = JSON.stringify(data)
            const res = await Axios.post(constant.url + 'admintemplates', {
                userId: decoded.id,
                template: template,
                canvasWidth: CanvasData.canvasWidth,
                canvasHeight: CanvasData.canvasHeight,
                canvasMultiply: CanvasData.multiply,
                projectName: CanvasData.projectName,
                description: description,
                colors: colors,
                category: category,
                tags: tags,
                region: regions,
            })
            if (res.data === "done") {
                setloading(false)
            }
        } catch (error) {
            setloading(false)
            console.log(error)
        }
    }

    return (
        <React.Fragment>
            {
                showCard === true ? <AdminPopup
                    close={() => setshowCard(false)}
                    submit={addAdminTemplate} /> : null
            }
            <nav>
                <div className="row">
                    <img src={Logo} alt="" />
                    <div><span>Creatosaurus</span> | Apollo</div>
                    <div className="project-info">
                        <input
                            onChange={(e) => changeProjectName(e.target.value)}
                            value={CanvasData.projectName} />
                        {
                            loading === true ?
                                <span>Saving...</span> :
                                <span onClick={saveTemplate}>Save</span>
                        }
                    </div>
                </div>
                <div className="row">
                    <span className="share">
                        <img src={Share} alt="" />
                        <span>share</span>
                    </span>
                    <button className="cache">
                        <img src={Update} alt="" />
                        <span>Cache it</span>
                    </button>
                    <button onClick={downloadImage} className="download">
                        <img src={Download} alt="" />
                        <span>Download</span>
                    </button>
                </div>
            </nav>
        </React.Fragment>
    )
}

export default Navigationbar

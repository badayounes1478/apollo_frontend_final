import React from 'react'
import ShowFilters from '../../assets/ShowFilters.jpg'
import CanvasData from '../../Canvas'
import { draw } from '../../functions/DrawElementsOnCanvas'
import '../reusablecomponentscss/Filters.css'

const Filters = (props) => {

    const addFilter = (brightness, contrast, saturate, filterName) => {
        CanvasData.activeObject.brightness = brightness
        CanvasData.activeObject.contrast = contrast
        CanvasData.activeObject.saturate = saturate
        CanvasData.activeObject.invert = '0%'
        CanvasData.activeObject.grayscale = '0%'
        CanvasData.activeObject.sepia = '0%'
        CanvasData.activeObject.filterName = filterName
        draw()
    }


    return (
        <div className="filters-container">
            <div onClick={() => props.toggleFilter()} className="header">← Filters</div>
            <div className="filters">
                <div className="none">
                    <span className="container" onClick={() => addFilter('100%', '100%', '100%', 'None')}>
                        <span>None</span>
                        <img src={ShowFilters} alt="" />
                    </span>
                </div>
                <div className="all-filters">
                    <span className="container" onClick={() => addFilter('122%', '54%', '138%', 'Cali')}>
                        <span>Cali</span>
                        <img className="cali" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('90%', '121%', '134%', 'drama')}>
                        <span>Drama</span>
                        <img className="drama" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('110%', '76%', '126%', 'Edge')}>
                        <span>Edge</span>
                        <img className="edge" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('106%', '120%', '86%', 'Epic')}>
                        <span>Epic</span>
                        <img className="epic" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('110%', '121%', '124%', 'Festive')} >
                        <span>Festive</span>
                        <img className="festive" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('115%', '80%', '0%', 'Grayscale')}>
                        <span>Grayscale</span>
                        <img className="grayscale" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('115%', '84%', '50%', 'Nordic')}>
                        <span>Nordic</span>
                        <img className="nordic" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('100%', '86%', '82%', 'Retro')}>
                        <span>Retro</span>
                        <img className="retro" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('100%', '155%', '72%', "Rosie")}>
                        <span>Rosie</span>
                        <img className="rosie" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('110%', '88%', '88%', 'Selfie')}>
                        <span>Selfie</span>
                        <img className="selfie" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('93%', '121%', '5%', 'Street')}>
                        <span>Street</span>
                        <img className="street" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('110%', '114%', '118%', 'Summer')}>
                        <span>Summer</span>
                        <img className="summer" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('163%', '63%', '74%', 'The Blues')}>
                        <span>The Blues</span>
                        <img className="blues" src={ShowFilters} alt="" />
                    </span>
                    <span className="container" onClick={() => addFilter('143%', '85%', '47%', 'Whimsical')}>
                        <span>Whimsical</span>
                        <img className="whimsical" src={ShowFilters} alt="" />
                    </span>
                </div>
            </div>
        </div>
    )
}

export default Filters

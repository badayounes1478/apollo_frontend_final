import React, { useEffect, useState, useContext } from 'react'
import RightArrow from '../../assets/RightArrow.svg'
import '../reusablecomponentscss/Template.css'
//import constant from '../../constant'
import CanvasData from '../../Canvas'
import AppContext from '../../store/DataProvider'
import { draw } from '../../functions/DrawElementsOnCanvas'
import WebFont from 'webfontloader'
import Axios from 'axios'

const Template = () => {

    const [templatesAdmin, settemplatesAdmin] = useState([])
    const context = useContext(AppContext)

    useEffect(() => {
        getTheTemplates()
    }, [])

    const getTheTemplates = async () => {
        try {
            const res = await Axios.get("https://apollo.creatosaurus.io/apollo/video/template/" + CanvasData.canvasWidth + "/" + CanvasData.canvasHeight + "?page=1&limit=5")
            const filterDataToNormal = res.data.map(data => {
                data.template = JSON.parse(data.template)
                return data
            })
            settemplatesAdmin(filterDataToNormal)
        } catch (error) {
            console.log(error)
        }
    }


    const loadFonts = () => {
        let fonts = []
        CanvasData.multipleCanvas.forEach(data => {
            data.arrayOfElements.forEach(data1 => {
                if (data1.type === "text") {
                    fonts.push(data1.fontFamily)
                }
                if (data1.type === "group") {
                    data1.data.forEach(data2 => {
                        if (data2.type === "text") {
                            fonts.push(data2.fontFamily)
                        }
                    })
                }
            })
        })

        if (fonts.length <= 0) return
        WebFont.load({
            google: {
                families: fonts
            }
        })
    }

    const addTemplate = (data) => {
        CanvasData.audio = JSON.parse(data.audio)
        CanvasData.canvasWidth = data.canvasWidth
        CanvasData.canvasHeight = data.canvasHeight
        CanvasData.multiply = data.canvasMultiply
        CanvasData.projectName = data.projectName
        CanvasData.multipleCanvas = data.template

        CanvasData.arrayOfElements = data.template[0].arrayOfElements
        CanvasData.design_id = data.template[0].id
        CanvasData.backgroundColor = data.template[0].backgroundColor
        CanvasData.duration = data.template[0].duration
        context.refreshTheScreen()
        loadFonts()
        setTimeout(() => {
            draw()
        }, 5000);
    }

    return (
        <div className="template-container">
            <div className="left-header-title">Templates</div>
            <input className="left-search-box" placeholder="Search" type="text" />
            <div className="template-section">
                <div className="header">
                    <span>Birthday Presentation</span>
                    <span>See all</span>
                </div>
                <div className="cards">
                    {
                        templatesAdmin.map(data => {
                            return <img
                                onClick={() => addTemplate(data)}
                                key={data._id}
                                src={data.template[0].image}
                                crossOrigin="anonymous"
                                alt="" />
                        })
                    }
                </div>
            </div>

            <div className="template-section">
                <div className="header">
                    <span>Sales Presentation</span>
                    <span>See all</span>
                </div>
                <div className="cards">
                    <img src="https://cdn.pixabay.com/photo/2021/05/03/13/38/fairy-flower-6226285__340.jpg" alt="" />
                    <img src="https://cdn.pixabay.com/photo/2021/05/03/13/38/fairy-flower-6226285__340.jpg" alt="" />
                    <img className="arrow" src={RightArrow} alt="" />
                </div>
            </div>
        </div>
    )
}

export default Template

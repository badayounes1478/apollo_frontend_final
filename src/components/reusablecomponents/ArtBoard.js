import React, { useContext, useState } from 'react'
import '../reusablecomponentscss/ArtBoard.css'
import CanvasData from '../../Canvas'
import { draw } from '../../functions/DrawElementsOnCanvas'
import AppContext from '../../store/DataProvider'
import { drawAnimation } from '../../functions/PreviewElementsOnCanvas'

//head bar icons
import Previous from '../../assets/Previous.svg'
import Play from '../../assets/Play.svg'
import Pause from '../../assets/Pause.svg'
import Next from '../../assets/Next.svg'


const ArtBoard = () => {

    const context = useContext(AppContext)
    const [time, settime] = useState("00:00")
    let cancelRequestAnimationFrame3
    let cancelRequestAnimationFrame4

    const callPreview = () => {
        let time = 0
        let n = 0
        let seenTime = 0

        CanvasData.multipleCanvas.forEach(data => {
            time = parseInt(data.duration) + time
        })

        seenTime = parseInt(CanvasData.multipleCanvas[n].duration) + seenTime

        const callTheSeens = () => {
            currentTimeOfVideoPlaying()
            if (CanvasData.playing === false) { //stop request animation frame after pause
                cancelAnimationFrame(cancelRequestAnimationFrame3)
                cancelAnimationFrame(cancelRequestAnimationFrame4)
                return
            }

            if (seenTime / (1000 / 30) > CanvasData.allFrame) {
                let arrayOfElements = CanvasData.multipleCanvas[n].arrayOfElements
                let backgroundColor = CanvasData.multipleCanvas[n].backgroundColor
                drawAnimation(arrayOfElements, backgroundColor)
            } else {
                n = n + 1
                if (n >= CanvasData.multipleCanvas.length) return closeTheVideoPlayingAfterFinishingTheFrames()
                CanvasData.frame = 0
                seenTime = parseInt(CanvasData.multipleCanvas[n].duration) + seenTime
                let arrayOfElements = CanvasData.multipleCanvas[n].arrayOfElements
                let backgroundColor = CanvasData.multipleCanvas[n].backgroundColor
                drawAnimation(arrayOfElements, backgroundColor)
            }
            cancelRequestAnimationFrame4 = requestAnimationFrame(callTheSeens)
        }

        cancelRequestAnimationFrame3 = requestAnimationFrame(callTheSeens)
    }


    const currentTimeOfVideoPlaying = () => {
        context.refreshTheScreen()
        let millisec = CanvasData.allFrame * (1000 / 30)
        let seconds = Math.floor((millisec / 1000) % 60)
        let minutes = Math.floor((millisec / (1000 * 60)) % 60)

        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;
        settime(minutes + ":" + seconds)
    }

    const closeTheVideoPlayingAfterFinishingTheFrames = () => {
        CanvasData.canvas.style.display = "unset"
        CanvasData.frame = 0
        CanvasData.allFrame = 0
        CanvasData.playing = false
        if (CanvasData.audio !== null) {
            let audio = document.getElementById(CanvasData.audio.elementId)
            audio.pause()
            audio.currentTime = 0
        }
        CanvasData.multipleCanvas.forEach(data => {
            data.arrayOfElements.forEach(element => {
                if (element.type === "video") {
                    let video = document.getElementById(element.elementId)
                    video.pause()
                    video.currentTime = 0
                }
                element.showElementPreview = true

                // animation work
                if (element.animation !== undefined) {
                    if (element.animation.type === "fromLeft") {
                        element.animation = { play: true, x: 0 - element.width, y: 0, type: "fromLeft", speed: element.animation.speed, delay: 2000, time: element.animation.time }
                    } else if (element.animation.type === "fromRight") {
                        element.animation = { play: true, x: CanvasData.canvasWidth, y: 0, type: "fromRight", speed: element.animation.speed, delay: 2000, time: element.animation.time }
                    } else if (element.animation.type === "fromTop") {
                        element.animation = { play: true, x: 0, y: 0 - element.height, type: "fromTop", speed: element.animation.speed, delay: 2000, time: element.animation.time }
                    } else if (element.animation.type === "fromBottom") {
                        element.animation = { play: true, x: 0, y: CanvasData.canvasHeight, type: "fromBottom", speed: element.animation.speed, delay: 2000, time: element.animation.time }
                    } else if (element.animation.type === "fadeIn") {
                        element.animation = { play: true, type: "fadeIn", opacity: 0, speed: element.animation.speed, delay: 2000, time: element.animation.time }
                    } else if (element.animation.type === "fadeOut") {
                        element.animation = { play: true, type: "fadeOut", opacity: 1, speed: element.animation.speed, delay: 2000, time: element.animation.time }
                    } else if (element.animation.type === "rotate") {
                        element.animation = { play: true, type: "rotate", rotate: 0, speed: element.animation.speed, time: element.animation.time, delay: 2000 }
                    }
                }
            })
        })
        context.refreshTheScreen()
        return
    }

    const addCanvas = () => {
        let activeCanvas = CanvasData.multipleCanvas.find(data => data.id === CanvasData.design_id)
        activeCanvas.arrayOfElements = CanvasData.arrayOfElements
        activeCanvas.image = CanvasData.canvas.toDataURL('image/jpeg', 0.1)
        activeCanvas.backgroundColor = CanvasData.backgroundColor
        activeCanvas.duration = CanvasData.duration

        CanvasData.design_id = Date.now().toString()
        CanvasData.arrayOfElements = []
        CanvasData.backgroundColor = "#ffffff"
        CanvasData.activeObject = null
        CanvasData.duration = 1000

        draw()
        const imageData = CanvasData.canvas.toDataURL('image/jpeg', 0.1);

        let addCanvasPage = {
            arrayOfElements: CanvasData.arrayOfElements,
            image: imageData,
            id: CanvasData.design_id,
            backgroundColor: CanvasData.backgroundColor,
            duration: CanvasData.duration
        }

        CanvasData.multipleCanvas = [...CanvasData.multipleCanvas, addCanvasPage]
        context.refreshTheScreen()
    }

    const editCanvas = (data) => {
        CanvasData.arrayOfElements = data.arrayOfElements
        CanvasData.design_id = data.id
        CanvasData.backgroundColor = data.backgroundColor
        CanvasData.duration = data.duration
        draw()
    }


    const viewImageFull = () => {
        CanvasData.multiplier = CanvasData.multiply
        draw()
        let data = CanvasData.canvas.toDataURL('image/png', 1.0)
        CanvasData.multiplier = 1
        draw()

        const x = document.getElementById("image-preview")
        x.src = data
        if (x.requestFullscreen) {
            x.requestFullscreen();
        } else if (x.webkitRequestFullscreen) { /* Safari */
            x.webkitRequestFullscreen();
        } else if (x.msRequestFullscreen) { /* IE11 */
            x.msRequestFullscreen();
        }
    }


    const giveTheCurrentStateToAnimation = () => {
        CanvasData.multipleCanvas.forEach(arrayOfElements => {
            arrayOfElements.arrayOfElements.forEach(element => {
                if (element.animation !== undefined) {
                    if (element.animation.type === "fromLeft") {
                        element.animation.x = 0 - element.width
                        element.animation.speed = (element.width + element.x) / (30 * element.animation.time)
                    } else if (element.animation.type === "fromRight") {
                        element.animation.speed = (CanvasData.canvasWidth - element.x) / (30 * element.animation.time)
                    } else if (element.animation.type === "fromTop") {
                        element.animation.y = 0 - element.height
                        element.animation.speed = (element.height + element.y) / (30 * element.animation.time)
                    } else if (element.animation.type === "fromBottom") {
                        element.animation.speed = (CanvasData.canvasHeight - element.y) / (30 * element.animation.time)
                    }
                }
            })
        })
    }


    const toogleCanvasPlaying = () => {
        if (CanvasData.playing === false) {
            CanvasData.playing = !CanvasData.playing
            context.refreshTheScreen()
            CanvasData.canvas.style.display = "none"

            let activeCanvas = CanvasData.multipleCanvas.find(data => data.id === CanvasData.design_id)
            activeCanvas.arrayOfElements = CanvasData.arrayOfElements
            activeCanvas.image = CanvasData.canvas.toDataURL('image/jpeg', 0.1)
            activeCanvas.backgroundColor = CanvasData.backgroundColor
            activeCanvas.duration = CanvasData.duration

            giveTheCurrentStateToAnimation()
            callPreview()
        } else {
            context.refreshTheScreen()
            cancelAnimationFrame(cancelRequestAnimationFrame3)
            cancelAnimationFrame(cancelRequestAnimationFrame4)

            CanvasData.playing = !CanvasData.playing
            if (CanvasData.audio !== null) {
                let audio = document.getElementById(CanvasData.audio.elementId)
                audio.pause()
            }
            CanvasData.multipleCanvas.forEach(data => {
                data.arrayOfElements.forEach(element => {
                    if (element.type === "video") {
                        let video = document.getElementById(element.elementId)
                        video.pause()
                    }
                })
            })
        }
    }

    return (
        <div className="art-board">
            <div className="header-bar">
                <div>
                    <span>✂  Split</span>
                    <span>🔊  Volume</span>
                </div>

                <div className="play-container">
                    <img src={Previous} alt="" />
                    <img src={CanvasData.playing ? Pause : Play} alt="" onClick={() => toogleCanvasPlaying()} />
                    <span>{time}</span>
                    <img src={Next} alt="" />
                </div>

                <div onClick={viewImageFull}>
                    <span>🔭  50%</span>
                    <span>📺  Full Screen</span>
                </div>
            </div>

            <div className="layer">
                <span>Object</span>
                <span className="layer-name">|| {CanvasData.activeObject === null ? "Canvas" : CanvasData.activeObject.name} ||</span>
            </div>

            <div className="artboard-seen">
                <span>Artboard</span>
                {
                    CanvasData.multipleCanvas.map((data, index) => {
                        return <img onClick={() => {
                            editCanvas(data)
                            CanvasData.activeObject = null
                        }} key={"artboard" + index} src={data.image} alt="" />
                    })
                }
                <button className="add" onClick={addCanvas}>+</button>
            </div>

            <div className="audio-container">
                <span>Audio</span>
                <span className={CanvasData.audio === null ? "hide" : "audio"}>{CanvasData.audio === null ? "" : CanvasData.audio.name}</span>
                {/*  <button className="add">+</button> */}
            </div>

            <img src="" alt="" id="image-preview" />
        </div>
    )
}

export default ArtBoard

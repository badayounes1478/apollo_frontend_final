import React, { useEffect, useState } from 'react'
import '../reusablecomponentscss/FontsContainer.css'
import WebFont from 'webfontloader'
import Axios from 'axios'
import CanvasData from '../../Canvas'
import { draw } from '../../functions/DrawElementsOnCanvas'

const FontsContainer = (props) => {

    const [fonts, setfonts] = useState([])
    const [search, setsearch] = useState([])

    useEffect(() => {
        getTheFontsList()
    }, [])

    const getTheFontsList = async () => {
        try {
            const font = await Axios.get("https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyBn6luyYCNgtZymHWzLkiS-d4iuFwwaBgQ")
            setfonts(font.data.items)
            setsearch(font.data.items)
        } catch (error) {
            console.log(error.response.data)
        }
    }

    const loadTheFontAndAssingToText = (fontFamily) => {
        WebFont.load({
            google: {
                families: [fontFamily]
            },
            timeout: 1000
        });
        CanvasData.activeObject.fontFamily = fontFamily
        setTimeout(() => {
            draw()
        }, 1100);
    }

    const filterSearch = (searchQuery) => {
        let fontSearch = searchQuery.toLowerCase();
        const result = fonts.filter((data) => {
            return data.family.toLowerCase().search(fontSearch) !== -1
        })
        setsearch(result)
    }

    return (
        <div className="font-container">
            <div className="header-font" onClick={() => props.toggleShowFontContainer()}>
                <span>← </span>
                <span>Fonts</span>
            </div>
            <input
                onChange={(e) => filterSearch(e.target.value)}
                className="left-search-box"
                placeholder="Search Fonts" type="text" />
            <div className="fonts-list">
                {
                    search.map((data, index) => {
                        return <div
                            onClick={() => loadTheFontAndAssingToText(data.family)}
                            key={"fontfamily" + index}
                            className="font-name">{data.family}</div>
                    })
                }
            </div>
        </div>
    )
}

export default FontsContainer

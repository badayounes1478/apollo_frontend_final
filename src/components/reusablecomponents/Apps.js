import React from 'react'
import '../reusablecomponentscss/Apps.css'

const Apps = () => {
    return (
        <div className="apps-container">
            <div className="left-header-title">Apps</div>
            <input className="left-search-box" placeholder="Search" type="text" />

            <div className="cards">
                <span>
                    <img src="https://cdn.pixabay.com/photo/2021/05/03/13/38/fairy-flower-6226285__340.jpg" alt="" />
                    <div className="title">Muse</div>
                    <div className="subtitle">graphic designer</div>
                </span>
                <span>
                    <img src="https://cdn.pixabay.com/photo/2021/05/03/13/38/fairy-flower-6226285__340.jpg" alt="" />
                    <div className="title">Muse</div>
                    <div className="subtitle">graphic designer</div>
                </span>
            </div>

            <div className="cards">
                <img src="https://cdn.pixabay.com/photo/2021/05/03/13/38/fairy-flower-6226285__340.jpg" alt="" />
                <img src="https://cdn.pixabay.com/photo/2021/05/03/13/38/fairy-flower-6226285__340.jpg" alt="" />
            </div>
        </div>
    )
}

export default Apps

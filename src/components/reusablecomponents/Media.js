import React, { useState, useEffect, useContext } from 'react'
import Axios from 'axios'
import '../reusablecomponentscss/Media.css'
import { addImageInCanvas, addVideoInCanvas } from '../../functions/AddElementToCanvas'
import CanvasData from '../../Canvas'
import { draw } from '../../functions/DrawElementsOnCanvas'
import constant from '../../constant'
import AppContext from '../../store/DataProvider'

const Media = () => {

    const [active, setactive] = useState(1)
    const [images, setimages] = useState([])
    const [videos, setvideos] = useState([])
    const [videoLoading, setvideoLoading] = useState(false)
    const [videoPage, setvideoPage] = useState(1)
    const [page, setpage] = useState(1)
    const [searchQuery, setsearchQuery] = useState("")
    const [searchQueryVideos, setsearchQueryVideos] = useState("")
    const [loading, setloading] = useState(false)
    const [audio, setaudio] = useState([])
    const context = useContext(AppContext)

    useEffect(() => {
        getTheImages()
        getTheVideos()
        getTheAudio()
    }, [])// eslint-disable-line react-hooks/exhaustive-deps

    const getTheVideos = async () => {
        try {
            setvideoLoading(true)
            const vid = await Axios.get(`https://pixabay.com/api/videos/?key=17413650-b71223c8282f00889b36892d3&page=${videoPage}&editors_choice=true`)
            setvideoPage(videoPage + 1)
            setvideos([...videos, ...vid.data.hits])
            setvideoLoading(false)
        } catch (error) {
            setvideoLoading(false)
            console.log(error)
        }
    }

    const getTheImages = async () => {
        try {
            setloading(true)
            const image = await Axios.get(`${constant.imagesURL}images?page=${page}&q=${searchQuery}`)
            setimages([...images, ...image.data])
            setpage((prev) => prev + 1)
            setloading(false)
        } catch (error) {
            setloading(false)
        }
    }

    const getTheAudio = async () => {
        try {
            const audio = await Axios.get(constant.url + "audio")
            setaudio(audio.data)
        } catch (error) {
            console.log(error)
        }
    }

    const search = async (e) => {
        try {
            e.preventDefault()
            setloading(true)
            const image = await Axios.get(`${constant.imagesURL}images/search?query=${searchQuery}`)
            setimages([...image.data, ...images])
            setloading(false)
        } catch (error) {
            setloading(false)
            console.log(error.response.data)
        }
    }

    const searchVideos = async (e) => {
        try {
            e.preventDefault()
            setvideoLoading(true)
            const vid = await Axios.get(`https://pixabay.com/api/videos/?key=17413650-b71223c8282f00889b36892d3&page=1&editors_choice=true&q=${searchQueryVideos}`)
            setvideoPage(videoPage + 1)
            setvideos([...vid.data.hits, ...videos])
            setvideoLoading(false)
        } catch (error) {
            setvideoLoading(false)
            console.log(error)
        }
    }

    const toDataURL = url => fetch(url)
        .then(response => response.blob())
        .then(blob => new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.onloadend = () => resolve(reader.result)
            reader.onerror = reject
            reader.readAsDataURL(blob)
        }))

    const checkSvgOrImage = async (id, url) => {
        if (CanvasData.activeObject === null) {
            addImageInCanvas(id, url)
            return
        }
        if (CanvasData.activeObject.type === "svg") {
            if (CanvasData.activeObject.mask === true) {
                CanvasData.activeObject.elementId2 = id
                CanvasData.activeObject.src1 = url
                CanvasData.activeObject.base641 = null
                CanvasData.activeObject.type = "mask"
                CanvasData.activeObject.name = "Mask"
                CanvasData.activeObject.mask = true
                draw()
                CanvasData.activeObject.base641 = await toDataURL(url)
            } else {
                addImageInCanvas(id, url)
            }
        } else {
            if (CanvasData.activeObject.type === "mask") {
                CanvasData.activeObject.elementId2 = id
                CanvasData.activeObject.src1 = url
                CanvasData.activeObject.base641 = null
                draw()
                CanvasData.activeObject.base641 = await toDataURL(url)
            } else {
                addImageInCanvas(id, url)
            }
        }
    }

    const addAudio = (url, id, name) => {
        let audio = {
            id: new Date().getTime(), elementId: id, start: null, end: null, type: "audio", src: url, name: name
        }
        CanvasData.audio = audio
        context.refreshTheScreen()
    }


    return (
        <div className="media-container">
            <div className="media-header">
                <span onClick={() => setactive(1)} className={active === 1 ? "active" : null}>Photo</span>
                <span onClick={() => setactive(2)} className={active === 2 ? "active" : null}>Video</span>
                <span onClick={() => setactive(3)} className={active === 3 ? "active" : null}>Music</span>
            </div>

            <div className={active === 1 ? "show" : "hide"}>
                <form onSubmit={search}>
                    <input
                        onChange={(e) => setsearchQuery(e.target.value)}
                        className="left-search-box"
                        placeholder="Search" type="text" />
                </form>

                <div className="media-image-container">
                    {
                        images.map((data, index) => {
                            return <img
                                id={data.id}
                                crossOrigin="anonymous"
                                onClick={() => checkSvgOrImage(data.id, data.url)}
                                key={"image" + index}
                                src={data.url} alt="" />
                        })
                    }
                </div>
                <div className="loader-container" style={loading ? null : { display: 'none' }}>
                    <div className="loader"></div>
                </div>
                <button onClick={getTheImages} className="load-more"
                    style={loading ? { display: 'none' } : null}>Load More</button>
            </div>


            <div className={active === 2 ? "show" : "hide"}>
                <form onSubmit={searchVideos}>
                    <input
                        onChange={(e) => setsearchQueryVideos(e.target.value)}
                        className="left-search-box"
                        placeholder="Search" type="text" />
                </form>
                <div className="media-image-container">
                    {
                        videos.map((data, index) => {
                            return <video
                                muted={true}
                                key={"video" + index}
                                id={data.picture_id.toString()}
                                src={data.videos.tiny.url}
                                controls={false}
                                onClick={() => addVideoInCanvas(data.picture_id.toString(), data.videos.tiny.url)}
                                crossOrigin="anonymous" />
                        })
                    }
                </div>
                <button onClick={getTheVideos} className="load-more"
                    style={videoLoading ? { display: 'none' } : null}>Load More</button>
            </div>


            <div className={active === 3 ? "show" : "hide"}>
                <form onSubmit={searchVideos}>
                    <input
                        onChange={(e) => setsearchQueryVideos(e.target.value)}
                        className="left-search-box"
                        placeholder="Search" type="text" />
                </form>
                {
                    audio.map((data, index) => {
                        return <div key={"audio" + index} className="audio-card">
                            <audio id={data._id} controls controlsList="nodownload" src={data.url} />
                            <button onClick={() => addAudio(data.url, data._id, data.file_name)}>+</button>
                        </div>
                    })
                }
                <button onClick={getTheVideos} className="load-more"
                    style={videoLoading ? { display: 'none' } : null}>Load More</button>
            </div>
        </div>
    )
}

export default Media

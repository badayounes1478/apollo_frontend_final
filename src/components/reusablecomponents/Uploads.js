import React, { useRef, useEffect, useState, useContext } from 'react'
import '../reusablecomponentscss/Uploads.css'
import jwt_decode from "jwt-decode";
import constant from '../../constant'
import { addImageInCanvas } from '../../functions/AddElementToCanvas'
import CanvasData from '../../Canvas'
import { draw } from '../../functions/DrawElementsOnCanvas'
import AppContext from '../../store/DataProvider'
import axios from 'axios';

const Uploads = () => {

    const inputContainer = useRef(null)
    const [userUploads, setuserUploads] = useState([])
    const context = useContext(AppContext)

    useEffect(() => {
        getTheUserUploads()
    }, [])

    const getTheUserUploads = async () => {
        try {
            const decoded = jwt_decode(localStorage.getItem('token'))
            const res = await axios.get(constant.url + 'uploads/' + decoded.id)
            setuserUploads(res.data)
        } catch (error) {
            console.log(error)
        }
    }

    const uploadImage = async (e) => {
        const file = e.target.files[0]
        const decoded = jwt_decode(localStorage.getItem('token'))

        const formData = new FormData();
        formData.append('userId', decoded.id);
        formData.append('media', file)

        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }

        try {
            const res = await axios.post(constant.url + 'uploads', formData, config)
            setuserUploads([res.data, ...userUploads])
        } catch (error) {
            console.log(error)
        }
    }

    const toDataURL = url => fetch(url)
        .then(response => response.blob())
        .then(blob => new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.onloadend = () => resolve(reader.result)
            reader.onerror = reject
            reader.readAsDataURL(blob)
        }))

    const addElement = async (data) => {
        if (data.mimetype === "image/svg+xml") {
            const svg = document.getElementById(data._id)
            let svgElement = {
                id: Date.now(), name: "Svg", elementId: Date.now(), x: 10, y: 10, width: svg.width, height: svg.height, base64: null,
                type: "svg", src: data.url, rotate: 0, blur: 0, opacity: 1.0, lock: false, horizontalFlip: false, colorObject: [], colorObjectSet: false,
                verticalFlip: false, overlayImage: false, colors: [], flag: 0, display: true, filterName: "None", blobURL: null,
                shadowOffsetX: 0, shadowOffsetY: 0, shadowColor: '#000000', shadowBlur: 0, mask: false, blobCompleted: false,
                brightness: '100%', contrast: '100%', grayscale: '0%', saturate: '100%', sepia: '0%', invert: '0%',
                startTime: null, endTime: null
            }
            CanvasData.arrayOfElements = [...CanvasData.arrayOfElements, svgElement]
            let activeCanvas = CanvasData.multipleCanvas.find(data => data.id === CanvasData.design_id)
            activeCanvas.arrayOfElements = CanvasData.arrayOfElements
            context.refreshTheScreen()
            setTimeout(() => {
                draw()
            }, 300);
            svgElement.base64 = await toDataURL(data.url)
        } else {
            let id = data._id
            let url = data.url
            if (CanvasData.activeObject === null) {
                addImageInCanvas(id, url)
                return
            }
            if (CanvasData.activeObject.type === "svg") {
                if (CanvasData.activeObject.mask === true) {
                    CanvasData.activeObject.elementId2 = id
                    CanvasData.activeObject.src1 = url
                    CanvasData.activeObject.base641 = null
                    CanvasData.activeObject.type = "mask"
                    CanvasData.activeObject.name = "Mask"
                    CanvasData.activeObject.mask = true
                    draw()
                    CanvasData.activeObject.base641 = await toDataURL(url)
                } else {
                    addImageInCanvas(id, url)
                }
            } else {
                if (CanvasData.activeObject.type === "mask") {
                    CanvasData.activeObject.elementId2 = id
                    CanvasData.activeObject.src1 = url
                    CanvasData.activeObject.base641 = null
                    draw()
                    CanvasData.activeObject.base641 = await toDataURL(url)
                } else {
                    addImageInCanvas(id, url)
                }
            }
        }
    }

    return (
        <div className="upload-container">
            <div className="left-header-title">Uploads</div>
            <input type="file" name="myImage" accept="image/*"
                ref={inputContainer}
                style={{ opacity: 0, height: 0, width: 0 }}
                onChange={(e) => uploadImage(e)} />
            <button className="upload-button" onClick={() => inputContainer.current.click()}>Upload Media</button>
            <div className="grid">
                {
                    userUploads.map(data => {
                        return <img id={data._id} crossOrigin="anonymous" key={data._id} src={data.url} alt="" onClick={() => addElement(data)} />
                    })
                }
            </div>
        </div>
    )
}

export default Uploads

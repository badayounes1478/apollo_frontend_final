import React, { useContext } from 'react'
import '../reusablecomponentscss/Animation.css'
//import UpArrow from '../../assets/UpArrow.svg'
import DownArrow from '../../assets/DownArrow.svg'
import InAnimation from '../../assets/InAnimation.svg'
import InPlaceAnimation from '../../assets/InPlaceAnimation.svg'
import OutAnimation from '../../assets/OutAnimation.svg'
import ShowFilter from '../../assets/Animation.png'
import Clock from '../../assets/Clock.svg'
import { fadeIn, fadeOut, fromBottom, fromLeft, fromRight, fromTop, rotate } from '../../functions/AddAnimation'
import CanvasData from '../../Canvas'
import AppContext from '../../store/DataProvider'

const Animation = () => {

    const context = useContext(AppContext)

    const changeRotateSpeed = (e) => {
        CanvasData.activeObject.animation.speed = 12 / parseFloat(e.target.value)//12 because after 30 roundes it reaches 360
        CanvasData.activeObject.animation.time = parseFloat(e.target.value)
        context.refreshTheScreen()
    }

    const changeFromLeftSpeed = (e) => {
        let speed = (CanvasData.activeObject.width + CanvasData.activeObject.x) / (30 * parseFloat(e.target.value))
        CanvasData.activeObject.animation.speed = speed
        CanvasData.activeObject.animation.time = parseFloat(e.target.value)
        context.refreshTheScreen()
    }

    const changeFromRightSpeed = (e) => {
        let speed = (CanvasData.canvasWidth - CanvasData.activeObject.x) / (30 * parseFloat(e.target.value))
        CanvasData.activeObject.animation.speed = speed
        CanvasData.activeObject.animation.time = parseFloat(e.target.value)
        context.refreshTheScreen()
    }

    const changeFromTopSpeed = (e) => {
        let speed = (CanvasData.activeObject.height + CanvasData.activeObject.y) / (30 * parseFloat(e.target.value))
        CanvasData.activeObject.animation.speed = speed
        CanvasData.activeObject.animation.time = parseFloat(e.target.value)
        context.refreshTheScreen()
    }

    const changeFromBottomSpeed = (e) => {
        let speed = (CanvasData.canvasHeight - CanvasData.activeObject.y) / (30 * parseFloat(e.target.value))
        CanvasData.activeObject.animation.speed = speed
        CanvasData.activeObject.animation.time = parseFloat(e.target.value)
        context.refreshTheScreen()
    }

    const changeFadeInSpeed = (e) => {
        let speed = 1 / (30 * parseFloat(e.target.value))
        CanvasData.activeObject.animation.speed = speed
        CanvasData.activeObject.animation.time = parseFloat(e.target.value)
        context.refreshTheScreen()
    }

    const changeFadeOutSpeed = (e) => {
        let speed = 1 / (30 * parseFloat(e.target.value))
        CanvasData.activeObject.animation.speed = speed
        CanvasData.activeObject.animation.time = parseFloat(e.target.value)
        context.refreshTheScreen()
    }


    const checkAnimationType = (e) => {
        if (CanvasData.activeObject === null) return
        if (CanvasData.activeObject.animation === undefined) return
        if (CanvasData.activeObject.animation.type === "fromLeft") {
            changeFromLeftSpeed(e)
        } else if (CanvasData.activeObject.animation.type === "fromRight") {
            changeFromRightSpeed(e)
        } else if (CanvasData.activeObject.animation.type === "fromTop") {
            changeFromTopSpeed(e)
        } else if (CanvasData.activeObject.animation.type === "fromBottom") {
            changeFromBottomSpeed(e)
        } else if (CanvasData.activeObject.animation.type === "fadeIn") {
            changeFadeInSpeed(e)
        } else if (CanvasData.activeObject.animation.type === "fadeOut") {
            changeFadeOutSpeed(e)
        } else if (CanvasData.activeObject.animation.type === "rotate") {
            changeRotateSpeed(e)
        }
    }

    const setAnimationToElement = (animationFunction) => {
        animationFunction()
        context.refreshTheScreen()
    }

    return (
        <div className="animation-container">
            <div className="header" style={{ marginTop: 10 }}>
                <span>Animate</span>
                <img src={DownArrow} alt="" />
            </div>

            <div className="animation-button-container">
                <button>
                    <img src={InAnimation} alt="" />
                    <span>In</span>
                </button>
                <button>
                    <img src={InPlaceAnimation} alt="" />
                    <span>In Place</span>
                </button>
                <button>
                    <img src={OutAnimation} alt="" />
                    <span>Out</span>
                </button>
            </div>

            <div className="animation-types-container">
                <div className="non-container" onClick={()=> {
                    CanvasData.activeObject.animation = undefined
                    context.refreshTheScreen()
                }}>
                    <label>None</label>
                    <img src={ShowFilter} alt="" />
                </div>
                <div className="animation-container-grid">
                    <div className="non-container" onClick={() => setAnimationToElement(fromLeft)}>
                        <label>From Left</label>
                        <img src={ShowFilter} alt="" />
                    </div>
                    <div className="non-container" onClick={() => setAnimationToElement(fromRight)}>
                        <label>From Right</label>
                        <img src={ShowFilter} alt="" />
                    </div>
                    <div className="non-container" onClick={() => setAnimationToElement(fromTop)}>
                        <label>From Top</label>
                        <img src={ShowFilter} alt="" />
                    </div>
                    <div className="non-container" onClick={() => setAnimationToElement(fromBottom)}>
                        <label>From Bottom</label>
                        <img src={ShowFilter} alt="" />
                    </div>
                    <div className="non-container" onClick={() => setAnimationToElement(fadeIn)}>
                        <label>Fade In</label>
                        <img src={ShowFilter} alt="" />
                    </div>
                    <div className="non-container" onClick={() => setAnimationToElement(fadeOut)}>
                        <label>Fade Out</label>
                        <img src={ShowFilter} alt="" />
                    </div>
                    <div className="non-container" onClick={() => setAnimationToElement(rotate)}>
                        <label>Rotate</label>
                        <img src={ShowFilter} alt="" />
                    </div>
                </div>
            </div>

            <div className="animation-duration-container">
                <div className="title-duration">Animation Duration</div>
                <div className="slider-container">
                    <img src={Clock} alt="" />
                    <input onChange={checkAnimationType} value={CanvasData.activeObject === null ? 1 : CanvasData.activeObject.animation === undefined ? 1 : CanvasData.activeObject.animation.time} type="range" min="0.1" max="5.0" step="0.1" className="slider" />
                    <span>{CanvasData.activeObject === null ? "" : CanvasData.activeObject.animation === undefined ? "" : CanvasData.activeObject.animation.time}s</span>
                </div>
            </div>
        </div>
    )
}

export default Animation

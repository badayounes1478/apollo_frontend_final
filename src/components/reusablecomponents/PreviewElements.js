import React from 'react'
import CanvasData from '../../Canvas'
import '../reusablecomponentscss/PreviewElements.css'

const PreviewElements = () => {
    return (
        <div className="preview-elements">
            {
                CanvasData.multipleCanvas.map(data => {
                    return data.arrayOfElements.map((data, index) => {
                        if (data.type === 'image') {
                            return <img key={"previewImage" + index}
                                crossOrigin="anonymous"
                                style={{ height: 100, width: 'auto' }}
                                id={data.elementId}
                                src={data.base64 === null ? data.src : data.base64} alt="" />
                        } else if (data.type === "svg") {
                            return <React.Fragment key={"previewSvg" + index}>
                                <img
                                    crossOrigin="anonymous"
                                    style={{ height: 100, width: 'auto' }}
                                    data={data.elementId}
                                    id={data.elementId}
                                    src={data.base64 === null ? data.src : data.base64} alt="" />
                                <object
                                    style={{ height: 100, width: 'auto' }}
                                    id={data.elementId + "svg"}
                                    data={data.blobURL === null ? "" : data.blobURL}
                                    type="image/svg+xml"
                                    aria-label="mayur"
                                    crossOrigin="anonymous"
                                />
                                <img
                                    crossOrigin="anonymous"
                                    style={{ height: 100, width: 'auto' }}
                                    id={data.elementId + 'svgsecret'}
                                    src="" alt="" />
                            </React.Fragment>
                        } else if (data.type === "mask") {
                            return <React.Fragment key={"previewMask"+index}>
                                <img
                                    crossOrigin="anonymous"
                                    style={{ height: 100, width: 'auto' }}
                                    id={data.elementId}
                                    data={data.elementId}
                                    src={data.base64 === null ? data.src : data.base64} alt="" />
                                <img
                                    crossOrigin="anonymous"
                                    style={{ height: 100, width: 'auto' }}
                                    id={data.elementId2}
                                    data={data.elementId2}
                                    src={data.base641 === null ? data.src1 : data.base641} alt="" />
                            </React.Fragment>
                        } if (data.type === "group") {
                            return data.data.map((data1, index) => {
                                if (data1.type === "image") {
                                    return <img
                                        key={"groupImage" + index}
                                        crossOrigin="anonymous"
                                        style={{ height: 100, width: 'auto' }}
                                        id={data1.elementId}
                                        src={data1.base64 === null ? data1.src : data1.base64} alt="" />
                                } else if (data1.type === "svg") {
                                    return <React.Fragment key={"groupSvg" + index}>
                                        <img
                                            crossOrigin="anonymous"
                                            style={{ height: 100, width: 'auto' }}
                                            data={data1.elementId}
                                            id={data1.elementId}
                                            src={data1.base64 === null ? data1.src : data1.base64} alt="" />
                                        <object
                                            style={{ height: 100, width: 'auto' }}
                                            id={data1.elementId + "svg"}
                                            data={data1.blobURL === null ? "" : data1.blobURL}
                                            type="image/svg+xml"
                                            aria-label="mayur"
                                            crossOrigin="anonymous"
                                        />
                                        <img
                                            crossOrigin="anonymous"
                                            style={{ height: 100, width: 'auto' }}
                                            id={data1.elementId + 'svgsecret'}
                                            src="" alt="" />
                                    </React.Fragment>
                                } else if (data1.type === "mask") {
                                    return <React.Fragment key={"groupMask" + index}>
                                        <img
                                            crossOrigin="anonymous"
                                            style={{ height: 100, width: 'auto' }}
                                            data={data1.elementId}
                                            id={data1.elementId}
                                            src={data1.base64 === null ? data1.src : data1.base64} alt="" />
                                        <img
                                            crossOrigin="anonymous"
                                            style={{ height: 100, width: 'auto' }}
                                            data={data1.elementId2}
                                            id={data1.elementId2}
                                            src={data1.base641 === null ? data1.src : data1.base641} alt="" />
                                    </React.Fragment>
                                } else {
                                    return null
                                }
                            })
                        } else {
                            return null
                        }
                    })
                })
            }
        </div>
    )
}

export default PreviewElements

import React, { useContext, useState } from 'react'
import Element1 from '../../assets/Element1.svg'
import Element2 from '../../assets/Element2.svg'
import Element3 from '../../assets/Element3.svg'
import Element4 from '../../assets/Element4.svg'
import CanvasData from '../../Canvas'
import Duplicate from '../../assets/Duplicate.svg'
import SendFront from '../../assets/SendFront.svg'
import SendBack from '../../assets/SendBack.svg'
import Delete from '../../assets/Delete.svg'
import Rotate from '../../assets/Rotate.svg'
import Lock from '../../assets/Lock.svg'
import Lock2 from '../../assets/Lock2.svg'
import Unlock from '../../assets/Unlock.svg'
import Eye from '../../assets/Eye.svg'
import EyeOff from '../../assets/EyeOff.svg'
import UpArrow from '../../assets/UpArrow.svg'
import DownArrow from '../../assets/DownArrow.svg'

//text functionality elements
import Alignment from '../../assets/Alignment.svg'
import AlignmentCenter from '../../assets/AlignmentCenter.svg'
import AlignmentRight from '../../assets/AlignmentRight.svg'
import List from '../../assets/List.svg'
import Underline from '../../assets/Underline.svg'
import Italic from '../../assets/Italic.svg'
import Bold from '../../assets/Bold.svg'

//blend
import Blend from '../../assets/Blend.svg'
import Settings from '../../assets/Settings.svg'

//layout
import HorizontalFlip from '../../assets/HorizontalFlip.svg'
import VerticalFlip from '../../assets/VerticalFlip.svg'
import Crop from '../../assets/Crop.svg'

//Filter
import Filter from '../../assets/Filter.svg'
import Adjust from '../../assets/Adjust.svg'

import '../reusablecomponentscss/EditBox.css'
import AppContext from '../../store/DataProvider'
import { draw } from '../../functions/DrawElementsOnCanvas'
import {
    duplicateElement, deleteElement, sendFront, sendBack, displayElementToggle,
    toggleListText, boldText, italicText, flip, lockAndUnlock, changeSvgColors,
} from '../../functions/ElementsCommonFunctions'
import ImageCropModal from './ImageCropModal'
import FontsContainer from './FontsContainer'
import Filters from './Filters'
import Animation from './Animation'

const EditBox = () => {

    const context = useContext(AppContext)
    const [toggleAnimation, settoggleAnimation] = useState(false)
    const [colorToChangeSvg, setcolorToChangeSvg] = useState(null)
    const [cropImage, setcropImage] = useState(false)
    const [showFonts, setshowFonts] = useState(false)
    const [filter, setfilter] = useState(false)
    const [copy, setcopy] = useState(null)
    const [state, setstate] = useState(true)
    const [layout, setlayout] = useState(false)
    const [filterContainer, setfilterContainer] = useState(false)
    const [blend, setblend] = useState(false)
    const [effect, seteffect] = useState(false)
    const [text, settext] = useState(true)
    const [fill, setfill] = useState(false)
    const [color, setcolor] = useState(false)
    const [adjustFilter, setadjustFilter] = useState(false)
    const [shadowBox, setshadowBox] = useState(false)
    const [blurBox, setblurBox] = useState(false)

    const fontWeight = ["normal", "bold", "bolder", "lighter", "100", "200", "300", "400", "500", "600", "700", "800", "900"]


    const paste = () => {
        if (copy === null) return
        let data = {
            ...copy
        }
        data.id = Date.now()
        data.x = data.x + 10
        data.y = data.y + 10
        CanvasData.arrayOfElements = [...CanvasData.arrayOfElements, data]
        draw()
        context.refreshTheScreen()
    }

    const copyElement = () => {
        setcopy(CanvasData.activeObject)
    }

    const editTheText = (e) => {
        if (CanvasData.activeObject.type !== "text") return
        CanvasData.activeObject.content = e.target.value
        draw()
        context.refreshTheScreen()
    }

    const changePosition = (type, num) => {
        if (CanvasData.activeObject === null) return
        if (type === "x") {
            CanvasData.activeObject.x = parseInt(num)
        } else if (type === "y") {
            CanvasData.activeObject.y = parseInt(num)
        } else if (type === "rotate") {
            CanvasData.activeObject.rotate = parseInt(num)
        } else if (type === "width") {
            if (CanvasData.activeObject.type === "text") return
            CanvasData.activeObject.width = parseInt(num)
        } else if (type === "height") {
            if (CanvasData.activeObject.type === "text") return
            CanvasData.activeObject.height = parseInt(num)
        }
        draw()
        context.refreshTheScreen()
    }

    const changeTextColor = (e) => {
        if (CanvasData.activeObject === null) return
        if (CanvasData.activeObject.type !== "text") return
        CanvasData.activeObject.color = e.target.value
        draw()
        context.refreshTheScreen()
    }

    const changeShadowColor = (e) => {
        if (CanvasData.activeObject === null) return
        CanvasData.activeObject.shadowColor = e.target.value
        draw()
        context.refreshTheScreen()
    }

    const changeShadow = (type, num) => {
        if (CanvasData.activeObject === null) return
        if (type === "x") {
            CanvasData.activeObject.shadowOffsetX = parseInt(num)
        } else if (type === "y") {
            CanvasData.activeObject.shadowOffsetY = parseInt(num)
        } else if (type === "blur") {
            CanvasData.activeObject.shadowBlur = parseInt(num)
        } else if (type === "blurElement") {
            CanvasData.activeObject.blur = parseInt(num)
        }
        draw()
        context.refreshTheScreen()
    }


    const letterSpacing = (value) => {
        CanvasData.activeObject.letterSpacing = parseInt(value)
        draw()
        context.refreshTheScreen()
    }

    const lineHeight = (value) => {
        CanvasData.activeObject.lineHeight = parseInt(value)
        draw()
        context.refreshTheScreen()
    }

    const capitalizeText = () => {
        if (CanvasData.activeObject === null) return
        if (CanvasData.activeObject.textStyle === "user") {
            CanvasData.activeObject.content = CanvasData.activeObject.content.toUpperCase()
            CanvasData.activeObject.textStyle = "capital"
        } else if (CanvasData.activeObject.textStyle === "capital") {
            CanvasData.activeObject.content = CanvasData.activeObject.content.toLowerCase()
            CanvasData.activeObject.textStyle = "small"
        } else if (CanvasData.activeObject.textStyle === "small") {
            CanvasData.activeObject.content = CanvasData.activeObject.content.toUpperCase()
            CanvasData.activeObject.textStyle = "capital"
        }
        draw()
        context.refreshTheScreen()
    }

    const textAlignment = () => {
        if (CanvasData.activeObject === null) return
        if (CanvasData.activeObject.type !== "text") return
        if (CanvasData.activeObject.align === "left") {
            CanvasData.activeObject.align = "center"
            CanvasData.activeObject.baseline = "bottom"
        } else if (CanvasData.activeObject.align === "center") {
            CanvasData.activeObject.align = "right"
            CanvasData.activeObject.baseline = "top"
        } else {
            CanvasData.activeObject.align = "left"
            CanvasData.activeObject.baseline = "top"
        }
        draw()
        context.refreshTheScreen()
    }

    const changeSvgColor = (colorToChange) => {
        const colorPicker = document.getElementById('svgcolorchanger')
        colorPicker.focus()
        setcolorToChangeSvg(() => colorToChange)
        colorPicker.click()
    }

    const getTheSvgColorToChange = (e) => {
        const svgIdToChange = CanvasData.activeObject.elementId
        changeSvgColors(CanvasData.activeObject, e.target.value, colorToChangeSvg, svgIdToChange)
        setcolorToChangeSvg(() => e.target.value)
        draw()
        setTimeout(() => {
            draw()
            context.refreshTheScreen()
        }, 100);
    }

    const changeOpacity = (value) => {
        CanvasData.activeObject.opacity = parseInt(value) / 100
        draw()
        context.refreshTheScreen()
    }

    const changeElementName = (value) => {
        CanvasData.activeObject.name = value
        draw()
        context.refreshTheScreen()
    }

    const borderRadiusToImage = (value) => {
        if (value === "") {
            CanvasData.activeObject.radius = 0
        } else {
            CanvasData.activeObject.radius = parseInt(value)
        }
        draw()
        context.refreshTheScreen()
    }

    const changeLockState = () => {
        lockAndUnlock()
        context.refreshTheScreen()
    }

    const changeShowState = () => {
        displayElementToggle()
        context.refreshTheScreen()
    }

    const toggleShowFontContainer = () => {
        setshowFonts(!showFonts)
    }

    const changeFontWeight = (data) => {
        CanvasData.activeObject.bold = data
        draw()
        context.refreshTheScreen()
    }

    const changeFontSize = (data) => {
        CanvasData.activeObject.size = parseInt(data)
        draw()
        context.refreshTheScreen()
    }

    const changeFilterAdjustment = (name, data) => {
        if (name === 'brightness') {
            CanvasData.activeObject.brightness = data + '%'
        } else if (name === 'contrast') {
            CanvasData.activeObject.contrast = data + '%'
        } else if (name === 'saturate') {
            CanvasData.activeObject.saturate = data + '%'
        } else if (name === 'invert') {
            CanvasData.activeObject.invert = data + '%'
        } else if (name === 'grayscale') {
            CanvasData.activeObject.grayscale = data + '%'
        } else if (name === 'sepia') {
            CanvasData.activeObject.sepia = data + '%'
        }
        draw()
        context.refreshTheScreen()
    }

    const deleteObjectElement = () => {
        deleteElement()
        CanvasData.activeObject = null
        context.refreshTheScreen()
    }

    const checkSvgOrMaskAndSetImageOrRemove = () => {
        if (CanvasData.activeObject.type === "mask") {
            delete CanvasData.activeObject.elementId2
            delete CanvasData.activeObject.src1
            CanvasData.activeObject.type = "svg"
            CanvasData.activeObject.name = "svg"
            CanvasData.activeObject.mask = false

            let activeCanvas = CanvasData.multipleCanvas.find(data => data.id === CanvasData.design_id)
            activeCanvas.arrayOfElements = CanvasData.arrayOfElements
            context.refreshTheScreen()
            draw()
            setTimeout(() => {
                draw()
            }, 100);
            setTimeout(() => {
                draw()
            }, 500);
            setTimeout(() => {
                draw()
            }, 1000);
        } else {
            CanvasData.activeObject.mask = !CanvasData.activeObject.mask
            draw()
            context.refreshTheScreen()
        }
    }

    const changeBarColor = (index, e) => {
        CanvasData.activeObject.data[index].color = e.target.value
        draw()
        context.refreshTheScreen()
    }

    const changeValueOfbar = (index, e) => {
        CanvasData.activeObject.data[index].marks = parseInt(e.target.value)
        draw()
        context.refreshTheScreen()
    }

    return (
        <div className="design-container">
            <div className="title">
                <span style={!toggleAnimation ? { fontWeight: 500 } : null} onClick={() => settoggleAnimation(false)}>Edit</span>
                <span style={toggleAnimation ? { fontWeight: 500 } : null} onClick={() => settoggleAnimation(true)}>Animations</span>
            </div>
            {
                toggleAnimation === true ? <Animation /> :
                    <React.Fragment>
                        {filter === true ? <Filters toggleFilter={() => setfilter(false)} /> : null}
                        {showFonts === true ? <FontsContainer toggleShowFontContainer={toggleShowFontContainer} /> : null}
                        {
                            cropImage === true ? <ImageCropModal toggleCropModal={() => {
                                setcropImage(false)
                                setTimeout(() => {
                                    draw()
                                }, 100);
                            }} /> : null
                        }
                        <div style={showFonts === true || filter === true ? { display: 'none' } : null}>
                            {
                                CanvasData.activeObject === null ?
                                    <div className="first-container">
                                        <div className="element-name-container boderBottom">
                                            <span>Background canvas</span>
                                        </div>

                                        <div className="state">
                                            <div className="header">
                                                <span>State</span>
                                                <img src={UpArrow} alt="" />
                                            </div>
                                            <div className="canvas-state">
                                                <div>
                                                    <span className="state-title">W</span>
                                                    <span>1920</span>
                                                </div>
                                                <div>
                                                    <span className="state-title">H</span>
                                                    <span>1080</span>
                                                </div>
                                                <div>
                                                    <img src={Lock} alt="" />
                                                </div>
                                            </div>
                                            <div className="canvas-state">
                                                <div>
                                                    <span className="state-title">Duration</span>
                                                    <input
                                                        onChange={(e) => {
                                                            CanvasData.duration = e.target.value
                                                            let activeCanvas = CanvasData.multipleCanvas.find(data => data.id === CanvasData.design_id)
                                                            activeCanvas.duration = e.target.value
                                                            context.refreshTheScreen()
                                                        }}
                                                        value={CanvasData.duration}
                                                        type="number" />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="style">
                                            <div className="header">
                                                <span>Style</span>
                                                <img src={UpArrow} alt="" />
                                            </div>
                                            <div className="style-container">
                                                <div className="style-subContainer">
                                                    <span className="checkBox"></span>
                                                    <span>Solid</span>
                                                </div>

                                                <div className="style-subContainer">
                                                    <span className="checkBox"></span>
                                                    <span>Transparent</span>
                                                </div>
                                            </div>

                                            <div className="color-container">
                                                <div className="block-container">
                                                    <span className="block" style={{ backgroundColor: CanvasData.backgroundColor }}>
                                                        <input
                                                            style={{ opacity: 0 }}
                                                            value={CanvasData.backgroundColor}
                                                            type="color"
                                                            onChange={(e) => {
                                                                CanvasData.backgroundColor = e.target.value
                                                                draw()
                                                                context.refreshTheScreen()
                                                            }} />
                                                    </span>
                                                    <span>{CanvasData.backgroundColor}</span>
                                                </div>
                                                <span>100%</span>
                                            </div>
                                        </div>


                                        <div className="layout-canvas">
                                            <div className="header">
                                                <span>Layout</span>
                                                <img src={UpArrow} alt="" />
                                            </div>
                                            <div className="layout-subContainer">
                                                <span className="checkBox"></span>
                                                <span>Transparent</span>
                                            </div>
                                            <div className="layout-subContainer">
                                                <span className="checkBox"></span>
                                                <span>Clip Content</span>
                                            </div>
                                        </div>
                                    </div> : <>
                                        <div className="first-container">
                                            <div className="element-name-container">
                                                <input
                                                    onChange={(e) => changeElementName(e.target.value)}
                                                    value={CanvasData.activeObject.name} />
                                                <div className="hover-components">
                                                    <img onClick={changeLockState} src={CanvasData.activeObject.lock ? Unlock : Lock2} alt="" />
                                                    <img onClick={changeShowState} src={CanvasData.activeObject.display ? Eye : EyeOff} alt="" />
                                                </div>
                                            </div>
                                            <div className="icon-container">
                                                <img src={Element1} onClick={copyElement} alt="" />
                                                <img src={Element2} onClick={paste} alt="" />
                                                <img onClick={duplicateElement} src={Duplicate} alt="" />
                                                <img onClick={sendFront} src={SendFront} alt="" />
                                                <img onClick={sendBack} src={SendBack} alt="" />
                                                <img onClick={deleteObjectElement} src={Delete} alt="" />
                                            </div>
                                        </div>

                                        <div className="state">
                                            <div className="header" onClick={() => setstate(!state)}>
                                                <span>State</span>
                                                <img src={state ? UpArrow : DownArrow} alt="" />
                                            </div>
                                            <div className="coordinate-container" style={state ? null : { display: 'none' }}>
                                                <div className="state-container">
                                                    <span className="title1">
                                                        <div>X</div>
                                                        <div>W</div>
                                                        <img style={{ height: 14, width: 14 }} src={Rotate} alt="" />
                                                    </span>
                                                    <span>
                                                        <input type="number"
                                                            style={{ marginTop: 12 }}
                                                            onChange={(e) => changePosition("x", e.target.value)}
                                                            value={CanvasData.activeObject.x} /> <br />

                                                        <input type="number"
                                                            onChange={(e) => changePosition("width", e.target.value)}
                                                            value={CanvasData.activeObject.width} /> <br />

                                                        <input type="number"
                                                            onChange={(e) => changePosition("rotate", e.target.value)}
                                                            value={CanvasData.activeObject.rotate} />
                                                    </span>
                                                </div>
                                                <div className="state-container">
                                                    <span className="title1">
                                                        <div>Y</div>
                                                        <div>H</div>
                                                        <img style={{ height: 14, width: 14 }} src={Element3} alt="" />
                                                    </span>
                                                    <span>
                                                        <input type="number"
                                                            style={{ marginTop: 12 }}
                                                            onChange={(e) => changePosition("y", e.target.value)}
                                                            value={CanvasData.activeObject.y} /> <br />

                                                        <input type="number"
                                                            onChange={(e) => changePosition("height", e.target.value)}
                                                            value={CanvasData.activeObject.height} /> <br />

                                                        <input type="number"
                                                            onChange={(e) => borderRadiusToImage(e.target.value)}
                                                            value={CanvasData.activeObject.radius} />
                                                    </span>
                                                </div>
                                                <div className="state-container">
                                                    <span>
                                                        <div>&nbsp;</div>
                                                        <img src={Lock} alt="" /> <br />
                                                        <img src={Element4} alt="" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="duration-container">
                                            <div className="header">
                                                <span>Duration</span>
                                                <span>{CanvasData.activeObject.endTime - CanvasData.activeObject.startTime}</span>
                                            </div>

                                            <div className="duration-sub-container">
                                                <div>
                                                    <span>Start</span>
                                                    <input
                                                        onChange={(e) => {
                                                            CanvasData.activeObject.startTime = e.target.value
                                                            context.refreshTheScreen()
                                                        }}
                                                        type="number"
                                                        value={CanvasData.activeObject.startTime === null ? "" : CanvasData.activeObject.startTime} />
                                                </div>
                                                <div>
                                                    <span>End</span>
                                                    <input
                                                        onChange={(e) => {
                                                            CanvasData.activeObject.endTime = e.target.value
                                                            context.refreshTheScreen()
                                                        }}
                                                        type="number"
                                                        value={CanvasData.activeObject.endTime === null ? "" : CanvasData.activeObject.endTime} />
                                                </div>
                                                <div>
                                                </div>
                                            </div>
                                        </div>

                                        {
                                            CanvasData.activeObject.type === "text" ?
                                                <React.Fragment>
                                                    <div className="text-edit-container">
                                                        <div className="header" onClick={() => settext(!text)}>
                                                            <span>Text</span>
                                                            <img src={text ? UpArrow : DownArrow} alt="" />
                                                        </div>

                                                        <div style={text ? null : { display: 'none' }}>
                                                            <textarea
                                                                value={CanvasData.activeObject.content}
                                                                onChange={editTheText}
                                                                placeholder="edit text here" />

                                                            <div className="text-container-holder">
                                                                <div className="text-container">
                                                                    <span>
                                                                        <div>English</div>
                                                                        <div className="fontSize">{CanvasData.activeObject.bold}</div>
                                                                        <div className="fontHoverList">
                                                                            {
                                                                                fontWeight.map(data => {
                                                                                    return <div key={data} onClick={() => changeFontWeight(data)}>{data}</div>
                                                                                })
                                                                            }
                                                                        </div>
                                                                        <div>Line Height</div>
                                                                    </span>
                                                                    <span className="image-container-down">
                                                                        <img src={DownArrow} alt="" /> <br />
                                                                        <img src={DownArrow} alt="" /> <br />
                                                                        <input type="number"
                                                                            onChange={(e) => lineHeight(e.target.value)}
                                                                            value={CanvasData.activeObject.lineHeight} />
                                                                    </span>
                                                                </div>
                                                                <div className="text-container">
                                                                    <span>
                                                                        <div onClick={() => setshowFonts(true)}>{CanvasData.activeObject.fontFamily}</div>
                                                                        <div>Font Size</div>
                                                                        <div>Letter Spacing</div>
                                                                    </span>
                                                                    <span className="image-container-down">
                                                                        <img src={DownArrow} alt="" onClick={() => setshowFonts(true)} /> <br />
                                                                        <input type="number"
                                                                            onChange={(e) => changeFontSize(e.target.value)}
                                                                            value={CanvasData.activeObject.size} /> <br />
                                                                        <input type="number"
                                                                            onChange={(e) => letterSpacing(e.target.value)}
                                                                            value={CanvasData.activeObject.letterSpacing} />
                                                                    </span>
                                                                </div>
                                                            </div>

                                                            <div className="text-edit-functionality">
                                                                <img onClick={textAlignment} src={CanvasData.activeObject.align === "left" ? Alignment : CanvasData.activeObject.align === "center" ? AlignmentCenter : AlignmentRight} alt="" />
                                                                <img onClick={toggleListText} src={List} alt="" />
                                                                <img src={Underline} alt="" />
                                                                <img onClick={italicText} src={Italic} alt="" />
                                                                <img onClick={boldText} src={Bold} alt="" />
                                                                <span onClick={capitalizeText}>aA</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="colors">
                                                        <div className="header" onClick={() => setfill(!fill)}>
                                                            <span>Fill</span>
                                                            <img src={fill ? UpArrow : DownArrow} alt="" />
                                                        </div>
                                                        <div className="color-container" style={fill ? null : { display: 'none' }}>
                                                            <div className="block-container">
                                                                <span className="block" style={{ backgroundColor: CanvasData.activeObject.color }}>
                                                                    <input
                                                                        style={{ opacity: 0 }}
                                                                        value={CanvasData.activeObject.color}
                                                                        type="color"
                                                                        onChange={changeTextColor} />
                                                                </span>
                                                                <span>{CanvasData.activeObject.color}</span>
                                                            </div>
                                                            <div>100%</div>
                                                        </div>
                                                    </div>
                                                </React.Fragment>
                                                : CanvasData.activeObject.type === "group" ?
                                                    null
                                                    : CanvasData.activeObject.type === "chart" ?
                                                        null :
                                                        <React.Fragment>
                                                            <div className="layout">
                                                                <div className="header" onClick={() => setlayout(!layout)}>
                                                                    <span>Layout</span>
                                                                    <img src={layout ? UpArrow : DownArrow} alt="" />
                                                                </div>
                                                                <div style={layout ? null : { display: 'none' }}>
                                                                    <div className="layout-container">
                                                                        <span className="center" onClick={() => flip("horizontal")}>
                                                                            <img src={HorizontalFlip} alt="" />
                                                                            <span className="layout-title">Flip Horizontal</span>
                                                                        </span>
                                                                        <span className="center" onClick={() => flip("vertical")}>
                                                                            <img src={VerticalFlip} alt="" />
                                                                            <span className="layout-title">Flip Vertical</span>
                                                                        </span>
                                                                    </div>
                                                                    <div className="layout-container">
                                                                        <div className="center" onClick={() => setcropImage(true)}>
                                                                            <img src={Crop} alt="" />
                                                                            <span className="layout-title">Crop</span>
                                                                        </div>
                                                                        {
                                                                            CanvasData.activeObject.type === "svg" || CanvasData.activeObject.type === "mask" ?
                                                                                <div className="center" onClick={() => checkSvgOrMaskAndSetImageOrRemove()}>
                                                                                    <div className="check-box">
                                                                                        {
                                                                                            CanvasData.activeObject.mask === true ? "✓" : null
                                                                                        }
                                                                                    </div>
                                                                                    <span className="layout-title">Mask</span>
                                                                                </div> : null
                                                                        }
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            {
                                                                CanvasData.activeObject.type === "svg" ?
                                                                    <div className="colors">
                                                                        <div className="header" onClick={() => setcolor(!color)}>
                                                                            <span>Colors</span>
                                                                            <img src={color ? UpArrow : DownArrow} alt="" />
                                                                        </div>
                                                                        <div style={color ? null : { display: 'none' }}>
                                                                            <div className="svg-color-container">
                                                                                {
                                                                                    CanvasData.activeObject.colors.map((data, index) => {
                                                                                        return <div key={"svgColor" + index} style={{ backgroundColor: data }}
                                                                                            onClick={() => changeSvgColor(data)}
                                                                                            className="colorBlockSvg" />
                                                                                    })
                                                                                }
                                                                                <input type="color" id="svgcolorchanger" onChange={(e) => getTheSvgColorToChange(e)} style={{ position: 'absolute', zIndex: -2 }} />
                                                                            </div>
                                                                        </div>
                                                                    </div> : null
                                                            }

                                                            <div className="filter">
                                                                <div className="header" onClick={() => setfilterContainer(!filterContainer)}>
                                                                    <span>Filter</span>
                                                                    <img src={filterContainer ? UpArrow : DownArrow} alt="" />
                                                                </div>

                                                                <div style={filterContainer ? null : { display: 'none' }}>
                                                                    <div className="filter-container">
                                                                        <span className="filter-container-sub" onClick={() => setfilter(true)}>
                                                                            <img src={Filter} alt="" />
                                                                            <span className="left-margin">{CanvasData.activeObject.filterName}</span>
                                                                            <img className="left-margin" src={DownArrow} alt="" />
                                                                        </span>
                                                                        <span className="filter-container-sub">
                                                                            <span>100%</span>
                                                                            <img className="left-margin" src={Eye} alt="" />
                                                                        </span>
                                                                    </div>

                                                                    <div>
                                                                        <div>
                                                                            <div className="filter-container" onClick={() => setadjustFilter(!adjustFilter)}>
                                                                                <span className="filter-container-sub">
                                                                                    <img src={Adjust} alt="" />
                                                                                    <span className="left-margin">Adjust</span>
                                                                                </span>
                                                                                <span className="filter-container-sub">
                                                                                    <img src={Eye} alt="" />
                                                                                    <img className="left-margin" src={adjustFilter ? UpArrow : DownArrow} alt="" />
                                                                                </span>
                                                                            </div>
                                                                            <div style={adjustFilter ? null : { display: 'none' }}>
                                                                                <div className="filter-adjust">
                                                                                    <label htmlFor="points">Brightness &nbsp;&nbsp;&nbsp;&nbsp;{CanvasData.activeObject.brightness}</label>
                                                                                    <input
                                                                                        onChange={(e) => changeFilterAdjustment('brightness', e.target.value)}
                                                                                        value={parseInt(CanvasData.activeObject.brightness.replace(/[^A-Za-z0-9]/g, ""))}
                                                                                        type="range" id="points" name="points" min="0" max="200" />
                                                                                </div>
                                                                                <div className="filter-adjust">
                                                                                    <label htmlFor="points">Contrast &nbsp;&nbsp;&nbsp;&nbsp;{CanvasData.activeObject.contrast}</label>
                                                                                    <input
                                                                                        onChange={(e) => changeFilterAdjustment('contrast', e.target.value)}
                                                                                        value={parseInt(CanvasData.activeObject.contrast.replace(/[^A-Za-z0-9]/g, ""))}
                                                                                        type="range" id="points" name="points" min="0" max="200" />
                                                                                </div>
                                                                                <div className="filter-adjust">
                                                                                    <label htmlFor="points">Saturate  &nbsp;&nbsp;&nbsp;&nbsp;{CanvasData.activeObject.saturate}</label>
                                                                                    <input
                                                                                        onChange={(e) => changeFilterAdjustment('saturate', e.target.value)}
                                                                                        value={parseInt(CanvasData.activeObject.saturate.replace(/[^A-Za-z0-9]/g, ""))}
                                                                                        type="range" id="points" name="points" min="0" max="200" />
                                                                                </div>
                                                                                <div className="filter-adjust">
                                                                                    <label htmlFor="points">Invert &nbsp;&nbsp;&nbsp;&nbsp;{CanvasData.activeObject.invert}</label>
                                                                                    <input
                                                                                        onChange={(e) => changeFilterAdjustment('invert', e.target.value)}
                                                                                        value={parseInt(CanvasData.activeObject.invert.replace(/[^A-Za-z0-9]/g, ""))}
                                                                                        type="range" id="points" name="points" min="0" max="100" />
                                                                                </div>
                                                                                <div className="filter-adjust">
                                                                                    <label htmlFor="points">Grayscale &nbsp;&nbsp;&nbsp;&nbsp;{CanvasData.activeObject.grayscale}</label>
                                                                                    <input
                                                                                        onChange={(e) => changeFilterAdjustment('grayscale', e.target.value)}
                                                                                        value={parseInt(CanvasData.activeObject.grayscale.replace(/[^A-Za-z0-9]/g, ""))}
                                                                                        type="range" id="points" name="points" min="0" max="100" />
                                                                                </div>
                                                                                <div className="filter-adjust">
                                                                                    <label htmlFor="points">Sepia  &nbsp;&nbsp;&nbsp;&nbsp;{CanvasData.activeObject.sepia}</label>
                                                                                    <input
                                                                                        onChange={(e) => changeFilterAdjustment('sepia', e.target.value)}
                                                                                        value={parseInt(CanvasData.activeObject.sepia.replace(/[^A-Za-z0-9]/g, ""))}
                                                                                        type="range" id="points" name="points" min="0" max="100" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </React.Fragment>
                                        }

                                        <div className="blend">
                                            <div className="header" onClick={() => setblend(!blend)}>
                                                <span>Blend</span>
                                                <img src={blend ? UpArrow : DownArrow} alt="" />
                                            </div>

                                            <div className="transperency-container" style={blend ? null : { display: 'none' }}>
                                                <span>
                                                    <img src={Blend} alt="" />
                                                    <span className="marginLeft">Transparency</span>
                                                    <img className="marginLeft" src={DownArrow} alt="" />
                                                </span>
                                                <span>
                                                    <input type="number"
                                                        onChange={(e) => changeOpacity(e.target.value)}
                                                        value={CanvasData.activeObject.opacity * 100} />
                                                    <img className="marginLeft" src={Eye} alt="" />
                                                </span>
                                            </div>
                                        </div>

                                        <div className="effects">
                                            <div className="header" onClick={() => seteffect(!effect)}>
                                                <span>Effects</span>
                                                <img src={effect ? UpArrow : DownArrow} alt="" />
                                            </div>

                                            <div style={effect ? null : { display: 'none' }}>
                                                <div className="shadow-container" onClick={() => setshadowBox(!shadowBox)}>
                                                    <span>
                                                        <span>Drop shadow</span>
                                                        <img className="marginLeft" src={shadowBox ? UpArrow : DownArrow} alt="" />
                                                    </span>
                                                    <span>
                                                        <img src={Settings} alt="" />
                                                        <img className="marginLeft" src={Eye} alt="" />
                                                        <span className="marginLeft">—</span>
                                                    </span>
                                                </div>
                                                <div style={shadowBox ? null : { display: 'none' }}>
                                                    <div className="shadow-state">
                                                        <div className="state-shadow-container">
                                                            <span className="title2">
                                                                <div>X</div>
                                                                <div>Y</div>
                                                            </span>
                                                            <span>
                                                                <input type="number"
                                                                    onChange={(e) => changeShadow("x", e.target.value)}
                                                                    value={CanvasData.activeObject.shadowOffsetX} /> <br />
                                                                <input type="number"
                                                                    onChange={(e) => changeShadow("y", e.target.value)}
                                                                    value={CanvasData.activeObject.shadowOffsetY} />
                                                            </span>
                                                        </div>
                                                        <div className="state-shadow-container" style={{ marginBottom: 32 }}>
                                                            <span className="title2">
                                                                <div>Blur</div>
                                                            </span>
                                                            <span>
                                                                <input type="number"
                                                                    onChange={(e) => changeShadow("blur", e.target.value)}
                                                                    value={CanvasData.activeObject.shadowBlur} /> <br />
                                                            </span>
                                                        </div>
                                                    </div>

                                                    <div className="color-container">
                                                        <div className="block-container">
                                                            <span className="block" style={{ backgroundColor: CanvasData.activeObject.shadowColor }}>
                                                                <input
                                                                    style={{ opacity: 0 }}
                                                                    value={CanvasData.activeObject.shadowColor}
                                                                    type="color"
                                                                    onChange={changeShadowColor} />
                                                            </span>
                                                            <span>{CanvasData.activeObject.shadowColor}</span>
                                                        </div>
                                                        <div>100%</div>
                                                    </div>
                                                </div>

                                                <div className="shadow-container" style={{ marginTop: 15 }} onClick={() => setblurBox(!blurBox)}>
                                                    <span>
                                                        <span>Layer Blur</span>
                                                        <img className="marginLeft" src={blurBox ? UpArrow : DownArrow} alt="" />
                                                    </span>
                                                    <span>
                                                        <img src={Settings} alt="" />
                                                        <img className="marginLeft" src={Eye} alt="" />
                                                        <span className="marginLeft">—</span>
                                                    </span>
                                                </div>

                                                <div className="blur" style={blurBox ? null : { display: 'none' }}>
                                                    <span>Blur</span>
                                                    <input type="number"
                                                        onChange={(e) => changeShadow("blurElement", e.target.value)}
                                                        value={CanvasData.activeObject.blur} />
                                                </div>
                                            </div>
                                        </div>
                                        {
                                            CanvasData.activeObject.type === "chart" ?
                                                <div className="chart-editor-container">
                                                    {
                                                        CanvasData.activeObject.data.map((data, index) => {
                                                            return <div key={"chart" + index} className="chart-editor">
                                                                <input type="number" value={data.marks} onChange={(e) => changeValueOfbar(index, e)} />
                                                                <span style={{ backgroundColor: data.color }}>
                                                                    <input
                                                                        style={{ opacity: 0, width: '100%', height: '100%' }}
                                                                        value={data.color}
                                                                        type="color"
                                                                        onChange={(e) => changeBarColor(index, e)}
                                                                    />
                                                                </span>
                                                            </div>
                                                        })
                                                    }
                                                </div> : null
                                        }
                                    </>
                            }
                        </div>
                    </React.Fragment>
            }
        </div>
    )
}

export default EditBox

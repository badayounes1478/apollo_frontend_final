import React, { useEffect, useState, useContext } from 'react'
import '../reusablecomponentscss/Element.css'
import AppContext from '../../store/DataProvider'
import Axios from 'axios'
import CanvasData from '../../Canvas'
import { draw } from '../../functions/DrawElementsOnCanvas'
import constant from '../../constant'

const Element = () => {

    const [svg, setsvg] = useState([])
    const [page, setpage] = useState(1)
    const [query, setquery] = useState("")
    const [loading, setloading] = useState(false)
    const context = useContext(AppContext)

    useEffect(() => {
        getTheSvgElements()
    }, [])// eslint-disable-line react-hooks/exhaustive-deps

    const getTheSvgElements = async () => {
        try {
            setloading(true)
            let response = await Axios.get(`${constant.imagesURL}elements?page=${page}`)
            setpage((prev) => prev + 1)
            setsvg([...svg, ...response.data])
            setloading(false)
        } catch (error) {
            setloading(false)
            console.log(error)
        }
    }

    const search = async (e) => {
        try {
            e.preventDefault()
            setloading(true)
            let response = await Axios.get(`${constant.imagesURL}elements/search?query=${query}`)
            setsvg([...response.data, ...svg])
            setloading(false)
        } catch (error) {
            setloading(false)
        }
    }

    const toDataURL = url => fetch(url)
        .then(response => response.blob())
        .then(blob => new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.onloadend = () => resolve(reader.result)
            reader.onerror = reject
            reader.readAsDataURL(blob)
        }))

    const addSvgInCanvas = async (data) => {
        const svg = document.getElementById(data.id)
        let svgElement = {
            id: Date.now(), name: "Svg", elementId: Date.now(), x: 10, y: 10, width: svg.width, height: svg.height, base64: null,
            type: "svg", src: data.svg, rotate: 0, blur: 0, opacity: 1.0, lock: false, horizontalFlip: false, colorObject: [], colorObjectSet: false,
            verticalFlip: false, overlayImage: false, colors: [], flag: 0, display: true, filterName: "None", blobURL: null,
            shadowOffsetX: 0, shadowOffsetY: 0, shadowColor: '#000000', shadowBlur: 0, mask: false, blobCompleted: false,
            brightness: '100%', contrast: '100%', grayscale: '0%', saturate: '100%', sepia: '0%', invert: '0%',
            startTime: null, endTime: null, showElementPreview: true
        }
        CanvasData.arrayOfElements = [...CanvasData.arrayOfElements, svgElement]
        let activeCanvas = CanvasData.multipleCanvas.find(data => data.id === CanvasData.design_id)
        activeCanvas.arrayOfElements = CanvasData.arrayOfElements
        context.refreshTheScreen()
        setTimeout(() => {
            draw()
        }, 300);
        svgElement.base64 = await toDataURL(data.svg)
    }

    return (
        <div className="element-container">
            <div className="left-header-title">Elements</div>
            <form onSubmit={search}>
                <input
                    onChange={(e) => setquery(e.target.value)}
                    className="left-search-box"
                    placeholder="Search" type="text" />
            </form>
            <div className="elements">
                {
                    svg.map((data, index) => {
                        return <img
                            id={data.id}
                            crossOrigin="anonymous"
                            onClick={() => addSvgInCanvas(data)}
                            key={index + "svg"}
                            src={data.svg} alt="" />
                    })
                }
            </div>

            <div className="heatSvgAndImage">
                {
                    svg.map((data, index) => {
                        return <React.Fragment key={index + "svgheat"}>
                            <object
                                style={{ height: 1, width: 1 }}
                                id={data.id + "svg"}
                                data=""
                                type="image/svg+xml"
                                aria-label="mayur"
                                crossOrigin="anonymous"
                            />
                            <img style={{ height: 1, width: 1 }}
                                src="" id={`${data.id}svgsecret`} alt="" />
                        </React.Fragment>
                    })
                }
            </div>
            <div className="loader-container" style={loading ? null : { display: 'none' }}>
                <div className="loader"></div>
            </div>
            <button onClick={getTheSvgElements} className="load-more"
                style={loading ? { display: 'none' } : null}>Load More</button>
        </div>
    )
}

export default Element
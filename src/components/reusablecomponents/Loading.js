import React, { useEffect } from 'react'
import '../reusablecomponentscss/Loading.css'
import Guest from 'cross-domain-storage/guest'
import Logo from '../../assets/Logo.svg'

const Loading = (props) => {

    useEffect(() => {
        const token = Guest('https://www.app.creatosaurus.io');
        token.get('token', function (error, value) {
            if (!error) {
                if (value === null) return window.open('https://www.app.creatosaurus.io/login?app=apollo', "_self")
                localStorage.setItem('token', value)
                props.history.push('/home')
            } else {
                window.open('https://www.app.creatosaurus.io/login?app=apollo', "_self")
            }
        })
    }, [])// eslint-disable-line react-hooks/exhaustive-deps

    return (
        <React.Fragment>
            <nav>
                <div className="row">
                    <img src={Logo} alt="" />
                    <div><span>Creatosaurus</span> | Muse</div>
                </div>
                <div className="circle-container" />
            </nav>

            <div className="loader-container-loading">
                <h1>Loading . . .</h1>
            </div>
        </React.Fragment>
    )
}

export default Loading

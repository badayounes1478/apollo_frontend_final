import React, { useContext } from 'react'
import '../reusablecomponentscss/LeftSideBarContainer.css'
import EditBox from './EditBox'
import Layer from './Layer'
import Media from './Media'
import Template from './Template'
import Element from './Element'
import AppContext from '../../store/DataProvider'
import Components from './Components'
import Apps from './Apps'
import Uploads from './Uploads'

const LeftSideBarContainer = () => {

    const context = useContext(AppContext)

    return (
        <div className="left-side-bar-container">
            <div className="button-container">
                <button onClick={() => context.changeActiveButtonLeft(1)} className={context.activeLeftButton === 1 ? "active" : null}>
                    <div>🖼</div>
                    <div className="button-title">Templates</div>
                </button>
                <button onClick={() => context.changeActiveButtonLeft(2)} className={context.activeLeftButton === 2 ? "active" : null}>
                    <div>🖌</div>
                    <div className="button-title">Design</div>
                </button>
                <button onClick={() => context.changeActiveButtonLeft(3)} className={context.activeLeftButton === 3 ? "active" : null}>
                    <div>📚</div>
                    <div className="button-title">Layers</div>
                </button>
                <button onClick={() => context.changeActiveButtonLeft(4)} className={context.activeLeftButton === 4 ? "active" : null}>
                    <div>🎥</div>
                    <div className="button-title">Media</div>
                </button>
                <button onClick={() => context.changeActiveButtonLeft(5)} className={context.activeLeftButton === 5 ? "active" : null}>
                    <div>🏗</div>
                    <div className="button-title">Components</div>
                </button>
                <button onClick={() => context.changeActiveButtonLeft(6)} className={context.activeLeftButton === 6 ? "active" : null}>
                    <div>🎯</div>
                    <div className="button-title">Elements</div>
                </button>
                <button onClick={() => context.changeActiveButtonLeft(7)} className={context.activeLeftButton === 7 ? "active" : null}>
                    <div>🗂</div>
                    <div className="button-title">Uploads</div>
                </button>
                <button onClick={() => context.changeActiveButtonLeft(8)} className={context.activeLeftButton === 8 ? "active" : null}>
                    <div>💻</div>
                    <div className="button-title">Apps</div>
                </button>
            </div>
            <div className="preview-container">
                <div className={context.activeLeftButton === 1 ? null : "hide-left-bar"}>
                    <Template />
                </div>
                <div className={context.activeLeftButton === 2 ? null : "hide-left-bar"}>
                    <EditBox />
                </div>
                <div className={context.activeLeftButton === 3 ? null : "hide-left-bar"}>
                    <Layer />
                </div>
                <div className={context.activeLeftButton === 4 ? null : "hide-left-bar"}>
                    <Media />
                </div>
                <div className={context.activeLeftButton === 5 ? null : "hide-left-bar"}>
                    <Components />
                </div>
                <div className={context.activeLeftButton === 6 ? null : "hide-left-bar"}>
                    <Element />
                </div>
                <div className={context.activeLeftButton === 7 ? null : "hide-left-bar"}>
                   <Uploads />
                </div>
                <div className={context.activeLeftButton === 8 ? null : "hide-left-bar"}>
                    <Apps />
                </div>
            </div>
        </div>
    )
}

export default LeftSideBarContainer

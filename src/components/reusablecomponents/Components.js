import React from 'react'
import UpArrow from '../../assets/UpArrow.svg'
import { addTextInCanvas, addChartInCanvas } from '../../functions/AddElementToCanvas'
import '../reusablecomponentscss/Components.css'

const Components = () => {
    return (
        <div className="component-container">
            <div className="left-header-title">Components</div>
            <input className="left-search-box" placeholder="Search" type="text" />
            <div className="input-container">
                <span>Input</span>
                <img src={UpArrow} alt="" />
            </div>
            <div className="text-title-row">
                <span className="heading" onClick={()=> addTextInCanvas(24, 'bold')}>Heading</span>
                <span className="subheading" onClick={()=> addTextInCanvas(18, '600')}>Subheading</span>
                <span className="body" onClick={()=> addTextInCanvas(14, '400')}>Body</span>
            </div>

            <div className="input-container">
                <span>Text</span>
                <span className="seeall">See all</span>
            </div>
            <div className="cards" onClick={addChartInCanvas}>
                <img src="https://cdn.pixabay.com/photo/2021/05/03/13/38/fairy-flower-6226285__340.jpg" alt="" />
                <img src="https://cdn.pixabay.com/photo/2021/05/03/13/38/fairy-flower-6226285__340.jpg" alt="" />
            </div>

            <div className="input-container">
                <span>Button</span>
                <span className="seeall">See all</span>
            </div>
            <div className="cards">
                <img src="https://cdn.pixabay.com/photo/2021/05/03/13/38/fairy-flower-6226285__340.jpg" alt="" />
                <img src="https://cdn.pixabay.com/photo/2021/05/03/13/38/fairy-flower-6226285__340.jpg" alt="" />
            </div>
        </div>
    )
}

export default Components

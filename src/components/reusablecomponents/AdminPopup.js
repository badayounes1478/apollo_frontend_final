import React, { useState } from 'react'
import '../reusablecomponentscss/AdminPopup.css'

const AdminPopup = (props) => {

    const [description, setdescription] = useState("")
    const [colors, setcolors] = useState("")
    const [category, setcategory] = useState("")
    const [tags, settags] = useState("")
    const [region, setregion] = useState("")

    const submit = () => {
        let colorsArray = colors.split(" ");
        let categoryArray = category.split(" ");
        let tagsArray = tags.split(" ");
        let regiosArray = region.split(" ");
        props.submit(description, colorsArray, categoryArray, tagsArray, regiosArray)
    }

    return (
        <div className="admin-popup">
            <div className="fields">
                <label>Description</label>
                <textarea
                    value={description}
                    onChange={(e) => setdescription(e.target.value)}
                    placeholder="add description" />

                <label>Colors</label>
                <textarea
                    value={colors}
                    onChange={(e) => setcolors(e.target.value)}
                    placeholder="add colors" />

                <label>Category</label>
                <textarea
                    value={category}
                    onChange={(e) => setcategory(e.target.value)}
                    placeholder="add category" />

                <label>Tags</label>
                <textarea 
                value={tags}
                onChange={(e)=> settags(e.target.value)}
                 placeholder="add tags" />

                <label>Region</label>
                <textarea 
                onChange={(e)=> setregion(e.target.value)}
                value={region} 
                placeholder="add regions" />

                <div>
                    <button 
                    onClick={submit}>Submit</button>

                    <button
                        onClick={() => props.close()}
                        style={{ backgroundColor: 'red' }}>Close</button>
                </div>
            </div>
        </div>
    )
}

export default AdminPopup
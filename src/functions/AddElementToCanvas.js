import CanvasData from '../Canvas'
import WebFont from 'webfontloader'
import { draw } from '../functions/DrawElementsOnCanvas'

const changeTextFont = (value) => {
    WebFont.load({
        google: {
            families: [value]
        },
        timeout: 1000
    });
    if (CanvasData.activeObject === null) return
    if (CanvasData.activeObject.type !== "text") return
    CanvasData.activeObject.fontFamily = value
    setTimeout(() => {
        draw()
    }, 1000);
}

const addTextInCanvas = (size, bold) => {
    let textElement = {
        id: Date.now(), x: 10, y: 10, type: 'text', color: "#000000", size: size, lock: false, opacity: 1.0, name: "Text",
        fontFamily: 'Arial', content: "Select template", horizontalFlip: false, verticalFlip: false,
        stroke: false, list: false, italic: "", bold: bold, align: 'left', baseline: 'top', letterSpacing: 0,
        shadowOffsetX: 0, shadowOffsetY: 0, shadowColor: '#000000', shadowBlur: 0, display: true,
        doubleCliked: false, foucesIndex: 0, rotate: 0, blur: 0, textStyle: 'user', lineHeight: 0,
        startTime: null, endTime: null, showElementPreview: true
    }
    CanvasData.arrayOfElements = [...CanvasData.arrayOfElements, textElement]
    changeTextFont(textElement.fontFamily)
    draw()
}

const toDataURL = url => fetch(url)
    .then(response => response.blob())
    .then(blob => new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.onloadend = () => resolve(reader.result)
        reader.onerror = reject
        reader.readAsDataURL(blob)
    }))

const addImageInCanvas = async (id, src) => {
    const data = document.getElementById(id)
    let imageElement = {
        id: Date.now(), name: "Image", elementId: id, x: 10, y: 10, width: data.width, base64: null,
        height: data.height, type: "image", src: src, rotate: 0, blur: 0, opacity: 1.0, display: true,
        lock: false, horizontalFlip: false, verticalFlip: false, overlayImage: false, radius: 0,
        shadowOffsetX: 0, shadowOffsetY: 0, shadowColor: '#000000', shadowBlur: 0, filterName: "None",
        brightness: '100%', contrast: '100%', grayscale: '0%', saturate: '100%', sepia: '0%', invert: '0%',
        startTime: null, endTime: null, showElementPreview: true
    }
    CanvasData.arrayOfElements = [...CanvasData.arrayOfElements, imageElement]
    draw()
    imageElement.base64 = await toDataURL(src)
}

const addChartInCanvas = () => {
    let chartElement = {
        id: new Date().getTime(), x: 200, y: 100, type: 'chart', name: "Chart", lock: false,
        shadowOffsetX: 0, shadowOffsetY: 0, shadowColor: '#000000', shadowBlur: 0, blur: 0, opacity: 1.0,
        display: true, chartType: "bar", height: 200, width: 300, data: [
            { name: "mayur", marks: 1000, color: 'orange' },
            { name: "malav", marks: 120, color: 'red' },
            { name: "saurabh", marks: 50, color: 'black' },
            { name: "vishal", marks: 130, color: 'yellow' },
            { name: "asfa", marks: 1110, color: 'orange' },
            { name: "yada", marks: 1230, color: 'green' },
        ], radius: 0, rotate: 0,
        startTime: null, endTime: null, showElementPreview: true
    }
    CanvasData.arrayOfElements = [...CanvasData.arrayOfElements, chartElement]
    draw()
}

const addVideoInCanvas = async (id, src) => {
    const data = document.getElementById(id)
    /*let video = {
        id: new Date().getTime(), elementId: id, x: 10, y: 10, width: data.videoWidth, height: data.videoHeight, type: "video",
        src: src, blob: null,name:"video",
        opacity: 1.0, lock: false, horizontalFlip: false, verticalFlip: false, overlayImage: true, color: 'red',
        startVideo: null, endVideo: null, showVideo: true, show: null, start: null, end: null
    }*/

    let videoElement = {
        id: Date.now(), name: "Video", elementId: id, x: 10, y: 10, width: data.videoWidth / 3, base64: null,
        height: data.videoHeight / 3, type: "video", src: src, rotate: 0, blur: 0, opacity: 1.0, display: true,
        lock: false, horizontalFlip: false, verticalFlip: false, overlayImage: false, radius: 0,
        shadowOffsetX: 0, shadowOffsetY: 0, shadowColor: '#000000', shadowBlur: 0, filterName: "None",
        brightness: '100%', contrast: '100%', grayscale: '0%', saturate: '100%', sepia: '0%', invert: '0%',
        startTime: null, endTime: null, showElementPreview: true
    }
    CanvasData.arrayOfElements = [...CanvasData.arrayOfElements, videoElement]
    draw()
}

export {
    addTextInCanvas,
    addImageInCanvas,
    addChartInCanvas,
    addVideoInCanvas
}
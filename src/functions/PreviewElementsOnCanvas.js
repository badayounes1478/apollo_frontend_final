import CanvasData from '../Canvas'
import { fadeInAnimationImage, fadeOutAnimationImage, fromBottomAnimationImage, fromLeftAnimationImage, fromRightAnimationImage, fromTopAnimationImage, rotateAnimationImage } from './ImageAnimations';
import { fadeInAnimationSvg, fadeOutAnimationSvg, fromBottomAnimationSvg, fromLeftAnimationSvg, fromRightAnimationSvg, fromTopAnimationSvg, rotateAnimationSvg } from './SvgAnimation';
import { fadeInAnimationText, fadeOutAnimationText, fromBottomAnimationText, fromLeftAnimationText, fromRightAnimationText, fromTopAnimationText, rotateAnimationText } from './TextAnimation';

const clearCanvas = () => {
    CanvasData.ctx4.clearRect(0, 0, CanvasData.canvas4.width, CanvasData.canvas4.height);
}

const setWidthAndHeightToCanvas = () => {
    CanvasData.canvas4.width = CanvasData.canvasWidth * CanvasData.multiplier
    CanvasData.canvas4.height = CanvasData.canvasHeight * CanvasData.multiplier
}

const setColorToCanvas = (backgroundColor) => {
    CanvasData.ctx4.fillStyle = backgroundColor
    CanvasData.ctx4.fillRect(0, 0, CanvasData.canvas4.width, CanvasData.canvas4.height);
}

const rotateElement = (data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier

    CanvasData.ctx4.translate(x + (width / 2), y + (height / 2))
    CanvasData.ctx4.rotate((Math.PI / 180) * data.rotate);
    CanvasData.ctx4.translate(-x - (width / 2), -y - (height / 2));
}

const blurElement = (data) => {
    CanvasData.ctx4.filter = `blur(${data.blur}px)`
}

const verticalFlipElement = (img, data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier

    CanvasData.ctx4.translate(x + width / 2, y + height / 2);
    CanvasData.ctx4.scale(1, -1);

    // Move registration point back to the top left corner of canvas
    CanvasData.ctx4.translate((-width) / 2, (-height) / 2);
    CanvasData.ctx4.globalAlpha = data.opacity
    CanvasData.ctx4.drawImage(img, 0, 0, width, height);
    CanvasData.ctx4.globalAlpha = 1.0;
}

const horizontalFlipElement = (img, data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier

    CanvasData.ctx4.translate(x + width / 2, y + height / 2);
    CanvasData.ctx4.scale(-1, 1);

    // Move registration point back to the top left corner of canvas
    CanvasData.ctx4.translate((-width) / 2, (-height) / 2);
    CanvasData.ctx4.globalAlpha = data.opacity
    CanvasData.ctx4.drawImage(img, 0, 0, width, height);
    CanvasData.ctx4.globalAlpha = 1.0;
}


const shadowToElement = (data) => {
    CanvasData.ctx4.shadowColor = data.shadowColor;
    CanvasData.ctx4.shadowBlur = data.shadowBlur;
    CanvasData.ctx4.shadowOffsetX = data.shadowOffsetX * CanvasData.multiplier;
    CanvasData.ctx4.shadowOffsetY = data.shadowOffsetY * CanvasData.multiplier;
}


const checkTheElementHaveToDrawOnCanvasOrNot = (data) => {
    if (data.endTime !== null) {
        if (data.endTime / 30 < CanvasData.frame) {
            data.showElementPreview = false
            return false
        }
    }

    if (data.showElementPreview === false) return false

    if (data.startTime === null) {
        return true
    } else {
        if (data.startTime / 30 < CanvasData.frame) {
            data.showElementPreview = true
            return true
        }
    }
}


const checkTheAnimationTypeOfImage = (data) => {
    if (data.animation.type === "fromLeft") {
        fromLeftAnimationImage(data)
    } else if (data.animation.type === "fromRight") {
        fromRightAnimationImage(data)
    } else if (data.animation.type === "fromTop") {
        fromTopAnimationImage(data)
    } else if (data.animation.type === "fromBottom") {
        fromBottomAnimationImage(data)
    } else if (data.animation.type === "fadeIn") {
        fadeInAnimationImage(data)
    } else if (data.animation.type === "fadeOut") {
        fadeOutAnimationImage(data)
    } else if (data.animation.type === "rotate") {
        rotateAnimationImage(data)
    }
}


const drawImageOnCanvas = (data) => {
    //check the element is capable of displaying on canvas or not as per time
    if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return

    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier
    let img = document.getElementById(data.elementId);

    rotateElement(data)
    shadowToElement(data)
    CanvasData.ctx4.filter = `brightness(${data.brightness}) contrast(${data.contrast}) grayscale(${data.grayscale}) saturate(${data.saturate}) sepia(${data.sepia}) invert(${data.invert}) blur(${data.blur}px)`
    if (data.verticalFlip === true) {
        verticalFlipElement(img, data)
    } else if (data.horizontalFlip === true) {
        horizontalFlipElement(img, data)
    } else {
        if (data.animation !== undefined) {
            checkTheAnimationTypeOfImage(data)
        } else {
            CanvasData.ctx4.globalAlpha = data.opacity
            CanvasData.ctx4.drawImage(img, x, y, width, height);
            CanvasData.ctx4.globalAlpha = 1.0;
        }
    }
}

const checkTheAnimationTypeOfSvg = (data, img) => {
    if (data.animation.type === "fromLeft") {
        fromLeftAnimationSvg(data, img)
    } else if (data.animation.type === "fromRight") {
        fromRightAnimationSvg(data, img)
    } else if (data.animation.type === "fromTop") {
        fromTopAnimationSvg(data, img)
    } else if (data.animation.type === "fromBottom") {
        fromBottomAnimationSvg(data, img)
    } else if (data.animation.type === "fadeIn") {
        fadeInAnimationSvg(data, img)
    } else if (data.animation.type === "fadeOut") {
        fadeOutAnimationSvg(data, img)
    } else if (data.animation.type === "rotate") {
        rotateAnimationSvg(data, img)
    }
}

const drawSvgOnCanvas = (data) => {
    //check the element is capable of displaying on canvas or not as per time
    if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return

    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier

    let img = document.getElementById(`${data.elementId}svgsecret`)

    CanvasData.ctx4.save()
    rotateElement(data)
    blurElement(data)
    shadowToElement(data)
    CanvasData.ctx4.filter = `brightness(${data.brightness}) contrast(${data.contrast}) grayscale(${data.grayscale}) saturate(${data.saturate}) sepia(${data.sepia}) invert(${data.invert}) blur(${data.blur}px)`

    if (data.verticalFlip === true) {
        verticalFlipElement(img, data)
    } else if (data.horizontalFlip === true) {
        horizontalFlipElement(img, data)
    } else {
        if (data.animation !== undefined) {
            checkTheAnimationTypeOfSvg(data, img)
        } else {
            CanvasData.ctx4.globalAlpha = data.opacity
            CanvasData.ctx4.drawImage(img, x, y, width, height);
            CanvasData.ctx4.globalAlpha = 1.0;
        }
    }
    CanvasData.ctx4.restore()
}

const checkTheAnimationTypeOfText = (txt, x, y, data) => {
    if (data.animation.type === "fromLeft") {
        fromLeftAnimationText(txt, x, y, data)
    } else if (data.animation.type === "fromRight") {
        fromRightAnimationText(txt, x, y, data)
    } else if (data.animation.type === "fromTop") {
        fromTopAnimationText(txt, x, y, data)
    } else if (data.animation.type === "fromBottom") {
        fromBottomAnimationText(txt, x, y, data)
    } else if (data.animation.type === "fadeIn") {
        fadeInAnimationText(txt, x, y, data)
    } else if (data.animation.type === "fadeOut") {
        fadeOutAnimationText(txt, x, y, data)
    } else if (data.animation.type === "rotate") {
        rotateAnimationText(txt, x, y, data)
    }
}

const checkTheAnimationTypeOfTextAndIncreaseData = (data) => {
    if (data.animation.type === "fromLeft") {
        data.animation.x = data.animation.x + data.animation.speed
    } else if (data.animation.type === "fromRight") {
        data.animation.x = data.animation.x - data.animation.speed
    } else if (data.animation.type === "fromTop") {
        data.animation.y = data.animation.y + data.animation.speed
    } else if (data.animation.type === "fromBottom") {
        data.animation.y = data.animation.y - data.animation.speed
    } else if (data.animation.type === "fadeIn") {
        data.animation.opacity = data.animation.speed + data.animation.opacity
    } else if (data.animation.type === "fadeOut") {
        data.animation.opacity = data.animation.opacity - data.animation.speed
    } else if (data.animation.type === "rotate") {
        data.animation.rotate = data.animation.rotate + data.animation.speed
    }
}

const drawTextOnCanvas = (data) => {
    //check the element is capable of displaying on canvas or not as per time
    if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return

    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let size = data.size * CanvasData.multiplier
    let width = 0

    let txt = data.content
    CanvasData.ctx4.save()
    CanvasData.canvas4.style.letterSpacing = (data.letterSpacing * CanvasData.multiplier) + 'px';
    CanvasData.ctx4.globalAlpha = data.opacity
    CanvasData.ctx4.textBaseline = data.baseline;
    CanvasData.ctx4.font = data.italic + " " + data.bold + " " + size + "px " + data.fontFamily
    CanvasData.ctx4.fillStyle = data.color;
    CanvasData.ctx4.textAlign = data.align

    blurElement(data)
    shadowToElement(data)

    //convert the text in array if multiline is there
    let data1 = txt.split("\n")
    let len = data1.length * size
    data.height = len

    if (data1.length <= 1) {
        CanvasData.ctx4.textAlign = "left"
        CanvasData.ctx4.textBaseline = "top";
        width = CanvasData.ctx4.measureText(txt).width
        data.width = Math.ceil(width)

        /*check the animation type and increase the axis
        because in array iterration makes the increase multiple times*/
        if (data.animation !== undefined) {
            checkTheAnimationTypeOfTextAndIncreaseData(data)
        }

        if (data.animation !== undefined) {
            checkTheAnimationTypeOfText(txt, x, y, data)
        } else {
            //to rotate the text
            rotateElement(data)
            CanvasData.ctx4.fillText(txt, x, y);
        }
    } else {
        //get the largest width from and array
        if (data.list) {
            for (let i = 0; i < data1.length; i++) {
                if (width < CanvasData.ctx4.measureText(i + 1 + ". " + data1[i]).width) {
                    width = CanvasData.ctx4.measureText(i + 1 + ". " + data1[i]).width
                    data.width = Math.ceil(width)
                }
            }
        } else {
            for (let i = 0; i < data1.length; i++) {
                if (width < CanvasData.ctx4.measureText(data1[i]).width) {
                    width = CanvasData.ctx4.measureText(data1[i]).width
                    data.width = Math.ceil(width)
                }
            }
        }

        rotateElement(data)
        let lineHeight = size + data.lineHeight

        /*check the animation type and increase the axis
        because in array iterration makes the increase multiple times*/
        if (data.animation !== undefined) {
            checkTheAnimationTypeOfTextAndIncreaseData(data)
        }

        for (let i = 0; i < data1.length; i++) {
            if (data.list) {
                if (data.align === "center") {
                    CanvasData.ctx4.fillText(i + 1 + ". " + data1[i], x + (width / 2), y + (len / 2) + i * lineHeight);
                } else if (data.align === "right") {
                    CanvasData.ctx4.fillText(i + 1 + ". " + data1[i], x + width, y + i * lineHeight);
                } else if (data.align === "left") {
                    CanvasData.ctx4.fillText(i + 1 + ". " + data1[i], x, y + i * lineHeight);
                }
            } else {
                if (data.align === "center") {
                    if (data.animation !== undefined) {
                        checkTheAnimationTypeOfText(data1[i], x + (width / 2), y + (len / 2) + i * lineHeight, data)
                    } else {
                        CanvasData.ctx4.fillText(data1[i], x + (width / 2), y + (len / 2) + i * lineHeight);
                    }
                } else if (data.align === "right") {
                    if (data.animation !== undefined) {
                        checkTheAnimationTypeOfText(data1[i], x + width, y + i * lineHeight, data)
                    } else {
                        CanvasData.ctx4.fillText(data1[i], x + width, y + i * lineHeight);
                    }
                } else if (data.align === "left") {
                    if (data.animation !== undefined) {
                        checkTheAnimationTypeOfText(data1[i], x, y + i * lineHeight, data)
                    } else {
                        CanvasData.ctx4.fillText(data1[i], x, y + i * lineHeight);
                    }
                }
            }
        }
    }
    CanvasData.canvas4.style.letterSpacing = 0 + 'px';
    CanvasData.ctx4.restore()
}

const drawMaskOnCanvas = (data) => {
    //check the element is capable of displaying on canvas or not as per time
    if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return

    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier
    let img = document.getElementById(data.elementId)
    let img1 = document.getElementById(data.elementId2)


    let canvasMask = document.getElementById('canvas3')
    let ctxMask = canvasMask.getContext("2d");

    canvasMask.height = height
    canvasMask.width = width
    ctxMask.clearRect(0, 0, canvasMask.width, canvasMask.height);
    ctxMask.fillRect(0, 0, canvasMask.width, canvasMask.height);
    ctxMask.globalCompositeOperation = 'source-in';

    ctxMask.drawImage(img, 0, 0, width, height);
    ctxMask.drawImage(img1, 0, 0, width, height);
    ctxMask.globalCompositeOperation = 'source-over';
    ctxMask.restore()

    CanvasData.ctx4.save()
    rotateElement(data)
    shadowToElement(data)
    if (data.animation !== undefined) {
        checkTheAnimationTypeOfSvg(data, canvasMask)
    } else {
        CanvasData.ctx4.drawImage(canvasMask, x, y, width, height);
    }
    CanvasData.ctx4.restore()
}

const drawGroupOnCanvas = (data) => {
    //check the element is capable of displaying on canvas or not as per time
    if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return

    data.data.forEach(element => {
        if (element.type === "image") {
            drawImageOnCanvas(element)
        } else if (element.type === "svg") {
            drawSvgOnCanvas(element)
        } else if (element.type === "text") {
            drawTextOnCanvas(element)
        } else if (element.type === "mask") {
            drawMaskOnCanvas(element)
        } else if (element.type === "chart") {
            drawChartOnCanvas(element)
        } else if (element.type === "video") {
            drawVideoOnCanvas(element)
        }
    })
}

const drawChartOnCanvas = (data) => {
    //check the element is capable of displaying on canvas or not as per time
    if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return

    if (data.chartType === "bar") {
        blurElement(data)
        shadowToElement(data)
        barGraph(data)
    }
}


let barGraph = (data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier

    CanvasData.ctx4.globalAlpha = data.opacity
    let content = data.data
    let numberOfbars = content.length
    let widthOfGraph = width
    let heightOfGraph = height
    let marginBetweenbars = 4
    let barWidth = widthOfGraph / numberOfbars - marginBetweenbars * 2

    //get the largest value from graph so we can plot as per ratio
    let largestValueInGraph = 0
    content.forEach(data => {
        if (data.marks > largestValueInGraph) {
            largestValueInGraph = data.marks
        }
    })

    let lineGap = height / numberOfbars
    let gabBetweenNumbers = largestValueInGraph / numberOfbars

    content.forEach((data1, index) => {
        CanvasData.ctx4.save()
        CanvasData.ctx4.beginPath();
        let y2 = Math.trunc(y + index * lineGap)
        CanvasData.ctx4.rect(x, y2, width, 1);
        CanvasData.ctx4.fillStyle = "rgb(0, 0, 0, 0.03)";
        CanvasData.ctx4.fill();
        CanvasData.ctx4.restore()

        CanvasData.ctx4.save()
        CanvasData.ctx4.fillStyle = "#000000"
        CanvasData.ctx4.font = `${12 * CanvasData.multiplier}px Arial`;
        let x1 = x - barWidth
        let y1 = Math.trunc(y + index * lineGap) + 3
        let plotNumber = Math.trunc(gabBetweenNumbers * (numberOfbars - index))
        CanvasData.ctx4.fillText(plotNumber, x1, y1, data.width)
        CanvasData.ctx4.restore()
    })

    content.forEach((data1, index) => {
        let ratio = data1.marks / largestValueInGraph
        let barHeight = ratio * heightOfGraph

        CanvasData.ctx4.save()
        CanvasData.ctx4.beginPath();
        CanvasData.ctx4.translate(x, y);
        CanvasData.ctx4.rotate((Math.PI / 180) * 180);
        CanvasData.ctx4.translate(-x - width, -y - height);
        let x1 = x + index * (barWidth + 10)
        let y1 = y
        CanvasData.ctx4.rect(x1, y1, barWidth, barHeight);
        CanvasData.ctx4.fillStyle = data1.color
        CanvasData.ctx4.fill();
        CanvasData.ctx4.restore()
    })
    CanvasData.ctx4.globalAlpha = 1.0;
}


const drawVideoOnCanvas = (data) => {
    //check the element is capable of displaying on canvas or not as per time
    if (!checkTheElementHaveToDrawOnCanvasOrNot(data)) return

    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier
    let video = document.getElementById(data.elementId);

    CanvasData.ctx4.save()
    rotateElement(data)
    shadowToElement(data)
    CanvasData.ctx4.filter = `brightness(${data.brightness}) contrast(${data.contrast}) grayscale(${data.grayscale}) saturate(${data.saturate}) sepia(${data.sepia}) invert(${data.invert}) blur(${data.blur}px)`
    if (data.verticalFlip === true) {
        verticalFlipElement(video, data)
    } else if (data.horizontalFlip === true) {
        horizontalFlipElement(video, data)
    } else {
        if (data.animation !== undefined) {
            checkTheAnimationTypeOfImage(data)
        } else {
            CanvasData.ctx4.globalAlpha = data.opacity
            CanvasData.ctx4.drawImage(video, x, y, width, height);
            CanvasData.ctx4.globalAlpha = 1.0;
        }
    }
    CanvasData.ctx4.restore()
    video.play()
}

let interval = 1000 / 30;
let then = performance.now();
const drawAnimation = (arrayOfElements, activeCanvasColor) => {
    let now = new Date().getTime();
    let delta = now - then;
    if (CanvasData.playing === false) return
    if (delta > interval) {
        then = now - (delta % interval);

        CanvasData.frame = CanvasData.frame + 1  // increase the frame of the seen
        CanvasData.allFrame = CanvasData.allFrame + 1 // increase the frame of all seens

        //canvasFunctions
        clearCanvas()
        setWidthAndHeightToCanvas()
        setColorToCanvas(activeCanvasColor)

        //start drawing on the canvas
        arrayOfElements.forEach(data => {
            if (data.display === false) return
            if (data.type === "image") {
                drawImageOnCanvas(data)
            } else if (data.type === "text") {
                drawTextOnCanvas(data)
            } else if (data.type === "svg") {
                drawSvgOnCanvas(data)
            } else if (data.type === "mask") {
                drawMaskOnCanvas(data)
            } else if (data.type === "group") {
                drawGroupOnCanvas(data)
            } if (data.type === "chart") {
                drawChartOnCanvas(data)
            } if (data.type === "video") {
                drawVideoOnCanvas(data)
            } if (CanvasData.audio !== null) {
                let audio = document.getElementById(CanvasData.audio.elementId)
                audio.play()
            }
        })
    }
}


export {
    drawAnimation
}
import CanvasData from '../Canvas'

const fromLeftAnimationSvg = (image, img) => {
    image.animation.x = image.animation.x + image.animation.speed
    if (image.x > image.animation.x) {
        CanvasData.ctx4.drawImage(img, image.animation.x, image.y, image.width, image.height);
    } else {
        CanvasData.ctx4.drawImage(img, image.x, image.y, image.width, image.height);
    }
}

const fromRightAnimationSvg = (image, img) => {
    image.animation.x = image.animation.x - image.animation.speed
    if (image.x < image.animation.x) {
        CanvasData.ctx4.drawImage(img, image.animation.x, image.y, image.width, image.height);
    } else {
        CanvasData.ctx4.drawImage(img, image.x, image.y, image.width, image.height);
    }
}

const fromTopAnimationSvg = (image, img) => {
    image.animation.y = image.animation.y + image.animation.speed
    if (image.y > image.animation.y) {
        CanvasData.ctx4.drawImage(img, image.x, image.animation.y, image.width, image.height);
    } else {
        CanvasData.ctx4.drawImage(img, image.x, image.y, image.width, image.height);
    }
}

const fromBottomAnimationSvg = (image, img) => {
    image.animation.y = image.animation.y - image.animation.speed
    if (image.y < image.animation.y) {
        CanvasData.ctx4.drawImage(img, image.x, image.animation.y, image.width, image.height);
    } else {
        CanvasData.ctx4.drawImage(img, image.x, image.y, image.width, image.height);
    }
}

const fadeInAnimationSvg = (data, img) => {
    if (data.animation.opacity < 1) {
        CanvasData.ctx4.globalAlpha = data.animation.opacity
        data.animation.opacity = data.animation.speed + data.animation.opacity
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
        CanvasData.ctx4.globalAlpha = 1
    } else {
        CanvasData.ctx4.globalAlpha = 1
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
    }
}

const fadeOutAnimationSvg = (data, img) => {
    if (data.animation.opacity > 0) {
        CanvasData.ctx4.globalAlpha = data.animation.opacity
        data.animation.opacity = data.animation.opacity - data.animation.speed
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
        CanvasData.ctx4.globalAlpha = 1
    } else {
        CanvasData.ctx4.globalAlpha = 0
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
        CanvasData.ctx4.globalAlpha = 1
    }
}

const rotateAnimationSvg = (data, img) => {
    data.animation.rotate = data.animation.rotate + data.animation.speed
    if (data.animation.rotate < 360) {
        CanvasData.ctx4.translate(data.x + (data.width / 2), data.y + (data.height / 2))
        CanvasData.ctx4.rotate((Math.PI / 180) * data.animation.rotate);
        CanvasData.ctx4.translate(-data.x - (data.width / 2), -data.y - (data.height / 2));
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
    } else {
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
    }
}


export {
    fromLeftAnimationSvg,
    fromRightAnimationSvg,
    fromTopAnimationSvg,
    fromBottomAnimationSvg,
    fadeInAnimationSvg,
    fadeOutAnimationSvg,
    rotateAnimationSvg
}
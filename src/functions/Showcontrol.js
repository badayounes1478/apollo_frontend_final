const drawBorder = (selectedShape, ctx, ctx2) => {
    // Blue Stroke 

    //rotate the lines as the element rotates
    ctx.save()
    ctx2.save()

    ctx.translate(selectedShape.x + (selectedShape.width / 2), selectedShape.y + (selectedShape.height / 2))
    ctx.rotate((Math.PI / 180) * selectedShape.rotate);
    ctx.translate(-selectedShape.x - (selectedShape.width / 2), -selectedShape.y - (selectedShape.height / 2));


    ctx.beginPath();
    ctx.rect(selectedShape.x, selectedShape.y, selectedShape.width, selectedShape.height);
    ctx.strokeStyle = "#FF4359";
    ctx.stroke();

    ctx2.beginPath();
    ctx2.rect(selectedShape.x, selectedShape.y, selectedShape.width, selectedShape.height);
    ctx2.strokeStyle = "#FF4359";
    ctx2.stroke();

    // Top left Cornor
    if (selectedShape.type !== "group") {
        ctx.beginPath();
        ctx.arc(selectedShape.x, selectedShape.y, 6, 0, 2 * Math.PI);
        ctx.fillStyle = "#FF4359";
        ctx.fill();
    }

    ctx2.beginPath();
    ctx2.arc(selectedShape.x, selectedShape.y, 6, 0, 2 * Math.PI);
    ctx2.fillStyle = "rgb(255,255,255)";
    ctx2.fill();

    //Top Center Cornor
    ctx.beginPath();
    ctx.arc(selectedShape.x + selectedShape.width / 2, selectedShape.y, 6, 0, 2 * Math.PI);
    ctx.fillStyle = "#FF4359";
    ctx.fill();

    ctx2.beginPath();
    ctx2.arc(selectedShape.x + selectedShape.width / 2, selectedShape.y, 6, 0, 2 * Math.PI);
    ctx2.fillStyle = "rgb(255,255,253)";
    ctx2.fill();

    //Top Right Cornor
    /*if(selectedShape.type !== "group"){
        ctx.beginPath();
        ctx.arc(selectedShape.x + selectedShape.width, selectedShape.y, 6, 0, 2 * Math.PI);
        ctx.fillStyle = "#FF4359";
        ctx.fill();
    }*/

    ctx2.beginPath();
    ctx2.arc(selectedShape.x + selectedShape.width, selectedShape.y, 6, 0, 2 * Math.PI);
    ctx2.fillStyle = "rgb(255,255,254)";
    ctx2.fill();

    //Left Center Cornor
    ctx.beginPath();
    ctx.arc(selectedShape.x, selectedShape.y + selectedShape.height / 2, 6, 0, 2 * Math.PI);
    ctx.fillStyle = "#FF4359";
    ctx.fill();

    ctx2.beginPath();
    ctx2.arc(selectedShape.x, selectedShape.y + selectedShape.height / 2, 6, 0, 2 * Math.PI);
    ctx2.fillStyle = "rgb(255,255,251)";
    ctx2.fill();

    //Bottom Left Cornor
    /*if(selectedShape.type !== "group"){
        ctx.beginPath();
        ctx.arc(selectedShape.x, selectedShape.y + selectedShape.height, 6, 0, 2 * Math.PI);
        ctx.fillStyle = "#FF4359";
        ctx.fill();
    }*/

    ctx2.beginPath();
    ctx2.arc(selectedShape.x, selectedShape.y + selectedShape.height, 5, 0, 2 * Math.PI);
    ctx2.fillStyle = "rgb(255,255,252)";
    ctx2.fill();

    //Bottom Center Cornor
    ctx.beginPath();
    ctx.arc(selectedShape.x + selectedShape.width / 2, selectedShape.y + selectedShape.height, 6, 0, 2 * Math.PI);
    ctx.fillStyle = "#FF4359";
    ctx.fill();

    ctx2.beginPath();
    ctx2.arc(selectedShape.x + selectedShape.width / 2, selectedShape.y + selectedShape.height, 6, 0, 2 * Math.PI);
    ctx2.fillStyle = "rgb(255,255,249)";
    ctx2.fill();

    //Bottom Right Cornor
    /*if(selectedShape.type !== "group"){
    ctx.beginPath();
    ctx.arc(selectedShape.x + selectedShape.width, selectedShape.y + selectedShape.height, 6, 0, 2 * Math.PI);
    ctx.fillStyle = "#FF4359";
    ctx.fill();
    }*/

    ctx2.beginPath();
    ctx2.arc(selectedShape.x + selectedShape.width, selectedShape.y + selectedShape.height, 6, 0, 2 * Math.PI);
    ctx2.fillStyle = "rgb(255,255,250)";
    ctx2.fill();

    //Right Center Cornor
    ctx.beginPath();
    ctx.arc(selectedShape.x + selectedShape.width, selectedShape.y + selectedShape.height / 2, 6, 0, 2 * Math.PI);
    ctx.fillStyle = "#FF4359";
    ctx.fill();

    ctx2.beginPath();
    ctx2.arc(selectedShape.x + selectedShape.width, selectedShape.y + selectedShape.height / 2, 6, 0, 2 * Math.PI);
    ctx2.fillStyle = "rgb(255,255,248)";
    ctx2.fill();

    ctx.restore()
    ctx2.restore()
}


const snapTheSelectedElement = (arrayOfElements, CanvasData, selectedShape) => {

    CanvasData.ctx.save()
    CanvasData.ctx.fillStyle = "#FF4359"; // alignment line color

    let snapAlignToLine = 4

    const elementIsInRange = (position1, position2) => {
        if ((Math.max(position1, position2) - Math.min(position1, position2)) <= snapAlignToLine) {
            return true;
        } else {
            return false;
        }
    }

    const getThePositionToSnap = (position1, position2) => {
        return Math.abs(position1 - position2)
    }

    arrayOfElements.map(data => {
        if (data.colorKey !== selectedShape.colorKey) {

            //start of the y position
            //check the upper y and show the line of the top
            if (elementIsInRange(data.y, selectedShape.y)) {
                if (selectedShape.y > data.y) {
                    //right y
                    let checkNegativeOrPositive = data.y - selectedShape.y
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.y = selectedShape.y + getThePositionToSnap(data.y, selectedShape.y)
                    } else {
                        selectedShape.y = selectedShape.y - getThePositionToSnap(data.y, selectedShape.y)
                    }
                } else {
                    //left y
                    let checkNegativeOrPositive = data.y - selectedShape.y
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.y = selectedShape.y + getThePositionToSnap(data.y, selectedShape.y)
                    } else {
                        selectedShape.y = selectedShape.y - getThePositionToSnap(data.y, selectedShape.y)
                    }
                }
            }

            //check the selected top and element bottom
            if (elementIsInRange(data.y + data.height, selectedShape.y)) {
                if (data.x > selectedShape.x) {
                    //left
                    let checkNegativeOrPositive = (data.y + data.height) - selectedShape.y
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.y = selectedShape.y + getThePositionToSnap(data.y + data.height, selectedShape.y)
                    } else {
                        selectedShape.y = selectedShape.y - getThePositionToSnap(data.y + data.height, selectedShape.y)
                    }
                } else {
                    //right
                    let checkNegativeOrPositive = (data.y + data.height) - selectedShape.y
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.y = selectedShape.y + getThePositionToSnap(data.y + data.height, selectedShape.y)
                    } else {
                        selectedShape.y = selectedShape.y - getThePositionToSnap(data.y + data.height, selectedShape.y)
                    }
                }
            }

            //check the selected down side  and show the line
            if (elementIsInRange(selectedShape.y + selectedShape.height, data.y)) {
                if (data.x > selectedShape.x) {
                    //left
                    let checkNegativeOrPositive = (selectedShape.y + selectedShape.height) - data.y
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.y = selectedShape.y - getThePositionToSnap(selectedShape.y + selectedShape.height, data.y)
                    } else {
                        selectedShape.y = selectedShape.y + getThePositionToSnap(selectedShape.y + selectedShape.height, data.y)
                    }
                }
                else {
                    //right
                    let checkNegativeOrPositive = (selectedShape.y + selectedShape.height) - data.y
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.y = selectedShape.y - getThePositionToSnap(selectedShape.y + selectedShape.height, data.y)
                    } else {
                        selectedShape.y = selectedShape.y + getThePositionToSnap(selectedShape.y + selectedShape.height, data.y)
                    }
                }
            }


            //check the selected down side  and show the line
            if (elementIsInRange(selectedShape.y + selectedShape.height, data.y + data.height)) {
                if (data.x > selectedShape.x) {
                    //left
                    let checkNegativeOrPositive = (selectedShape.y + selectedShape.height) - (data.y + data.height)
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.y = selectedShape.y - getThePositionToSnap(selectedShape.y + selectedShape.height, data.y + data.height)
                    } else {
                        selectedShape.y = selectedShape.y + getThePositionToSnap(selectedShape.y + selectedShape.height, data.y + data.height)
                    }
                }
                else {
                    //right
                    let checkNegativeOrPositive = (selectedShape.y + selectedShape.height) - (data.y + data.height)
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.y = selectedShape.y - getThePositionToSnap(selectedShape.y + selectedShape.height, data.y + data.height)
                    } else {
                        selectedShape.y = selectedShape.y + getThePositionToSnap(selectedShape.y + selectedShape.height, data.y + data.height)
                    }
                }
            }

            //center the align left right
            if (elementIsInRange(selectedShape.y + parseInt(selectedShape.height / 2), data.y + parseInt(data.height / 2))) {
                if (data.x > selectedShape.x) {
                    //left
                    let checkNegativeOrPositive = (selectedShape.y + parseInt(selectedShape.height / 2)) - (data.y + parseInt(data.height / 2))
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.y = selectedShape.y - getThePositionToSnap(selectedShape.y + parseInt(selectedShape.height / 2), data.y + parseInt(data.height / 2))
                    } else {
                        selectedShape.y = selectedShape.y + getThePositionToSnap(selectedShape.y + parseInt(selectedShape.height / 2), data.y + parseInt(data.height / 2))
                    }
                } else {
                    //right
                    let checkNegativeOrPositive = (selectedShape.y + parseInt(selectedShape.height / 2)) - (data.y + parseInt(data.height / 2))
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.y = selectedShape.y - getThePositionToSnap(selectedShape.y + parseInt(selectedShape.height / 2), data.y + parseInt(data.height / 2))
                    } else {
                        selectedShape.y = selectedShape.y + getThePositionToSnap(selectedShape.y + parseInt(selectedShape.height / 2), data.y + parseInt(data.height / 2))
                    }
                }
            }
            //end of the y position


            //check the left top and show line
            if (elementIsInRange(selectedShape.x, data.x)) {
                if (data.y > selectedShape.y) {
                    //top
                    let checkNegativeOrPositive = selectedShape.x - data.x
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x, data.x)
                    } else {
                        selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x, data.x)
                    }
                } else {
                    //bottom
                    let checkNegativeOrPositive = selectedShape.x - data.x
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x, data.x)
                    } else {
                        selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x, data.x)
                    }
                }
            }

            if (elementIsInRange(selectedShape.x, data.x + data.width)) {
                if (data.y > selectedShape.y) {
                    //top
                    let checkNegativeOrPositive = selectedShape.x - (data.x + data.width)
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x, data.x + data.width)
                    } else {
                        selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x, data.x + data.width)
                    }
                } else {
                    //bottom
                    let checkNegativeOrPositive = selectedShape.x - (data.x + data.width)
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x, data.x + data.width)
                    } else {
                        selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x, data.x + data.width)
                    }
                }
            }

            //check the top right and show the line
            if (elementIsInRange(selectedShape.x + selectedShape.width, data.x)) {
                if (data.y > selectedShape.y) {
                    //top
                    let checkNegativeOrPositive = (selectedShape.x + selectedShape.width) - data.x
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x + selectedShape.width, data.x)
                    } else {
                        selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x + selectedShape.width, data.x)
                    }
                } else {
                    //bottom
                    let checkNegativeOrPositive = (selectedShape.x + selectedShape.width) - data.x
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x + selectedShape.width, data.x)
                    } else {
                        selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x + selectedShape.width, data.x)
                    }
                }
            }

            if (elementIsInRange(selectedShape.x + selectedShape.width, data.x + data.width)) {
                if (data.y > selectedShape.y) {
                    //top
                    let checkNegativeOrPositive = (selectedShape.x + selectedShape.width) - (data.x + data.width)
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x + selectedShape.width, data.x + data.width)
                    } else {
                        selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x + selectedShape.width, data.x + data.width)
                    }
                } else {
                    //top
                    let checkNegativeOrPositive = (selectedShape.x + selectedShape.width) - (data.x + data.width)
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x + selectedShape.width, data.x + data.width)
                    } else {
                        selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x + selectedShape.width, data.x + data.width)
                    }
                }
            }

            //center the align top
            if (elementIsInRange(selectedShape.x + parseInt(selectedShape.width / 2), data.x + parseInt(data.width / 2))) {
                if (data.y > selectedShape.y) {
                    //top
                    let checkNegativeOrPositive = (selectedShape.x + parseInt(selectedShape.width / 2)) - (data.x + parseInt(data.width / 2))
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x + parseInt(selectedShape.width / 2), data.x + parseInt(data.width / 2))
                    } else {
                        selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x + parseInt(selectedShape.width / 2), data.x + parseInt(data.width / 2))
                    }
                } else {
                    //bottom
                    let checkNegativeOrPositive = (selectedShape.x + parseInt(selectedShape.width / 2)) - (data.x + parseInt(data.width / 2))
                    if (checkNegativeOrPositive > 0) {
                        selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x + parseInt(selectedShape.width / 2), data.x + parseInt(data.width / 2))
                    } else {
                        selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x + parseInt(selectedShape.width / 2), data.x + parseInt(data.width / 2))
                    }
                }
            }
        }

        //check the canvas padding cornors
        let width1 = Math.trunc(0.10 * CanvasData.canvas.width)
        let height1 = Math.trunc(0.10 * CanvasData.canvas.height)
        let smallValue = Math.min(width1, height1)
        let rectangleWidth = (CanvasData.canvas.width - (2 * smallValue))
        let rectangleHeight = (CanvasData.canvas.height - (2 * smallValue))

        //left cornor 
        if (elementIsInRange(selectedShape.x, smallValue)) {
            let checkNegativeOrPositive = selectedShape.x - smallValue
            if (checkNegativeOrPositive > 0) {
                selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x, smallValue)
            } else {
                selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x, smallValue)
            }
        }

        //right cornor
        if (elementIsInRange(parseInt(selectedShape.x + selectedShape.width), rectangleWidth + smallValue)) {
            let checkNegativeOrPositive = parseInt(selectedShape.x + selectedShape.width) - (rectangleWidth + smallValue)
            if (checkNegativeOrPositive > 0) {
                selectedShape.x = selectedShape.x - getThePositionToSnap(parseInt(selectedShape.x + selectedShape.width), (rectangleWidth + smallValue))
            } else {
                selectedShape.x = selectedShape.x + getThePositionToSnap(parseInt(selectedShape.x + selectedShape.width), (rectangleWidth + smallValue))
            }
        }

        //top cornor
        if (elementIsInRange(selectedShape.y, smallValue)) {
            let checkNegativeOrPositive = selectedShape.y - smallValue
            if (checkNegativeOrPositive > 0) {
                selectedShape.y = selectedShape.y - getThePositionToSnap(selectedShape.y, smallValue)
            } else {
                selectedShape.y = selectedShape.y + getThePositionToSnap(selectedShape.y, smallValue)
            }
        }

        //bottom cornor
        if (elementIsInRange(parseInt(selectedShape.y + selectedShape.height), rectangleHeight + smallValue)) {
            let checkNegativeOrPositive = parseInt(selectedShape.y + selectedShape.height) - (rectangleHeight + smallValue)
            if (checkNegativeOrPositive > 0) {
                selectedShape.y = selectedShape.y - getThePositionToSnap(parseInt(selectedShape.y + selectedShape.height), (rectangleHeight + smallValue))
            } else {
                selectedShape.y = selectedShape.y + getThePositionToSnap(parseInt(selectedShape.y + selectedShape.height), rectangleHeight + smallValue)
            }
        }


        //check the canvas center and height and width
        if (elementIsInRange(selectedShape.x + parseInt(selectedShape.width / 2), parseInt(CanvasData.canvas.width / 2))) {
            let checkNegativeOrPositive = (selectedShape.x + parseInt(selectedShape.width / 2)) - parseInt(CanvasData.canvas.width / 2)
            if (checkNegativeOrPositive > 0) {
                selectedShape.x = selectedShape.x - getThePositionToSnap(selectedShape.x + parseInt(selectedShape.width / 2), parseInt(CanvasData.canvas.width / 2))
            } else {
                selectedShape.x = selectedShape.x + getThePositionToSnap(selectedShape.x + parseInt(selectedShape.width / 2), parseInt(CanvasData.canvas.width / 2))
            }
        }

        if (elementIsInRange(selectedShape.y + parseInt(selectedShape.height / 2), parseInt(CanvasData.canvas.height / 2))) {
            let checkNegativeOrPositive = (selectedShape.y + parseInt(selectedShape.height / 2)) - parseInt(CanvasData.canvas.height / 2)
            if (checkNegativeOrPositive > 0) {
                selectedShape.y = selectedShape.y - getThePositionToSnap(selectedShape.y + parseInt(selectedShape.height / 2), parseInt(CanvasData.canvas.height / 2))
            } else {
                selectedShape.y = selectedShape.y + getThePositionToSnap(selectedShape.y + parseInt(selectedShape.height / 2), parseInt(CanvasData.canvas.height / 2))
            }
        }
        CanvasData.ctx.restore()
        return null
    })
}


const showTheAlignmentOfSelectedElement = (arrayOfElements, CanvasData, selectedShape) => {

    CanvasData.ctx.save()
    CanvasData.ctx.fillStyle = "#FF4359"; // alignment line color

    arrayOfElements.map(data => {
        if (data.colorKey !== selectedShape.colorKey) {
            let width

            //start of the y position
            //check the upper y and show the line of the top
            if (data.y === selectedShape.y) {
                if (data.x > selectedShape.x) {
                    width = data.x - selectedShape.x
                    if (width < 0) {
                        width = Math.abs(width)
                    }
                    width = width + data.width
                    CanvasData.ctx.fillRect(selectedShape.x, selectedShape.y, width, 1);
                } else {
                    width = data.x - selectedShape.x
                    if (width < 0) {
                        width = Math.abs(width) + selectedShape.width
                    }
                    CanvasData.ctx.fillRect(data.x, selectedShape.y, width, 1);
                }
            }

            //check the down side y and show the line of the top
            if (data.y + data.height === selectedShape.y) {
                if (data.x > selectedShape.x) {
                    let width = data.x - selectedShape.x
                    if (width < 0) {
                        width = Math.abs(width) + selectedShape.width
                    }
                    width = width + data.width
                    CanvasData.ctx.fillRect(selectedShape.x, data.y + data.height, width, 1);
                } else {
                    let width = data.x - selectedShape.x
                    if (width < 0) {
                        width = Math.abs(width) + selectedShape.width
                    }
                    CanvasData.ctx.fillRect(data.x, selectedShape.y, width, 1);
                }
            }

            //check the selected down side  and show the line
            if (selectedShape.y + selectedShape.height === data.y) {
                if (data.x > selectedShape.x) {
                    let width = data.x - selectedShape.x
                    if (width < 0) {
                        width = Math.abs(width) + data.width
                    }
                    width = Math.abs(width) + data.width
                    CanvasData.ctx.fillRect(selectedShape.x, data.y, width, 1);
                }
                else {
                    let width = data.x - selectedShape.x
                    if (width < 0) {
                        width = Math.abs(width) + selectedShape.width
                    }
                    CanvasData.ctx.fillRect(data.x, selectedShape.y + selectedShape.height, width, 1);
                }
            }


            //check the selected down side  and show the line
            if (selectedShape.y + selectedShape.height === data.y + data.height) {
                if (data.x > selectedShape.x) {
                    let width = data.x - selectedShape.x
                    if (width < 0) {
                        width = Math.abs(width) + data.width
                    }
                    width = Math.abs(width) + data.width
                    CanvasData.ctx.fillRect(selectedShape.x, data.y + data.height, width, 1);
                }
                else {
                    let width = data.x - selectedShape.x
                    if (width < 0) {
                        width = Math.abs(width) + selectedShape.width
                    }
                    CanvasData.ctx.fillRect(data.x, selectedShape.y + selectedShape.height, width, 1);
                }
            }

            //center the align left right
            if (selectedShape.y + parseInt(selectedShape.height / 2) === data.y + parseInt(data.height / 2)) {
                if (data.x > selectedShape.x) {
                    let width = data.x - selectedShape.x
                    if (width < 0) {
                        width = Math.abs(width) + data.width
                    }
                    width = Math.abs(width) + data.width
                    CanvasData.ctx.fillRect(selectedShape.x, data.y + parseInt(data.height / 2), width, 1);
                } else {
                    let width = selectedShape.x - data.x
                    if (width < 0) {
                        width = Math.abs(width) + selectedShape.width
                    }
                    width = width + selectedShape.width
                    CanvasData.ctx.fillRect(data.x, selectedShape.y + parseInt(selectedShape.height / 2), width, 1);
                }
            }
            //end of the y position


            //check the left top and show line
            if (selectedShape.x === data.x) {
                if (data.y > selectedShape.y) {
                    let height = selectedShape.y - data.y
                    if (height < 0) {
                        height = Math.abs(height) + data.height
                    }
                    CanvasData.ctx.fillRect(data.x, selectedShape.y, 1, height);
                } else {
                    let height = selectedShape.y - data.y
                    if (height < 0) {
                        height = Math.abs(height) + data.height
                    }
                    height = height + selectedShape.height
                    CanvasData.ctx.fillRect(selectedShape.x, data.y, 1, height);
                }
            }

            if (selectedShape.x === data.x + data.width) {
                if (data.y > selectedShape.y) {
                    let height = selectedShape.y - data.y
                    if (height < 0) {
                        height = Math.abs(height) + data.height
                    }
                    CanvasData.ctx.fillRect(data.x + data.width, selectedShape.y, 1, height);
                } else {
                    let height = selectedShape.y - data.y
                    if (height < 0) {
                        height = Math.abs(height) + selectedShape.height
                    }
                    height = height + selectedShape.height
                    CanvasData.ctx.fillRect(selectedShape.x, data.y, 1, height);
                }
            }

            //check the top right and show the line
            if (selectedShape.x + selectedShape.width === data.x) {
                if (data.y > selectedShape.y) {
                    let height = selectedShape.y - data.y
                    if (height < 0) {
                        height = Math.abs(height) + data.height
                    }
                    CanvasData.ctx.fillRect(data.x, selectedShape.y, 1, height);
                } else {
                    let height = selectedShape.y - data.y
                    if (height < 0) {
                        height = Math.abs(height) + selectedShape.height
                    }
                    height = height + selectedShape.height
                    CanvasData.ctx.fillRect(selectedShape.x + selectedShape.width, data.y, 1, height);
                }
            }

            if (selectedShape.x + selectedShape.width === data.x + data.width) {
                if (data.y > selectedShape.y) {
                    let height = selectedShape.y - data.y
                    if (height < 0) {
                        height = Math.abs(height) + data.height
                    }
                    CanvasData.ctx.fillRect(data.x + data.width, selectedShape.y, 1, height);
                } else {
                    let height = selectedShape.y - data.y
                    if (height < 0) {
                        height = Math.abs(height) + selectedShape.height
                    }
                    height = height + selectedShape.height
                    CanvasData.ctx.fillRect(selectedShape.x + selectedShape.width, data.y, 1, height);
                }
            }

            //center the align top
            if (selectedShape.x + parseInt(selectedShape.width / 2) === data.x + parseInt(data.width / 2)) {
                if (data.y > selectedShape.y) {
                    let height = selectedShape.y - data.y
                    if (height < 0) {
                        height = Math.abs(height) + data.height
                    }
                    CanvasData.ctx.fillRect(data.x + parseInt(data.width / 2), selectedShape.y, 1, height);
                } else {
                    let height = selectedShape.y - data.y
                    if (height < 0) {
                        height = Math.abs(height) + selectedShape.height
                    }
                    height = height + selectedShape.height
                    CanvasData.ctx.fillRect(selectedShape.x + parseInt(selectedShape.width / 2), data.y, 1, height);
                }
            }
        }

        let width1 = Math.trunc(0.10 * CanvasData.canvas.width)
        let height1 = Math.trunc(0.10 * CanvasData.canvas.height)
        let smallValue = Math.min(width1, height1)
        let rectangleWidth = (CanvasData.canvas.width - (2 * smallValue))
        let rectangleHeight = (CanvasData.canvas.height - (2 * smallValue))

        //check the canvas padding cornors
        if (selectedShape.x === smallValue || selectedShape.y === smallValue || parseInt(selectedShape.x + selectedShape.width) === (rectangleWidth + smallValue) || parseInt(selectedShape.y + selectedShape.height) === rectangleHeight + smallValue) {
            let x = smallValue
            let y = smallValue
            CanvasData.ctx.strokeStyle = "#FF4359";
            CanvasData.ctx.strokeRect(x, y, rectangleWidth, rectangleHeight);
        }

        //check the canvas center and height and width
        if (selectedShape.x + parseInt(selectedShape.width / 2) === parseInt(CanvasData.canvas.width / 2)) {
            CanvasData.ctx.fillRect(selectedShape.x + parseInt(selectedShape.width / 2), 0, 1, CanvasData.canvas.height);
        }

        if (selectedShape.y + parseInt(selectedShape.height / 2) === parseInt(CanvasData.canvas.height / 2)) {
            CanvasData.ctx.fillRect(0, selectedShape.y + parseInt(selectedShape.height / 2), CanvasData.canvas.width, 1);
        }
        return null
    })
    CanvasData.ctx.restore()
}

export {
    drawBorder,
    showTheAlignmentOfSelectedElement,
    snapTheSelectedElement
}
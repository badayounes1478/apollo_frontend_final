import CanvasData from '../Canvas'
import { draw } from '../functions/DrawElementsOnCanvas'
import { drawBorder } from '../functions/Showcontrol'

const duplicateElement = () => {
    if (CanvasData.activeObject === null) return
    let data = {
        ...CanvasData.activeObject
    }
    data.id = Date.now()
    data.x = data.x + 10
    data.y = data.y + 10
    CanvasData.arrayOfElements = [...CanvasData.arrayOfElements, data]
    draw()
}

const deleteElement = () => {
    if (CanvasData.activeObject === null) return
    const filteredObject = CanvasData.arrayOfElements.filter((item) => item.colorKey !== CanvasData.activeObject.colorKey);
    CanvasData.arrayOfElements = filteredObject
    draw()
}

const sendBack = () => {
    if (CanvasData.activeObject === null) return
    let index = CanvasData.arrayOfElements.findIndex(x => x.colorKey === CanvasData.activeObject.colorKey);
    if (index === 0) return
    let swap = CanvasData.arrayOfElements[index - 1]
    CanvasData.arrayOfElements[index - 1] = CanvasData.arrayOfElements[index]
    CanvasData.arrayOfElements[index] = swap
    draw()
    drawBorder(CanvasData.activeObject, CanvasData.ctx, CanvasData.ctx2)
}

const sendFront = () => {
    if (CanvasData.activeObject === null) return
    let index = CanvasData.arrayOfElements.findIndex(x => x.colorKey === CanvasData.activeObject.colorKey);
    if (index === CanvasData.arrayOfElements.length - 1) return
    let swap = CanvasData.arrayOfElements[index + 1]
    CanvasData.arrayOfElements[index + 1] = CanvasData.arrayOfElements[index]
    CanvasData.arrayOfElements[index] = swap
    draw()
    drawBorder(CanvasData.activeObject, CanvasData.ctx, CanvasData.ctx2)
}


const boldText = () => {
    if (CanvasData.activeObject === null) return
    if (CanvasData.activeObject.type !== "text") return
    if (CanvasData.activeObject.bold === "bold") {
        CanvasData.activeObject.bold = "400"
    } else {
        CanvasData.activeObject.bold = "bold"
    }
    draw()
}

const italicText = () => {
    if (CanvasData.activeObject === null) return
    if (CanvasData.activeObject.type !== "text") return
    if (CanvasData.activeObject.italic === "italic") {
        CanvasData.activeObject.italic = ""
    } else {
        CanvasData.activeObject.italic = "italic"
    }
    draw()
}

const toggleListText = () => {
    if (CanvasData.activeObject === null) return
    if (CanvasData.activeObject.type !== "text") return
    CanvasData.activeObject.list = !CanvasData.activeObject.list
    draw()
}

const flip = (value) => {
    if (CanvasData.activeObject.type === "text") return
    if (value === "horizontal") {
        CanvasData.activeObject.horizontalFlip = !CanvasData.activeObject.horizontalFlip
    } else {
        CanvasData.activeObject.verticalFlip = !CanvasData.activeObject.verticalFlip
    }
    draw()
}

const lockAndUnlock = () => {
    if (CanvasData.activeObject === null) return
    CanvasData.activeObject.lock = !CanvasData.activeObject.lock
    draw()
}


const changeSvgColors = (activeObject, color, colorToChange, elementId) => {
    let element = document.getElementById(elementId + "svg")
    if (element === null || typeof (element) === "undefined") return
    let doc = element.contentDocument;
    if (doc === null || typeof (doc) === "undefined") return
    let path = doc.getElementsByTagName("path")
    let rect = doc.getElementsByTagName("rect")
    let circle = doc.getElementsByTagName("circle")
    let ellipse = doc.getElementsByTagName("ellipse")
    let line = doc.getElementsByTagName("line")
    let polyline = doc.getElementsByTagName("polyline")
    let polygon = doc.getElementsByTagName("polygon")
    let stop = doc.getElementsByTagName('stop')
    let g = doc.getElementsByTagName('g')

    let stopColors = [...stop]
    let allShapes = [...path, ...rect, ...circle, ...ellipse, ...line, ...polyline, ...polygon, ...g]


    for (let i = 0; i < stopColors.length; i++) {
        let object = stopColors[i].attributes
        let n = object.length
        for (let index = 0; index < n; index++) {
            if (object[index].value === colorToChange) {
                //persist object
                activeObject.colorObject.forEach(data1 => {
                    if (colorToChange === data1.changeColor) {
                        data1.changeColor = color
                    }
                })
                object[index].value = color
            }
        }
    }

    allShapes.forEach(data => {
        if (data.getAttribute('fill') === colorToChange) {
            activeObject.colorObject.forEach(data1 => {
                if (colorToChange === data1.changeColor) {
                    data1.changeColor = color
                }
            })
            data.attributes.fill.value = color
        }
        if (data.getAttribute('stroke') === colorToChange) {
            activeObject.colorObject.forEach(data1 => {
                if (colorToChange === data1.changeColor) {
                    data1.changeColor = color
                }
            })
            data.attributes.stroke.value = color
        }
        /*if (data.style.fill === colorToChange) {
            data.style.fill = color
        }
        if (data.style.stroke === colorToChange) {
            data.style.stroke = color
        }*/
    })
}

const displayElementToggle = () => {
    if (CanvasData.activeObject === null) return
    CanvasData.activeObject.display = !CanvasData.activeObject.display
    draw()
}

export {
    duplicateElement,
    deleteElement,
    sendBack,
    sendFront,
    boldText,
    italicText,
    toggleListText,
    flip,
    lockAndUnlock,
    changeSvgColors,
    displayElementToggle
}
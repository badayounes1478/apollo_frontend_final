import CanvasData from '../Canvas'

const clearCanvas = () => {
    CanvasData.ctx.clearRect(0, 0, CanvasData.canvas.width, CanvasData.canvas.height);
    CanvasData.ctx2.clearRect(0, 0, CanvasData.canvas.width, CanvasData.canvas.height);
}

const setWidthAndHeightToCanvas = () => {
    CanvasData.canvas.width = CanvasData.canvasWidth * CanvasData.multiplier
    CanvasData.canvas.height = CanvasData.canvasHeight * CanvasData.multiplier

    CanvasData.canvas2.width = CanvasData.canvasWidth * CanvasData.multiplier
    CanvasData.canvas2.height = CanvasData.canvasHeight * CanvasData.multiplier
}

const setColorToCanvas = () => {
    CanvasData.ctx.fillStyle = CanvasData.backgroundColor;
    CanvasData.ctx.fillRect(0, 0, CanvasData.canvas.width, CanvasData.canvas.height);
}

const addHeatReactangleToSeconadCanvas = (data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width
    if (data.type === "text") {
        width = data.width
    } else {
        width = data.width * CanvasData.multiplier
    }
    let height = data.height * CanvasData.multiplier

    CanvasData.ctx2.save()
    CanvasData.ctx2.translate(x + (width / 2), y + (height / 2))
    CanvasData.ctx2.rotate((Math.PI / 180) * data.rotate);
    CanvasData.ctx2.translate(-x - (width / 2), -y - (height / 2));

    CanvasData.ctx2.beginPath();
    CanvasData.ctx2.rect(x, y, width, height);
    CanvasData.ctx2.fillStyle = data.colorKey;
    CanvasData.ctx2.fill();
    CanvasData.ctx2.restore()
}

const rotateElement = (data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier

    CanvasData.ctx.translate(x + (width / 2), y + (height / 2))
    CanvasData.ctx.rotate((Math.PI / 180) * data.rotate);
    CanvasData.ctx.translate(-x - (width / 2), -y - (height / 2));
}

const blurElement = (data) => {
    CanvasData.ctx.filter = `blur(${data.blur}px)`
}

const verticalFlipElement = (img, data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier

    CanvasData.ctx.translate(x + width / 2, y + height / 2);
    CanvasData.ctx.scale(1, -1);

    // Move registration point back to the top left corner of canvas
    CanvasData.ctx.translate((-width) / 2, (-height) / 2);
    CanvasData.ctx.globalAlpha = data.opacity
    CanvasData.ctx.drawImage(img, 0, 0, width, height);
    CanvasData.ctx.globalAlpha = 1.0;
}

const horizontalFlipElement = (img, data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier

    CanvasData.ctx.translate(x + width / 2, y + height / 2);
    CanvasData.ctx.scale(-1, 1);

    // Move registration point back to the top left corner of canvas
    CanvasData.ctx.translate((-width) / 2, (-height) / 2);
    CanvasData.ctx.globalAlpha = data.opacity
    CanvasData.ctx.drawImage(img, 0, 0, width, height);
    CanvasData.ctx.globalAlpha = 1.0;
}

const getRandomColor = () => {
    const r = Math.round(Math.random() * 255);
    const g = Math.round(Math.random() * 255);
    /*added 247 because the cornors in heat canvas using 
    the colors which are above 247 so to avoid conflict 
    and dublication of colors 247 has been added*/
    const b = Math.round(Math.random() * 247);
    return `rgb(${r},${g},${b})`;
}

const shadowToElement = (data) => {
    CanvasData.ctx.shadowColor = data.shadowColor;
    CanvasData.ctx.shadowBlur = data.shadowBlur;
    CanvasData.ctx.shadowOffsetX = data.shadowOffsetX * CanvasData.multiplier;
    CanvasData.ctx.shadowOffsetY = data.shadowOffsetY * CanvasData.multiplier;
}

const borderRadiusToImages = (data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier
    let radius = data.radius * CanvasData.multiplier

    CanvasData.ctx.beginPath();
    CanvasData.ctx.moveTo(x + radius, y);
    CanvasData.ctx.lineTo(x + width - radius, y);
    CanvasData.ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    CanvasData.ctx.lineTo(x + width, y + height - radius);
    CanvasData.ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    CanvasData.ctx.lineTo(x + radius, y + height);
    CanvasData.ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    CanvasData.ctx.lineTo(x, y + radius);
    CanvasData.ctx.quadraticCurveTo(x, y, x + radius, y);
    CanvasData.ctx.closePath();
    CanvasData.ctx.clip();
}

const showLoadingImage = (data) => {
    let svgImage = document.getElementById(data.elementId)
    CanvasData.ctx.save()
    CanvasData.ctx.filter = "blur(10px)";
    CanvasData.ctx.drawImage(svgImage, data.x, data.y, data.width, data.height);
    CanvasData.ctx.restore()
}


const addSvgColors = (allShapes, stopColors, data) => {

    for (let i = 0; i < stopColors.length; i++) {
        let object = stopColors[i].attributes
        let n = object.length
        for (let index = 0; index < n; index++) {
            data.colorObject.forEach(data1 => {
                if (data1.originalColor === object[index].value) {
                    object[index].value = data1.changeColor
                }
            })
        }
    }

    allShapes.forEach(data1 => {
        data.colorObject.forEach(data2 => {
            if (data2.originalColor === data1.getAttribute('fill')) {
                data1.attributes.fill.value = data2.changeColor
            }
            if (data2.originalColor === data1.getAttribute('stroke')) {
                data1.attributes.stroke.value = data2.changeColor
            }
        })
    })
}

const draw = () => {
    //canvasFunctions
    clearCanvas()
    setWidthAndHeightToCanvas()
    setColorToCanvas()

    //clear the colorHash array
    CanvasData.colorsHash = []

    //start drawing on the canvas
    CanvasData.arrayOfElements.forEach(data => {
        if (data.display === false) return
        if (data.type === "image") {
            drawImageOnCanvas(data)
        } else if (data.type === "text") {
            drawTextOnCanvas(data)
        } else if (data.type === "svg") {
            drawSvgOnCanvas(data)
        } else if (data.type === "mask") {
            drawMaskOnCanvas(data)
        } else if (data.type === "group") {
            drawGroupOnCanvas(data)
        } if (data.type === "chart") {
            drawChartOnCanvas(data)
        } if (data.type === "video") {
            drawVideoOnCanvas(data)
        }
    })
}

const drawImageOnCanvas = (data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier
    let img = document.getElementById(data.elementId);

    CanvasData.arrayOfElements.forEach(circle => {
        if (circle.id === data.id) {
            while (true) {
                const colorKey = getRandomColor();
                if (!CanvasData.colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    CanvasData.colorsHash = [...CanvasData.colorsHash, colorKey]
                    return circle.colorKey
                }
            }
        }
    });

    CanvasData.ctx.save()
    rotateElement(data)
    shadowToElement(data)
    borderRadiusToImages(data)
    CanvasData.ctx.filter = `brightness(${data.brightness}) contrast(${data.contrast}) grayscale(${data.grayscale}) saturate(${data.saturate}) sepia(${data.sepia}) invert(${data.invert}) blur(${data.blur}px)`
    if (data.verticalFlip === true) {
        verticalFlipElement(img, data)
    } else if (data.horizontalFlip === true) {
        horizontalFlipElement(img, data)
    } else {
        CanvasData.ctx.globalAlpha = data.opacity
        CanvasData.ctx.drawImage(img, x, y, width, height);
        CanvasData.ctx.globalAlpha = 1.0;
    }
    CanvasData.ctx.restore()
    addHeatReactangleToSeconadCanvas(data)
}

const drawSvgOnCanvas = (data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier

    CanvasData.arrayOfElements.forEach(circle => {
        if (circle.id === data.id) {
            while (true) {
                const colorKey = getRandomColor();
                if (!CanvasData.colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    CanvasData.colorsHash = [...CanvasData.colorsHash, colorKey]
                    return circle.colorKey
                }
            }
        }
    });


    const getSvgContent = () => {
        let element = document.getElementById(data.elementId + "svg")
        if (element === null || typeof (element) === "undefined") return
        let doc = element.contentDocument;
        if (doc === null || typeof (doc) === "undefined") return
        let svg = doc.getElementsByTagName("svg")
        if (svg.length <= 0) return

        let path = doc.getElementsByTagName("path")
        let rect = doc.getElementsByTagName("rect")
        let circle = doc.getElementsByTagName("circle")
        let ellipse = doc.getElementsByTagName("ellipse")
        let line = doc.getElementsByTagName("line")
        let polyline = doc.getElementsByTagName("polyline")
        let polygon = doc.getElementsByTagName("polygon")
        let stop = doc.getElementsByTagName('stop')
        let g = doc.getElementsByTagName('g')

        let stopColors = [...stop]
        let allShapes = [...path, ...rect, ...circle, ...ellipse, ...line, ...polyline, ...polygon, ...g]
        let color = []

        var s = new XMLSerializer().serializeToString(svg[0])
        var encodedData = window.btoa(s);
        let str2 = "data:image/svg+xml;base64,"
        let url = str2.concat(encodedData);
        data.svgImageUrl = url
        let img = document.getElementById(`${data.elementId}svgsecret`)
        img.src = url

        CanvasData.ctx.save()
        rotateElement(data)
        blurElement(data)
        shadowToElement(data)
        CanvasData.ctx.filter = `brightness(${data.brightness}) contrast(${data.contrast}) grayscale(${data.grayscale}) saturate(${data.saturate}) sepia(${data.sepia}) invert(${data.invert}) blur(${data.blur}px)`

        if (data.verticalFlip === true) {
            verticalFlipElement(img, data)
        } else if (data.horizontalFlip === true) {
            horizontalFlipElement(img, data)
        } else {
            CanvasData.ctx.globalAlpha = data.opacity
            CanvasData.ctx.drawImage(img, x, y, width, height);
            CanvasData.ctx.globalAlpha = 1.0;
        }

        const isColor = (strColor) => {
            var s = new Option().style;
            s.color = strColor;
            var test1 = s.color === strColor;
            var test2 = /^#[0-9A-F]{6}$/i.test(strColor);
            if (test1 === true || test2 === true) {
                return true;
            } else {
                return false;
            }
        }

        for (let i = 0; i < stopColors.length; i++) {
            let object = stopColors[i].attributes
            let n = object.length
            for (let index = 0; index < n; index++) {
                if (isColor(object[index].value)) {
                    color.push(object[index].value)
                }
            }
        }

        for (let i = 0; i < allShapes.length; i++) {
            color.push(allShapes[i].getAttribute('fill'))
            color.push(allShapes[i].getAttribute('stroke'))
            //color.push(allShapes[i].style.fill)
            //color.push(allShapes[i].style.stroke)
        }

        addSvgColors(allShapes, stopColors, data)

        let allColors = [...new Set(color)];

        data.colors = allColors.filter((data) => data !== "none" && data !== null && data !== "" && data !== "url(#paint0_linear)")

        if (data.colorObjectSet === false) {
            data.colorObject = data.colors.map(data => {
                return {
                    originalColor: data,
                    changeColor: data
                }
            })
            data.colorObjectSet = true
        }

        addHeatReactangleToSeconadCanvas(data)
        CanvasData.ctx.restore()
    }

    if (data.base64 === null) {
        showLoadingImage(data)
        addHeatReactangleToSeconadCanvas(data)
    } else if (data.flag === 0) {
        showLoadingImage(data)
        addHeatReactangleToSeconadCanvas(data)
        data.flag = 1
        fetch(data.base64)
            .then(res => res.blob())
            .then(blob => {
                let svg = document.getElementById(data.elementId + "svg")
                const blobURL = URL.createObjectURL(blob);
                svg.setAttribute("data", blobURL);
                data.blobURL = blobURL
                data.blobCompleted = true
            })
    } else if (data.blobCompleted === false) {
        showLoadingImage(data)
        addHeatReactangleToSeconadCanvas(data)
    } else if (data.blobURL !== null) {
        getSvgContent()
    } else {
        showLoadingImage(data)
        addHeatReactangleToSeconadCanvas(data)
    }
}

const drawTextOnCanvas = (data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let size = data.size * CanvasData.multiplier
    let width = 0

    CanvasData.arrayOfElements.forEach(circle => {
        if (circle.id === data.id) {
            while (true) {
                const colorKey = getRandomColor();
                if (!CanvasData.colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    CanvasData.colorsHash = [...CanvasData.colorsHash, colorKey]
                    return circle.colorKey
                }
            }
        }
    });

    let txt = data.content
    CanvasData.ctx.save()
    CanvasData.canvas.style.letterSpacing = (data.letterSpacing * CanvasData.multiplier) + 'px';
    CanvasData.ctx.globalAlpha = data.opacity
    CanvasData.ctx.textBaseline = data.baseline;
    CanvasData.ctx.font = data.italic + " " + data.bold + " " + size + "px " + data.fontFamily
    CanvasData.ctx.fillStyle = data.color;
    CanvasData.ctx.textAlign = data.align

    blurElement(data)
    shadowToElement(data)

    //convert the text in array if multiline is there
    let data1 = txt.split("\n")
    let len = data1.length * size
    data.height = len


    if (data1.length <= 1) {

        CanvasData.ctx.textAlign = "left"
        CanvasData.ctx.textBaseline = "top";
        width = CanvasData.ctx.measureText(txt).width
        data.width = Math.ceil(width)

        //to rotate the text
        rotateElement(data)

        //rotate the 2nd canvas rectangle
        CanvasData.ctx2.save()
        CanvasData.ctx2.translate(x + (width / 2), y + (data.height / 2))
        CanvasData.ctx2.rotate((Math.PI / 180) * data.rotate);
        CanvasData.ctx2.translate(-x - (width / 2), -y - (data.height / 2));

        CanvasData.ctx2.beginPath();
        CanvasData.ctx2.rect(x, y, width, data.height);
        CanvasData.ctx2.fillStyle = data.colorKey;
        CanvasData.ctx2.fill();

        CanvasData.ctx2.restore()
        CanvasData.ctx.fillText(txt, x, y);

    } else {
        //get the largest width from and array
        if (data.list) {
            for (let i = 0; i < data1.length; i++) {
                if (width < CanvasData.ctx.measureText(i + 1 + ". " + data1[i]).width) {
                    width = CanvasData.ctx.measureText(i + 1 + ". " + data1[i]).width
                    data.width = Math.ceil(width)
                }
            }
        } else {
            for (let i = 0; i < data1.length; i++) {
                if (width < CanvasData.ctx.measureText(data1[i]).width) {
                    width = CanvasData.ctx.measureText(data1[i]).width
                    data.width = Math.ceil(width)
                }
            }
        }

        rotateElement(data)

        let lineHeight = size + data.lineHeight
        for (let i = 0; i < data1.length; i++) {
            if (data.list) {
                if (data.align === "center") {
                    CanvasData.ctx.fillText(i + 1 + ". " + data1[i], x + (width / 2), y + (len / 2) + i * lineHeight);
                } else if (data.align === "right") {
                    CanvasData.ctx.fillText(i + 1 + ". " + data1[i], x + width, y + i * lineHeight);
                } else if (data.align === "left") {
                    CanvasData.ctx.fillText(i + 1 + ". " + data1[i], x, y + i * lineHeight);
                }
            } else {
                if (data.align === "center") {
                    CanvasData.ctx.fillText(data1[i], x + (width / 2), y + (len / 2) + i * lineHeight);
                } else if (data.align === "right") {
                    CanvasData.ctx.fillText(data1[i], x + width, y + i * lineHeight);
                } else if (data.align === "left") {
                    CanvasData.ctx.fillText(data1[i], x, y + i * lineHeight);
                }
            }
        }

        //rotate the rectangle
        CanvasData.ctx.restore()
        CanvasData.ctx2.translate(x + (width / 2), y + (len / 2))
        CanvasData.ctx2.rotate((Math.PI / 180) * data.rotate);
        CanvasData.ctx2.translate(-x - (width / 2), -y - (len / 2));

        CanvasData.ctx2.save()
        CanvasData.ctx2.beginPath();
        CanvasData.ctx2.rect(x, y, width, len);
        CanvasData.ctx2.fillStyle = data.colorKey;
        CanvasData.ctx2.fill();
        CanvasData.ctx2.restore()

    }
    CanvasData.canvas.style.letterSpacing = 0 + 'px';
    CanvasData.ctx.restore()
}

const drawMaskOnCanvas = (data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier
    let img = document.getElementById(data.elementId)
    let img1 = document.getElementById(data.elementId2)


    CanvasData.arrayOfElements.forEach(circle => {
        if (circle.id === data.id) {
            while (true) {
                const colorKey = getRandomColor();
                if (!CanvasData.colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    CanvasData.colorsHash = [...CanvasData.colorsHash, colorKey]
                    return circle.colorKey
                }
            }
        }
    });

    let canvasMask = document.getElementById('canvas3')
    let ctxMask = canvasMask.getContext("2d");

    canvasMask.height = height
    canvasMask.width = width
    ctxMask.clearRect(0, 0, canvasMask.width, canvasMask.height);
    ctxMask.fillRect(0, 0, canvasMask.width, canvasMask.height);
    ctxMask.globalCompositeOperation = 'source-in';

    ctxMask.drawImage(img, 0, 0, width, height);
    ctxMask.drawImage(img1, 0, 0, width, height);
    ctxMask.globalCompositeOperation = 'source-over';
    ctxMask.restore()

    CanvasData.ctx.save()
    rotateElement(data)
    shadowToElement(data)
    CanvasData.ctx.drawImage(canvasMask, x, y, width, height);
    CanvasData.ctx.restore()

    addHeatReactangleToSeconadCanvas(data)
}

const drawGroupOnCanvas = (data) => {
    CanvasData.arrayOfElements.forEach(circle => {
        if (circle.id === data.id) {
            while (true) {
                const colorKey = getRandomColor();
                if (!CanvasData.colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    CanvasData.colorsHash = [...CanvasData.colorsHash, colorKey]
                    return circle.colorKey
                }
            }
        }
    });

    data.data.forEach(element => {
        if (element.type === "image") {
            drawImageOnCanvas(element)
        } else if (element.type === "svg") {
            drawSvgOnCanvas(element)
        } else if (element.type === "text") {
            drawTextOnCanvas(element)
        } else if (element.type === "mask") {
            drawMaskOnCanvas(element)
        } else if (element.type === "chart") {
            drawChartOnCanvas(element)
        } else if (element.type === "video") {
            drawVideoOnCanvas(element)
        }
    })

    addHeatReactangleToSeconadCanvas(data)
}

const drawChartOnCanvas = (data) => {
    CanvasData.arrayOfElements.forEach(circle => {
        if (circle.id === data.id) {
            while (true) {
                const colorKey = getRandomColor();
                if (!CanvasData.colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    CanvasData.colorsHash = [...CanvasData.colorsHash, colorKey]
                    return circle.colorKey
                }
            }
        }
    });

    if (data.chartType === "bar") {
        blurElement(data)
        shadowToElement(data)
        barGraph(data)
    }
}


let barGraph = (data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier

    CanvasData.ctx.globalAlpha = data.opacity
    let content = data.data
    let numberOfbars = content.length
    let widthOfGraph = width
    let heightOfGraph = height
    let marginBetweenbars = 4
    let barWidth = widthOfGraph / numberOfbars - marginBetweenbars * 2

    //get the largest value from graph so we can plot as per ratio
    let largestValueInGraph = 0
    content.forEach(data => {
        if (data.marks > largestValueInGraph) {
            largestValueInGraph = data.marks
        }
    })

    let lineGap = height / numberOfbars
    let gabBetweenNumbers = largestValueInGraph / numberOfbars

    content.forEach((data1, index) => {
        CanvasData.ctx.save()
        CanvasData.ctx.beginPath();
        let y2 = Math.trunc(y + index * lineGap)
        CanvasData.ctx.rect(x, y2, width, 1);
        CanvasData.ctx.fillStyle = "rgb(0, 0, 0, 0.03)";
        CanvasData.ctx.fill();
        CanvasData.ctx.restore()

        CanvasData.ctx.save()
        CanvasData.ctx.fillStyle = "#000000"
        CanvasData.ctx.font = `${12 * CanvasData.multiplier}px Arial`;
        let x1 = x - barWidth
        let y1 = Math.trunc(y + index * lineGap) + 3
        let plotNumber = Math.trunc(gabBetweenNumbers * (numberOfbars - index))
        CanvasData.ctx.fillText(plotNumber, x1, y1, data.width)
        CanvasData.ctx.restore()
    })

    content.forEach((data1, index) => {
        let ratio = data1.marks / largestValueInGraph
        let barHeight = ratio * heightOfGraph

        CanvasData.ctx.save()
        CanvasData.ctx.beginPath();
        CanvasData.ctx.translate(x, y);
        CanvasData.ctx.rotate((Math.PI / 180) * 180);
        CanvasData.ctx.translate(-x - width, -y - height);
        let x1 = x + index * (barWidth + 10)
        let y1 = y
        CanvasData.ctx.rect(x1, y1, barWidth, barHeight);
        CanvasData.ctx.fillStyle = data1.color
        CanvasData.ctx.fill();
        CanvasData.ctx.restore()
    })
    CanvasData.ctx.globalAlpha = 1.0;

    addHeatReactangleToSeconadCanvas(data)
}


const drawVideoOnCanvas = (data) => {
    let x = data.x * CanvasData.multiplier
    let y = data.y * CanvasData.multiplier
    let width = data.width * CanvasData.multiplier
    let height = data.height * CanvasData.multiplier
    let img = document.getElementById(data.elementId);

    CanvasData.arrayOfElements.forEach(circle => {
        if (circle.id === data.id) {
            while (true) {
                const colorKey = getRandomColor();
                if (!CanvasData.colorsHash[colorKey]) {
                    circle.colorKey = colorKey;
                    CanvasData.colorsHash = [...CanvasData.colorsHash, colorKey]
                    return circle.colorKey
                }
            }
        }
    });

    CanvasData.ctx.save()
    rotateElement(data)
    shadowToElement(data)
    borderRadiusToImages(data)
    CanvasData.ctx.filter = `brightness(${data.brightness}) contrast(${data.contrast}) grayscale(${data.grayscale}) saturate(${data.saturate}) sepia(${data.sepia}) invert(${data.invert}) blur(${data.blur}px)`
    if (data.verticalFlip === true) {
        verticalFlipElement(img, data)
    } else if (data.horizontalFlip === true) {
        horizontalFlipElement(img, data)
    } else {
        CanvasData.ctx.globalAlpha = data.opacity
        CanvasData.ctx.drawImage(img, x, y, width, height);
        CanvasData.ctx.globalAlpha = 1.0;
    }
    CanvasData.ctx.restore()
    addHeatReactangleToSeconadCanvas(data)
}



export {
    draw
}
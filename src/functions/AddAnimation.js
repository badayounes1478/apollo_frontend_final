import CanvasData from "../Canvas"

const fromLeft = () => {
    if (CanvasData.activeObject === null) return
    let speed = (CanvasData.activeObject.width + CanvasData.activeObject.x) / 30
    CanvasData.activeObject.animation = { play: true, x: 0 - CanvasData.activeObject.width, y: 0, type: "fromLeft", speed: speed, time: 1, delay: 2000 }
}

const fromRight = () => {
    if (CanvasData.activeObject === null) return
    let speed = (CanvasData.canvasWidth - CanvasData.activeObject.x) / 30
    CanvasData.activeObject.animation = { play: true, x: CanvasData.canvasWidth, y: 0, type: "fromRight", speed: speed, time: 1, delay: 2000 }
}

const fromTop = () => {
    if (CanvasData.activeObject === null) return
    let speed = (CanvasData.activeObject.height + CanvasData.activeObject.y) / 30
    CanvasData.activeObject.animation = { play: true, x: 0, y: 0 - CanvasData.activeObject.height, type: "fromTop", speed: speed, time: 1, delay: 2000 }
}

const fromBottom = () => {
    if (CanvasData.activeObject === null) return
    let speed = (CanvasData.canvasHeight - CanvasData.activeObject.y) / 30
    CanvasData.activeObject.animation = { play: true, x: 0, y: CanvasData.canvasHeight, type: "fromBottom", speed: speed, time: 1, delay: 2000 }
}

const fadeIn = () => {
    if (CanvasData.activeObject === null) return
    CanvasData.activeObject.animation = { play: true, type: "fadeIn", opacity: 0, speed: 1 / 30, time: 1, delay: 2000 }
}

const fadeOut = () => {
    if (CanvasData.activeObject === null) return
    CanvasData.activeObject.animation = { play: true, type: "fadeOut", opacity: 1, speed: 1 / 30, time: 1, delay: 2000 }
}


const rotate = () => {
    if (CanvasData.activeObject === null) return
    CanvasData.activeObject.animation = { play: true, type: "rotate", rotate: 0, speed: 12, time: 1, delay: 2000 }
}

export {
    fromLeft,
    fromRight,
    fromTop,
    fromBottom,
    fadeIn,
    fadeOut,
    rotate
}
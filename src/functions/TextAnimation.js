import CanvasData from '../Canvas'

const fromLeftAnimationText = (txt, x, y, data) => {
    if (x > data.animation.x) {
        CanvasData.ctx4.fillText(txt, data.animation.x, y);
    } else {
        CanvasData.ctx4.fillText(txt, x, y);
    }
}

const fromRightAnimationText = (txt, x, y, data) => {
    if (x < data.animation.x) {
        CanvasData.ctx4.fillText(txt, data.animation.x, y);
    } else {
        CanvasData.ctx4.fillText(txt, x, y);
    }
}

const fromTopAnimationText = (txt, x, y, data) => {
    if (y > data.animation.y) {
        CanvasData.ctx4.fillText(txt, x, data.animation.y);
    } else {
        CanvasData.ctx4.fillText(txt, x, y);
    }
}

const fromBottomAnimationText = (txt, x, y, data) => {
    if (y < data.animation.y) {
        CanvasData.ctx4.fillText(txt, x, data.animation.y);
    } else {
        CanvasData.ctx4.fillText(txt, x, y);
    }
}

const fadeInAnimationText = (txt, x, y, data) => {
    if (data.animation.opacity < 1) {
        CanvasData.ctx4.globalAlpha = data.animation.opacity
        CanvasData.ctx4.fillText(txt, x, y);
        CanvasData.ctx4.globalAlpha = 1
    } else {
        CanvasData.ctx4.globalAlpha = 1
        CanvasData.ctx4.fillText(txt, x, y);
    }
}

const fadeOutAnimationText = (txt, x, y, data) => {
    if (data.animation.opacity > 0) {
        CanvasData.ctx4.globalAlpha = data.animation.opacity
        CanvasData.ctx4.fillText(txt, x, y);
        CanvasData.ctx4.globalAlpha = 1
    } else {
        CanvasData.ctx4.globalAlpha = 0
        CanvasData.ctx4.fillText(txt, x, y);
        CanvasData.ctx4.globalAlpha = 1
    }
}

const rotateAnimationText = (txt, x, y, data) => {
    if (data.animation.rotate < 360) {
        CanvasData.ctx4.translate(data.x + (data.width / 2), data.y + (data.height / 2))
        CanvasData.ctx4.rotate((Math.PI / 180) * data.animation.rotate);
        CanvasData.ctx4.translate(-data.x - (data.width / 2), -data.y - (data.height / 2));
        CanvasData.ctx4.fillText(txt, x, y);
    } else {
        CanvasData.ctx4.fillText(txt, x, y);
    }
}


export {
    fromLeftAnimationText,
    fromRightAnimationText,
    fromTopAnimationText,
    fromBottomAnimationText,
    fadeInAnimationText,
    fadeOutAnimationText,
    rotateAnimationText
}



import CanvasData from "../Canvas";

const fromLeftAnimationImage = (image) => {
    const img = document.getElementById(image.elementId);
    image.animation.x = image.animation.x + image.animation.speed
    if (image.x > image.animation.x) {
        CanvasData.ctx4.drawImage(img, image.animation.x, image.y, image.width, image.height);
    } else {
        CanvasData.ctx4.drawImage(img, image.x, image.y, image.width, image.height);
    }
}

const fromRightAnimationImage = (image) => {
    var img = document.getElementById(image.elementId);
    image.animation.x = image.animation.x - image.animation.speed
    if (image.x < image.animation.x) {
        CanvasData.ctx4.drawImage(img, image.animation.x, image.y, image.width, image.height);
    } else {
        CanvasData.ctx4.drawImage(img, image.x, image.y, image.width, image.height);
    }
}

const fromTopAnimationImage = (image) => {
    var img = document.getElementById(image.elementId);
    image.animation.y = image.animation.y + image.animation.speed
    if (image.y > image.animation.y) {
        CanvasData.ctx4.drawImage(img, image.x, image.animation.y, image.width, image.height);
    } else {
        CanvasData.ctx4.drawImage(img, image.x, image.y, image.width, image.height);
    }
}

const fromBottomAnimationImage = (image) => {
    var img = document.getElementById(image.elementId);
    image.animation.y = image.animation.y - image.animation.speed
    if (image.y < image.animation.y) {
        CanvasData.ctx4.drawImage(img, image.x, image.animation.y, image.width, image.height);
    } else {
        CanvasData.ctx4.drawImage(img, image.x, image.y, image.width, image.height);
    }
}

const fadeInAnimationImage = (data) => {
    const img = document.getElementById(data.elementId);
    if (data.animation.opacity < 1) {
        CanvasData.ctx4.globalAlpha = data.animation.opacity
        data.animation.opacity = data.animation.speed + data.animation.opacity
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
        CanvasData.ctx4.globalAlpha = 1
    } else {
        CanvasData.ctx4.globalAlpha = 1
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
    }
}

const fadeOutAnimationImage = (data) => {
    const img = document.getElementById(data.elementId);
    if (data.animation.opacity > 0) {
        CanvasData.ctx4.globalAlpha = data.animation.opacity
        data.animation.opacity = data.animation.opacity - data.animation.speed
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
        CanvasData.ctx4.globalAlpha = 1
    } else {
        CanvasData.ctx4.globalAlpha = 0
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
        CanvasData.ctx4.globalAlpha = 1
    }
}


const rotateAnimationImage = (data) => {
    const img = document.getElementById(data.elementId);
    data.animation.rotate = data.animation.rotate + data.animation.speed
    if (data.animation.rotate < 360) {
        CanvasData.ctx4.translate(data.x + (data.width / 2), data.y + (data.height / 2))
        CanvasData.ctx4.rotate((Math.PI / 180) * data.animation.rotate);
        CanvasData.ctx4.translate(-data.x - (data.width / 2), -data.y - (data.height / 2));
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
    } else {
        CanvasData.ctx4.drawImage(img, data.x, data.y, data.width, data.height);
    }
}

export {
    fromLeftAnimationImage,
    fromRightAnimationImage,
    fromTopAnimationImage,
    fromBottomAnimationImage,
    fadeInAnimationImage,
    fadeOutAnimationImage,
    rotateAnimationImage
}
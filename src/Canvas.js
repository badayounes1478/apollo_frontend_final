const CanvasData = {
    design_id: null,
    canvas: null,
    canvas2: null,
    canvas4: null,
    ctx: null,
    ctx2: null,
    ctx4: null,
    frame: 0,
    allFrame: 0,
    playing: false,
    activeObject: null,
    projectName: "Project name",
    backgroundColor: '#ffffff',
    multiplier: 1,
    multiply: 3,
    canvasWidth: 640,
    canvasHeight: 360,
    duration: 1000,
    colorsHash: [],
    groupes: [],
    audio: null,
    multipleCanvas: [],
    playingState: "finish",
    arrayOfElements: [
        {
            id: 3, x: 50, y: 40, type: 'text', color: "#000000", size: 24, lock: false, opacity: 1.0, name: "Text",
            fontFamily: 'Open Sans', content: "Select template", horizontalFlip: false, verticalFlip: false,
            stroke: false, list: false, italic: "", bold: "400", align: 'left', baseline: 'top', letterSpacing: 0,
            shadowOffsetX: 0, shadowOffsetY: 0, shadowColor: '#000000', shadowBlur: 0, display: true,
            doubleCliked: false, foucesIndex: 0, rotate: 0, blur: 0, textStyle: 'user', lineHeight: 0
        }
    ]
}


export default CanvasData
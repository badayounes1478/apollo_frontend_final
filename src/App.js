import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Home from './components/screens/Home'
import Editor from './components/screens/Editor'
import ProtectedRoutes from './components/screens/ProtectedRoutes'
import Loading from './components/reusablecomponents/Loading'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Loading} />
        <ProtectedRoutes exact path="/home" component={Home} />
        <ProtectedRoutes exact path="/editor" component={Editor} />
      </Switch>
    </BrowserRouter>
  )
}

export default App

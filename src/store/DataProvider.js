import { createContext, useState } from 'react'

const AppContext = createContext({
    refreshScreen: false,
    activeLeftButton: 1,

    //functions
    refreshTheScreen: () => { },
    changeActiveButtonLeft: () => { }
})

export const AppContextProvider = (props) => {

    const [refreshScreen, setrefreshScreen] = useState(false)
    const [activeLeftButton, setactiveLeftButton] = useState(1)

    const refreshTheScreen = () => {
        setrefreshScreen((prev) => !prev)
    }

    const changeActiveButtonLeft = (value) => {
        setactiveLeftButton(value)
    }

    const context = {
        //state
        refreshScreen: refreshScreen,
        activeLeftButton: activeLeftButton,

        //functions
        refreshTheScreen: refreshTheScreen,
        changeActiveButtonLeft: changeActiveButtonLeft
    }

    return <AppContext.Provider value={context}>
        {props.children}
    </AppContext.Provider>
}

export default AppContext